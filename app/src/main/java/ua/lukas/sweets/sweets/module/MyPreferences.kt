package ua.lukas.sweets.sweets.module

import android.content.Context
import java.util.*

interface MyPreferencesInterface {
    val phone: String
    val deviceID: String
    val token: String
    fun clear()
    val balance: Int
    var employeeCardNumber: String
}

class MyPreferences : MyPreferencesInterface {

    private val PREF_NAME = "LS_Pref"

    val context = LukasSweetsApp.getContext()

    companion object {
        private const val SHARED_PREF_DARK_STATUS = "darkStatus"
        private const val SHARED_PREF_USER_PHONE = "phoneNumber"
        private const val SHARED_PREF_COUNT_HISTORY = "countHistory"
        private const val SHARED_PREF_TOKEN_ANDROID = "tokenAndroid"
        private const val SHARED_PREF_HIDE_ECO_CHECK = "hide_eco_check"
        private const val SHARED_PREF_CARD_NUMBER = "card_number"
        private const val SHARED_PREF_EMPLOYEE_CARD_NUMBER = "employee_card_number"
        private const val SHARED_PREF_QA_RATING = "QA_"
        private const val SHARED_PREF_DLM_FEEDBACK = "dlm_feedback"
        private const val SHARED_PREF_DEVICE_ID = "device_id"
        private const val SHARED_PREF_BALANCE = "card_balance"
        private const val SHARED_PREF_USER_CITY = "user_city"
    }

    private val preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    override var phone: String
        get() = preferences.getString(SHARED_PREF_USER_PHONE, "-1")!!
        set(value) = preferences.edit().putString(SHARED_PREF_USER_PHONE, value).apply()

    override var deviceID: String
        get() = preferences.getString(SHARED_PREF_DEVICE_ID, "")!!
        set(value) = preferences.edit().putString(SHARED_PREF_DEVICE_ID, value).apply()

    override var token: String
        get() = preferences.getString(SHARED_PREF_TOKEN_ANDROID, "")!!
        set(value) = preferences.edit().putString(SHARED_PREF_TOKEN_ANDROID, value).apply()


    var darkMode = preferences.getInt(SHARED_PREF_DARK_STATUS, 0)
        set(value) = preferences.edit().putInt(SHARED_PREF_DARK_STATUS, value).apply()

//    var phoneNumber = preferences.getString(SHARED_PREF_USER_PHONE, "-1")!!
//        set(value) = preferences.edit().putString(SHARED_PREF_USER_PHONE, value).apply()

    var countHistory = preferences.getInt(SHARED_PREF_COUNT_HISTORY, 0)
        set(value) = preferences.edit().putInt(SHARED_PREF_COUNT_HISTORY, value).apply()


    var hideEcoCheck = preferences.getBoolean(SHARED_PREF_HIDE_ECO_CHECK, false)
        set(value) = preferences.edit().putBoolean(SHARED_PREF_HIDE_ECO_CHECK, value).apply()

    var cardNumber = preferences.getString(SHARED_PREF_CARD_NUMBER, "-1")!!
        set(value) = preferences.edit().putString(SHARED_PREF_CARD_NUMBER, value).apply()

    override var employeeCardNumber = preferences.getString(SHARED_PREF_EMPLOYEE_CARD_NUMBER, "-1")!!
        set(value) = preferences.edit().putString(SHARED_PREF_EMPLOYEE_CARD_NUMBER, value).apply()


    fun getRating(qa: Int): Float {
        return preferences.getFloat(SHARED_PREF_QA_RATING + qa, 0F)
    }

    fun setRating(qa: Int, number: Float) {
        preferences.edit().putFloat(SHARED_PREF_QA_RATING + qa, number).apply()
    }

    var dlmFeedback = preferences.getString(SHARED_PREF_DLM_FEEDBACK, "1970-01-01 00:00:00")!!
        set(value) = preferences.edit().putString(SHARED_PREF_DLM_FEEDBACK, value).apply()

    override var balance: Int
        get() = preferences.getInt(SHARED_PREF_BALANCE, 0)
        set(value) = preferences.edit().putInt(SHARED_PREF_BALANCE, value).apply()


    override fun clear() {
        preferences.edit().clear().apply()
        deviceID = UUID.randomUUID().toString()
    }


}