package ua.lukas.sweets.sweets.view.registrations

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewbinding.ViewBinding
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.HintRequest
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.Alerts
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.repository.sms.SMSRepository
import ua.lukas.sweets.sweets.databinding.ActivityRegisterPhoneBinding
import java.util.*


interface RegisterPhoneActivityInterface {
    fun generationCode(): String
}
class RegisterPhoneActivity : AppCompatActivity(), ViewBinding, RegisterPhoneActivityInterface {

    private lateinit var binding: ActivityRegisterPhoneBinding
    private val smsRepository = SMSRepository()
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterPhoneBinding.inflate(layoutInflater)
        setContentView(binding.root)

        alerts = Alerts(this)
        loadDialog = alerts.load()

        binding.nextButton.backgroundTintList =
            ContextCompat.getColorStateList(this, R.color.light_gray)
        binding.nextButton.isEnabled = false

        binding.input.requestFocus()


        requestHint()

        binding.rulesTextView1.setOnClickListener {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://sweets.lukas.ua/terms")
            )
            startActivity(browserIntent)
        }

        binding.input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence, i: Int, i1: Int, i2: Int
            ) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (binding.input.length() >= 13) {
                    binding.nextButton.backgroundTintList =
                        ContextCompat.getColorStateList(this@RegisterPhoneActivity, R.color.red)
                    binding.nextButton.isEnabled = true
                    //InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    //imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getRootView().getWindowToken(), 0);
                } else if (binding.input.length() < 13) {
                    binding.nextButton.backgroundTintList = ContextCompat.getColorStateList(
                        this@RegisterPhoneActivity,
                        R.color.light_gray
                    )
                    binding.nextButton.isEnabled = false
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })



        binding.nextButton.setOnClickListener {
            loadDialog?.show()
            CoroutineScope(Dispatchers.Default).launch {
                val code = generationCode()
                val phone = binding.input.text.toString().replace(" ", "")

                val result = smsRepository.sendOtpCode(code, "38$phone")
//                val result = ResponseCode.OK
                withContext(Dispatchers.Main) {
                    loadDialog?.dismiss()
                    when (result) {
                        ResponseCode.OK -> {
                            Intent(
                                this@RegisterPhoneActivity,
                                EnterCodeRegistration::class.java
                            ).apply {
                                putExtra("code", code)
                                putExtra("phone", phone)
                                startActivity(this)
                            }
                        }
                        else -> {
                            alerts.showAlerterNoInternet()
                        }
                    }
                }
            }
        }
        //End onCreate
    }

    private fun requestHint() {
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val credentialsClient = Credentials.getClient(this)
        val intent = credentialsClient.getHintPickerIntent(hintRequest)
        startIntentSenderForResult(intent.intentSender, 1, null, 0, 0, 0)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 ->
                // Obtain the phone number from the result
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val credential = data.getParcelableExtra<Credential>(Credential.EXTRA_KEY)
                    // credential.getId();  <-- will need to process phone number string
                    //Toast.makeText(this, credential?.id, Toast.LENGTH_SHORT ).show()
                    binding.input.setText(credential?.id?.substring(3))
                }
            // ...
        }
    }


    override fun generationCode(): String {
        val random = Random()
        return String.format("%04d", random.nextInt(10000))
    }

    override fun onBackPressed() {
        //super.onBackPressed();
        moveTaskToBack(true)
    }

    override fun getRoot(): View {
        TODO("Not yet implemented")
    }


    //End RegisterPhoneActivity
}
