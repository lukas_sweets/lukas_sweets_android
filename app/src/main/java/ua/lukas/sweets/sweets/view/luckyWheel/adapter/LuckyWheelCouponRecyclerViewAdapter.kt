package ua.lukas.sweets.sweets.view.luckyWheel.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.LuckyWheelCouponsApiModel
import ua.lukas.sweets.sweets.databinding.ListItemCouponsBinding
import ua.lukas.sweets.sweets.view.luckyWheel.LuckyWheelCoupons
import kotlin.random.Random

class LuckyWheelCouponRecyclerViewAdapter(
    private val context: Context,
    private var list: List<LuckyWheelCouponsApiModel>
) : RecyclerView.Adapter<LuckyWheelCouponRecyclerViewAdapter.CouponsItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponsItemViewHolder {
        val binding =
            ListItemCouponsBinding.inflate(LayoutInflater.from(context), parent, false)
        return CouponsItemViewHolder(binding)
    }


    override fun onBindViewHolder(holder: CouponsItemViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item, context)
    }

    override fun getItemCount() = list.size


    class CouponsItemViewHolder(
        couponsItemLayoutBinding: ListItemCouponsBinding
    ) :
        RecyclerView.ViewHolder(couponsItemLayoutBinding.root) {

        private val binding = couponsItemLayoutBinding

        fun bind(couponsItem: LuckyWheelCouponsApiModel, context: Context) {
            with(couponsItem) {
                with(binding) {
                    val colorList = context.resources.getIntArray(R.array.coupons).toList()
                    val colorBg = colorList[Random.nextInt(colorList.size)]
                    color = colorBg
                    nameTextView.text = name
                    dateTextView.text =
                        context.resources.getString(R.string.lucky_wheel_use_to, date_to)
                    statusTextView.text =
                        if (is_used) context.resources.getString(R.string.lucky_wheel_status_used) else context.resources.getString(
                            R.string.lucky_wheel_status_off
                        )
                    Picasso.get().load(picture).into(priseImageView)

                    if (!is_off && !is_used) materialCardView.backgroundTintList =
                        ColorStateList.valueOf(colorBg)

                    if (is_off) offLayout.visibility = View.VISIBLE else offLayout.visibility = View.GONE

                    if (is_used) {
                        dateTextView.visibility = View.GONE
                        statusTextView.setTextColor(ContextCompat.getColor(context, R.color.green))
                    } else {
                        dateTextView.visibility = View.VISIBLE
                        statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red))

                    }

                    if (!is_used && !is_off) {
                        statusTextView.visibility = View.GONE
                        binding.root.setOnClickListener {
                            val modalCoupon = LuckyWheelCouponModalBottomSheet(couponsItem)
                            modalCoupon.show(
                                (context as LuckyWheelCoupons).supportFragmentManager,
                                "TAG"
                            )
                        }
                    }
                }
            }
        }
    }
}