package ua.lukas.sweets.sweets.view.questions

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.RatingBar.OnRatingBarChangeListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.databinding.QaLayoutBinding

class QAActivity : AppCompatActivity() {


    private lateinit var binding: QaLayoutBinding
    private val preferences = MyPreferences()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = QaLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = intent.getStringExtra("question")




        binding.ratingBar.rating = preferences.getRating(intent.getIntExtra("numberQ", -1))

        binding.textView31.text = intent.getStringExtra("question")
        binding.answerTextView.text = intent.getStringExtra("answer")


        binding.call.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    this@QAActivity,
                    Manifest.permission.CALL_PHONE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                this@QAActivity.startActivity(
                    Intent(
                        Intent.ACTION_CALL,
                        Uri.parse("tel:0800505091")
                    )
                )
            } else {
                checkPermission(Manifest.permission.CALL_PHONE, 1)
            }
        }

        binding.ratingBar.onRatingBarChangeListener =
            OnRatingBarChangeListener { ratingBar, rating, fromUser ->
               preferences.setRating(intent.getIntExtra("numberQ", -1), rating)
            }


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish() // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item)
    }


    fun checkPermission(permission: String, requestCode: Int) {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(
                this@QAActivity,
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                this@QAActivity,
                arrayOf(permission),
                requestCode
            )
        } else {
            Toast.makeText(this@QAActivity, "Permission already granted", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(
            requestCode, permissions,
            grantResults
        )
        if (requestCode == 1) {
            // Checking whether user granted the permission or not.
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Showing the toast message
                this@QAActivity.startActivity(
                    Intent(
                        Intent.ACTION_CALL,
                        Uri.parse("tel:0800505091")
                    )
                )
            }
        }
    }
}