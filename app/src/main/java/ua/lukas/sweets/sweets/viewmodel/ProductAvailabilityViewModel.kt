package ua.lukas.sweets.sweets.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ua.lukas.sweets.sweets.data.api.ProductAvailabilityApiModel

class ProductAvailabilityViewModel:ViewModel() {

    private val localAvailabilityProductModel = MutableLiveData<List<ProductAvailabilityApiModel>?>()
    val availabilityProductModel: LiveData<List<ProductAvailabilityApiModel>?> = localAvailabilityProductModel

    fun setAvailabilityProduct(data: List<ProductAvailabilityApiModel>?) {
        localAvailabilityProductModel.value = data
    }


}