package ua.lukas.sweets.sweets.module

import android.app.Activity
import com.example.tastytoast.TastyToast
import com.kaopiz.kprogresshud.KProgressHUD
import ua.lukas.sweets.sweets.R


interface AlertsInterface {
    fun showAlerterNoInternet()
    fun showAlertFeedbackIsSend()
    fun load(): KProgressHUD
    fun error()
}

class Alerts(
    private val activity: Activity
) : AlertsInterface {

    override fun showAlerterNoInternet() {
        TastyToast.error(
            activity,
            activity.resources.getString(R.string.error_no_internet),
            TastyToast.LENGTH_LONG,
            TastyToast.SHAPE_ROUND,
            false
        );
    }

    override fun showAlertFeedbackIsSend() {
        TastyToast.success(
            activity,
            activity.resources.getString(R.string.feedback_send_text_ok),
            TastyToast.LENGTH_LONG,
            TastyToast.SHAPE_ROUND,
            false
        );
    }

    override fun load(): KProgressHUD {
        return KProgressHUD.create(activity)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)
    }

    override fun error() {
        TastyToast.success(
            activity,
            activity.resources.getString(R.string.error),
            TastyToast.LENGTH_LONG,
            TastyToast.SHAPE_ROUND,
            false
        );
    }


}