package ua.lukas.sweets.sweets.data.repository.catalog

import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.*
import java.util.concurrent.TimeUnit

interface CatalogRemoteDataSourceInterface {
    suspend fun getCatalog(): Pair<ResponseCode, CatalogApiModel?>
    suspend fun getPack(pack_id: Int, city_id: String?): Pair<ResponseCode, PackApiModel?>
    suspend fun getAvailabilityProduct(
        city_id: String,
        pack_id: Int
    ): Pair<ResponseCode, List<ProductAvailabilityApiModel>?>
    suspend fun getPackFromBarcode (barcode: String) : Int
}

class CatalogRemoteDataSource(
    private val api: Api = Api(),
) : CatalogRemoteDataSourceInterface {

    override suspend fun getCatalog(): Pair<ResponseCode, CatalogApiModel?> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.catalog)
            .setOkHttpClient(okHttpClient)
            .build()
        val response = request.executeForObject(CatalogApiModel::class.java)


        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as CatalogApiModel)
        else
            Log.e("CatalogRemoteDataSource: getCatalog", response.error.errorCode.toString())

        return Pair(ResponseCode.ERROR, null)
    }


    override suspend fun getPack(pack_id: Int, city_id: String?): Pair<ResponseCode, PackApiModel?> {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.catalogPack)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("pack_id", pack_id.toString())
            .addQueryParameter("city_id", city_id)
            .build()
        val response = request.executeForObject(PackApiModel::class.java)


        if (response.isSuccess)
            return if (response.okHttpResponse.code() == 204)
                Pair(ResponseCode.ERROR, null)
            else
                Pair(ResponseCode.OK, response.result as PackApiModel)
        else
            Log.e("CatalogRemoteDataSource: getCatalog", response.error.errorCode.toString())

        return Pair(ResponseCode.ERROR, null)
    }

    override suspend fun getAvailabilityProduct(
        city_id: String,
        pack_id: Int
    ): Pair<ResponseCode, List<ProductAvailabilityApiModel>?> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.catalogAvailability)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("pack_id", pack_id.toString())
            .addQueryParameter("city_id", city_id)
            .build()
        val response = request.executeForObjectList(ProductAvailabilityApiModel::class.java)

        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as List<ProductAvailabilityApiModel>)
        else
            Log.e("CatalogRemoteDataSource: getAvailabilityProduct", response.error.errorCode.toString())

        return Pair(ResponseCode.ERROR, null)
    }

    override suspend fun getPackFromBarcode(barcode: String): Int {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.catalogFindBarcode)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("barcode", barcode)
            .build()
        val response = request.executeForObject(PackFindBarcodeApi::class.java)

        if (response.isSuccess)
            return (response.result as PackFindBarcodeApi).packID
        else
            Log.e("CatalogRemoteDataSource: getAvailabilityProduct", response.error.errorCode.toString())

        return 0
    }
}