package ua.lukas.sweets.sweets.view.feedback

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.FeedbackModel
import ua.lukas.sweets.sweets.data.repository.feedback.FeedbackRepository
import ua.lukas.sweets.sweets.databinding.ActivityMyFeedbackBinding


class MyFeedbackActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMyFeedbackBinding
    private var feedbackRepository = FeedbackRepository()
    private var alerts: Alerts? = null
    private var dialog: KProgressHUD? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyFeedbackBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        alerts = Alerts(this)

        dialog = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)


        binding.recycleView.layoutManager = LinearLayoutManager(this)

        getFeedbacks()
    }

    override fun onResume() {
        super.onResume()
        getFeedbacks(false)
    }

    private fun getFeedbacks(reload: Boolean = false) {
        if (reload) dialog?.show()
        CoroutineScope(Dispatchers.Default).launch {
            val (response, arrayFeedback) = feedbackRepository.getFeedbacks(reload = reload)
            withContext(Dispatchers.Main) {
                dialog?.dismiss()

                if (reload && response == ResponseCode.ERROR)
                    alerts?.showAlerterNoInternet()

                binding.recycleView.adapter =
                    MyFeedbackAdapter(this@MyFeedbackActivity, arrayFeedback)
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.my_feedback_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == R.id.reload) {
            getFeedbacks(true)
        }

        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}


class MyFeedbackAdapter(
    private val context: Context,
    private val feedbackList: List<FeedbackModel>,
) : RecyclerView.Adapter<MyFeedbackAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var subject: TextView = itemView.findViewById(R.id.subjectTextView)
        var status: TextView = itemView.findViewById(R.id.statusTextView)
        var date: TextView = itemView.findViewById(R.id.dateTextView)
        var dotNewMessage: ImageView = itemView.findViewById(R.id.circleImageView)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_my_feedback, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return feedbackList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val topic = feedbackList[position]

        holder.date.text = DateTime.formatDateFeedback(topic.date_time)

        when (topic.subject) {
            1 -> holder.subject.text =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_gratitude)
            2 -> holder.subject.text =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_question)
            3 -> holder.subject.text =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_complaint)
            4 -> holder.subject.text =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_offer)
        }

        when (topic.status) {
            1 -> {
                holder.status.text =
                    LukasSweetsApp.getContext().resources.getString(R.string.feedback_status_new)
                holder.status.setTextColor(
                    ContextCompat.getColorStateList(
                        LukasSweetsApp.getContext(),
                        R.color.dark_gray
                    )
                )
            }
            2 -> {
                holder.status.text =
                    LukasSweetsApp.getContext().resources.getString(R.string.feedback_status_work)
                holder.status.setTextColor(
                    ContextCompat.getColorStateList(
                        LukasSweetsApp.getContext(),
                        R.color.feedback_yellow
                    )
                )
            }
            3 -> {
                holder.status.text =
                    LukasSweetsApp.getContext().resources.getString(R.string.feedback_status_closed)
                holder.status.setTextColor(
                    ContextCompat.getColorStateList(
                        LukasSweetsApp.getContext(),
                        R.color.feedback_green
                    )
                )
            }
        }

        if (topic.author == 1 && topic.is_read == 0) {
            holder.dotNewMessage.visibility = View.VISIBLE
        } else {
            holder.dotNewMessage.visibility = View.INVISIBLE
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(context, MyFeedbackDTActivity::class.java)
            intent.putExtra("hd_id", topic.feedback_id)
            intent.putExtra("subject", topic.subject)
            context.startActivity(intent)
        }


    }

}