package ua.lukas.sweets.sweets.data.repository.sms

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.ResponseCode

interface SMSRepositoryInterface {
    suspend fun sendOtpCode(code: String, phone: String): ResponseCode
}

class SMSRepository(
    private val smsRemoteDataSource: SMSRemoteDataSource = SMSRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),

    ) : SMSRepositoryInterface {

    override suspend fun sendOtpCode(code: String, phone: String): ResponseCode = withContext(scope.coroutineContext) {
            return@withContext smsRemoteDataSource.sendOtpCode(code, phone)
        }
}