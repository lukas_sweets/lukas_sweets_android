package ua.lukas.sweets.sweets.view.home.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.module.ifLet
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.stock.StockActivity
import ua.lukas.sweets.sweets.data.model.StockModel
import ua.lukas.sweets.sweets.data.model.StockModelParcelable
import ua.lukas.sweets.sweets.databinding.ListItemStockBinding

class StockRecyclerViewAdapter(
    private val context: Context,
    private var list: List<StockModel>
) : RecyclerView.Adapter<StockRecyclerViewAdapter.StockItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockItemViewHolder {
        val binding =
            ListItemStockBinding.inflate(LayoutInflater.from(context), parent, false)
        return StockItemViewHolder(binding)
    }


    override fun onBindViewHolder(holder: StockItemViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item, context)
    }

    override fun getItemCount() = list.size


    class StockItemViewHolder(
        stockItemLayoutBinding: ListItemStockBinding
    ) :
        RecyclerView.ViewHolder(stockItemLayoutBinding.root) {

        private val binding = stockItemLayoutBinding

        fun bind(stockItem: StockModel, context: Context) {
            with(stockItem) {
                with(binding){
                    nameTextView.text = name
                    old_price?.let {
                        oldPriceTextView.text =
                            "${old_price} ${context.resources.getString(R.string.grn)}"
                        oldPriceTextView.paintFlags =
                            oldPriceTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        oldPriceTextView.visibility = View.VISIBLE
                    } ?: run {
                        oldPriceTextView.visibility = View.GONE
                    }

                    new_price?.let {
                        newPriceTextView.text =
                            "${new_price} ${context.resources.getString(R.string.grn)}"
                        newPriceTextView.visibility = View.VISIBLE
                    } ?: run {
                        newPriceTextView.visibility = View.GONE
                    }

                    labelCardView.visibility = View.INVISIBLE

                    if (show_label) {
                        ifLet(
                            label_text,
                            label_color,
                            label_bg_color
                        ) { (label_t, label_c, label_bg_c) ->
                            labelTextView.text = label_t
                            labelTextView.setTextColor(Color.parseColor(label_c))
                            labelCardView.setCardBackgroundColor(Color.parseColor(label_bg_c))
                            labelCardView.visibility = View.VISIBLE
                        }
                    }

                    Picasso.get().load(picture).into(stockImageView)

                    root.setOnClickListener {
                        Intent(context, StockActivity::class.java).apply {
                            putExtra(
                                "data", StockModelParcelable(
                                    id,
                                    name,
                                    short_name,
                                    description,
                                    old_price,
                                    new_price,
                                    time,
                                    picture,
                                    top_position,
                                    show_label,
                                    label_text,
                                    label_color,
                                    label_bg_color,
                                    order,
                                    productsPackId
                                )
                            )
                            context.startActivity(this)
                        }
                    }
                }
            }
        }
    }
}