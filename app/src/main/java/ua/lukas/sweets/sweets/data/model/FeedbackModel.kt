package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ua.lukas.sweets.sweets.data.api.FeedbackApiModel

@Entity(
    tableName = "feedbacks",
//    indices = [
//        Index(
//            value = ["message_id"],
//            unique = true
//        )
//    ]
)
data class FeedbackModel(
    @PrimaryKey
    val message_id: Int,
    val feedback_id: Int,
    val date_time: String,
    val subject: Int,
    val status: Int,
    val text: String,
    val author: Int,
    val is_read: Int
)

fun FeedbackApiModel.toDBModel() : FeedbackModel {
    return FeedbackModel(
        this.message_id,
        this.feedback_id,
        this.date_time,
        this.subject,
        this.status,
        this.text,
        this.author,
        this.is_read
    )
}