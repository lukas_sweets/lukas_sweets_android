package ua.lukas.sweets.sweets.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.androidnetworking.AndroidNetworking
import ua.lukas.sweets.sweets.data.db.AppDatabase


open class LukasSweetsApp : Application() {
    override fun onCreate() {
        super.onCreate()
        mInstance = this
        AndroidNetworking.initialize(applicationContext);
        database = Room.databaseBuilder(this, AppDatabase::class.java, "AppDatabase").build()
    }

    companion object {
        lateinit var mInstance: LukasSweetsApp
        private lateinit var database: AppDatabase

        fun getContext(): Context {
            return mInstance.applicationContext
        }

        fun getDatabase(): AppDatabase {
            return database
        }

    }
}