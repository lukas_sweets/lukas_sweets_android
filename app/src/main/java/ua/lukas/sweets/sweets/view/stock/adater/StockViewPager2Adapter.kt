package ua.lukas.sweets.sweets.view.stock.adater

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.module.ifLet
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.stock.StockActivity
import ua.lukas.sweets.sweets.data.model.StockModel


class StockViewPager2Adapter(
    private val ctx: Context,
    private val arrayStocks: List<StockModel>
) :
    RecyclerView.Adapter<StockViewPager2Adapter.ViewHolder>() {

private val TAG = "StockViewPager2Adapter"
    // This method returns our layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(ctx).inflate(R.layout.list_item_stock, parent, false)
        return ViewHolder(view)
    }

    // This method binds the screen with the view
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val stock = arrayStocks[position]

        Picasso.get().load(stock.picture).into(holder.image)
        holder.nameTextView.text = stock.name
        stock.old_price?.let { old_prise ->
            holder.oldPriceTextView.text = "$old_prise ₴"
            holder.oldPriceTextView.paintFlags = holder.oldPriceTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            holder.oldPriceTextView.visibility = View.VISIBLE
        }
        stock.new_price?.let { new_price ->
            holder.newPriceTextView.text = "$new_price ₴"
            holder.newPriceTextView.visibility = View.VISIBLE
        }
        holder.labelCardView.visibility = View.GONE
        if (stock.show_label) {
            ifLet(
                stock.label_text,
                stock.label_color,
                stock.label_bg_color
            ) { (label_t, label_c, label_bg_c) ->
                holder.labelTextView.text = label_t
                holder.labelTextView.setTextColor(Color.parseColor(label_c))
                holder.labelCardView.setCardBackgroundColor(Color.parseColor(label_bg_c))
                holder.labelCardView.visibility = View.VISIBLE
            }
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(ctx, StockActivity::class.java)
            intent.putExtra("img", stock.picture)
            intent.putExtra("name", stock.name)
            intent.putExtra("time", stock.time)
            intent.putExtra("descriptions", stock.description)
            intent.putExtra("oldPrice", stock.old_price)
            intent.putExtra("newPrice", stock.new_price)
            intent.putExtra("label_text", stock.label_text)
            intent.putExtra("label_color", stock.label_color)
            intent.putExtra("label_bg_color", stock.label_bg_color)
            ctx.startActivity(intent)
        }



    }

    // This Method returns the size of the Array
    override fun getItemCount(): Int {
        return arrayStocks.size
    }

    // The ViewHolder class holds the view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var nameTextView: TextView
        var newPriceTextView: TextView
        var oldPriceTextView: TextView
        var labelTextView: TextView
        var labelCardView: androidx.cardview.widget.CardView

        init {
            image = itemView.findViewById(R.id.stockImageView)
            nameTextView = itemView.findViewById(R.id.nameTextView)
            newPriceTextView = itemView.findViewById(R.id.newPriceTextView)
            oldPriceTextView = itemView.findViewById(R.id.oldPriceTextView)
            labelTextView = itemView.findViewById(R.id.labelTextView)
            labelCardView = itemView.findViewById(R.id.labelCardView)
        }
    }
}