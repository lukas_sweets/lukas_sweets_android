package ua.lukas.sweets.sweets.view.registrations

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.StartActivity
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.databinding.NewUserDataActivityBinding

class NewUserDataActivity : AppCompatActivity(), ViewBinding {

    private lateinit var binding: NewUserDataActivityBinding

    private val cardRepository = CardRepository()
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null


    private lateinit var phone: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = NewUserDataActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        alerts = Alerts(this)
        loadDialog = alerts.load()

        phone = intent.getStringExtra("phone").toString()

//        dialog = KProgressHUD.create(this)
//            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//            .setCancellable(false)
//            .setAnimationSpeed(1)
//            .setDimAmount(0.5f)

        binding.nameTextField.requestFocus()

        binding.saveButton.setOnClickListener {
            if (binding.nameTextField.editText?.text.toString().trim().isEmpty()) {
                binding.nameTextField.error = resources.getString(R.string.feedback_enter_name)
            }
            if (binding.nameTextField.editText?.text.toString().trim().isNotEmpty()) {
                binding.nameTextField.error = null
                HideKeyboard.hideSoftKeyboard(this)
                loadDialog?.show()
                CoroutineScope(Dispatchers.Default).launch {
                    val response =
                        cardRepository.registerBonusCard(binding.nameEdit.text.toString(), phone)

                    withContext(Dispatchers.Main) {
                        loadDialog?.dismiss()
                        when (response) {
                            ResponseCode.OK -> {
                                startActivity(
                                    Intent(
                                        this@NewUserDataActivity,
                                        StartActivity::class.java
                                    )
                                )
                            }
                            else -> {
                                alerts.showAlerterNoInternet()
                            }
                        }
                    }
                }
            }
        }


        //END onCreate
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            val i = Intent(this, RegisterPhoneActivity::class.java)
            startActivity(i)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getRoot(): View {
        TODO("Not yet implemented")
    }

    //End NewUserDataActivity
}