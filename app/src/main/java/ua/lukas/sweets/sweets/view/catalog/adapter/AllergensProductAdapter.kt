package ua.lukas.sweets.sweets.view.catalog.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.databinding.ListItemAllergenProductBinding
import java.util.Dictionary



class AllergensProductAdapter(
    private val context: Context,
    private var allergens: List<String>
) :
    RecyclerView.Adapter<AllergensProductAdapter.AllergenItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllergenItemViewHolder {
        val binding =
            ListItemAllergenProductBinding.inflate(LayoutInflater.from(context), parent, false)
        return AllergenItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AllergenItemViewHolder, position: Int) {
        val item = allergens[position]
        holder.bind(item, context)
    }

    override fun getItemCount(): Int {
        return allergens.size
    }

    fun updateList(list: List<String>) {
        allergens = list
        notifyDataSetChanged()
    }


    class AllergenItemViewHolder(
        categoryItemLayoutBinding: ListItemAllergenProductBinding
    ) :
        RecyclerView.ViewHolder(categoryItemLayoutBinding.root) {

        private val binding = categoryItemLayoutBinding

        fun bind(allergenItem: String, context: Context) {

            when (allergenItem) {
                "celery" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_celery
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_celery)
                }

                "gluten" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_gluten
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_gluten)
                }

                "crustaceans" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_crustaceans
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_crustaceans)
                }

                "eggs" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_egg
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_eggs)
                }

                "fish" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_fish
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_fish)
                }

                "milk" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_milk
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_milk)
                }

                "shellfish" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_seashell
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_shellfish)
                }

                "mustard" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_mustard
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_mustard)
                }

                "nuts" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_nut
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_nuts)
                }

                "peanuts" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_peanut
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_peanuts)
                }

                "lupin" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_lupin
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_lupin)
                }

                "sesame" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_sesame
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_sesame)
                }

                "soy" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_soy
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_soy)
                }

                "sulfite" -> {
                    binding.iconImageView.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ic_sulfite
                        )
                    )
                    binding.nameTextView.text = context.getString(R.string.allergen_sulfite)
                }

            }
        }


    }

}