package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class NewsApiModel (
    val title: String,
    val validity: String,
    val text_news: String,
    val picture: String,
    val button_show: Boolean,
    val button_text: String?,
    val button_url: String?,
)