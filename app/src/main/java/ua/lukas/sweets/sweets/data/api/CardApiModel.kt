package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class CardApiModel(
//    @SerializedName("card_number")
    val card_number: String,

//    @SerializedName("bonus_card")
    val bonus_card: Int,

//    @SerializedName("balance")
    val balance: Int? ,

//    @SerializedName("purchase_amount")
    val purchase_amount: Double?,

//    @SerializedName("loyal_size")
    val loyal_size: Int
)
