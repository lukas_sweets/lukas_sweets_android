package ua.lukas.sweets.sweets.view.catalog.adapter

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.card.MaterialCardView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.R


class CatalogCheckBoxView : FrameLayout {

    private lateinit var categoryCardView: MaterialCardView
    private lateinit var categoryTextView: TextView
    private lateinit var categoryImageView: ImageView

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context) : super(context) {
        init(context)
    }


    private fun init(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.catalog_group, this)
        categoryCardView = rootView.findViewById<View>(R.id.categoryCardView) as MaterialCardView
        categoryTextView = rootView.findViewById<View>(R.id.categoryTextView) as TextView
        categoryImageView = rootView.findViewById<View>(R.id.categoryImageView) as ImageView
    }

    var checked: Boolean = false
        set(value) {
            field = value
            if (value) {
                categoryCardView.setCardBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.selectedRed
                    )
                )
                categoryImageView.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary))
                categoryTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))

            } else {
                categoryCardView.setCardBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorSecondaryContainer
                    )
                )
                categoryImageView.setColorFilter(ContextCompat.getColor(context, R.color.dark_gray))
                categoryTextView.setTextColor(ContextCompat.getColor(context, R.color.dark_gray))

            }
        }

    var title: String = ""
        set(value) {
            field = value
            categoryTextView.text = value
        }

    var image: String = ""
    set(value) {
        field = value
        Picasso.get().load(value).into(categoryImageView)
    }


}