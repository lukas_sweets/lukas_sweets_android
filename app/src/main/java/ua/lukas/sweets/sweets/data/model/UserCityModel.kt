package ua.lukas.sweets.sweets.data.model

data class UserCityModel(
    val userCity: String?,

    val cities: List<String>
)
