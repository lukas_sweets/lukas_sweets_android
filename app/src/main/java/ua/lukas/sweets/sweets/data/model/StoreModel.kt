package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ua.lukas.sweets.sweets.data.api.StoreApiModel
import ua.lukas.sweets.sweets.data.api.UserApiModel

@Entity(tableName = "stores")
data class StoreModel(
    @PrimaryKey
    val id: Int,
    val time_work: String?,
    val lat: String?,
    val lon: String?,
    val address: String,
    val phone: String?,
    val city_id: String
) {
//    override fun toString(): String = address
}
fun StoreApiModel.toDBModel() : StoreModel {
    return StoreModel(
        this.id,
        this.time_work,
        this.lat,
        this.lon,
        this.address,
        this.phone_number,
        this.city_id
    )
}