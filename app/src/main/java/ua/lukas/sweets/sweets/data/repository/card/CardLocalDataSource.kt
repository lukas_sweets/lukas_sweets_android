package ua.lukas.sweets.sweets.data.repository.card

import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.data.db.CardDao
import ua.lukas.sweets.sweets.data.model.CardModel

interface CardLocalDataSourceInterface {
    suspend fun getBonusCard(): CardModel?
    suspend fun getDiscountCard(): CardModel?
    suspend fun addCards(cards: ArrayList<CardModel>)
    suspend fun hasBonusCards(): Boolean
}

class CardLocalDataSource(
    private val cardDao: CardDao = LukasSweetsApp.getDatabase().cardsDao()
) : CardLocalDataSourceInterface {

    override suspend fun getBonusCard(): CardModel? {
        return cardDao.getBonusCard()
    }

    override suspend fun getDiscountCard(): CardModel? {
        return cardDao.getDiscountCard()
    }

    override suspend fun addCards(cards: ArrayList<CardModel>) {
        cardDao.addCards(cards)
    }

    override suspend fun hasBonusCards(): Boolean {
        return cardDao.hasBonusCard()
    }

}