package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class PackApiModel(
    val pack: Pack,
    val analog: List<Analog>,
)

data class Pack(
    val id: Int,
    val product_name: String,
    val article: String,
    val image: String,
    val name: String,
    val short_name: String,
    val description: String,
    val category: String,
    val expiration_date: String,
    val energy_value: Double,
    val composition: String,
    val storage_conditions: String,
    val proteins: Double,
    val fats: Double,
    val carbs: Double,
    val more_nutritional_values: String?,
    val allergens: List<String>,
    val price: Double,


)

data class Analog(
    val id: Int,
    val name: String,
    val current: Boolean,
)