package ua.lukas.sweets.sweets.view.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetBarcodeBinding
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetEmployeeCardBinding


class EmployeeCardModalBottomSheet(
    private val cardNumber: String
) : BottomSheetDialogFragment() {

    private lateinit var binding: ModalBottomSheetEmployeeCardBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ModalBottomSheetEmployeeCardBinding.inflate(layoutInflater)
        return binding.root
    }

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior
        modalBottomSheetBehavior.state =  BottomSheetBehavior.STATE_COLLAPSED
        setData()

    }

    private fun setData() {
        binding.barcodeImageView.viewTreeObserver.addOnGlobalLayoutListener {
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.encodeBitmap(cardNumber, BarcodeFormat.CODE_128, binding.barcodeImageView.measuredWidth, binding.barcodeImageView.measuredHeight)
            binding.barcodeImageView.setImageBitmap(bitmap)
        }

        binding.barcodeTextView.text = cardNumber

    }

}