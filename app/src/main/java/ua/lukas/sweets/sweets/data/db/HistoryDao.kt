package ua.lukas.sweets.sweets.data.db

import androidx.room.*
import ua.lukas.sweets.sweets.data.model.HistoryAdapterModel
import ua.lukas.sweets.sweets.data.model.HistoryModel

@Dao
interface HistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addHistories(history: ArrayList<HistoryModel>)

    @Query("DELETE FROM histories")
    fun deleteAllHistories()

    @Query("SELECT * FROM histories ORDER BY date_time DESC LIMIT :limit")
    fun getHistories(limit: Int): List<HistoryAdapterModel>

    @Query("SELECT * FROM histories WHERE id = :id")
    fun getHistory(id: Int): HistoryModel

    @Query("SELECT COUNT(id) FROM histories")
    fun getCountHistories(): Int
}