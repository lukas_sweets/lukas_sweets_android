package ua.lukas.sweets.sweets.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ua.lukas.sweets.sweets.data.model.StoreModel

@Dao
interface StoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addStores(store: ArrayList<StoreModel>)

    @Query("DELETE FROM stores")
    suspend fun deleteStores()

    @Query("SELECT * FROM stores WHERE id = :id")
    fun getStore(id: Int) : StoreModel

    @Query("SELECT * FROM stores")
    fun getStores() : List<StoreModel>

    @Query("SELECT address FROM stores WHERE id = :store_code")
    fun getAddressStore(store_code: Int): String

    @Query("SElECT city_id FROM stores GROUP BY city_id")
    fun getCities(): List<String>

    @Query("SELECT * FROM stores WHERE city_id = :city")
    fun getStoresInCity(city: String): List<StoreModel>


}