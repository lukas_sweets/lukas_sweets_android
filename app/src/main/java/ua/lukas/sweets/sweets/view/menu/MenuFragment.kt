package ua.lukas.sweets.sweets.view.menu

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.marcoscg.dialogsheet.DialogSheet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.BuildConfig
import ua.lukas.sweets.sweets.MainActivity
import ua.lukas.sweets.sweets.view.menu.adapter.MenuAdapter
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.databinding.FragmentMenuBinding
import ua.lukas.sweets.sweets.view.menu.adapter.Menus
import ua.lukas.sweets.sweets.view.store.StoresActivity


class MenuFragment : Fragment() {

    private var countDev = 0
    private lateinit var _binding: FragmentMenuBinding
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMenuBinding.inflate(inflater, container, false)
        return binding.root
    }


    private lateinit var recyclerView: RecyclerView
    private lateinit var menuAdapter: MenuAdapter
    private var menus: MutableList<Menus> = mutableListOf()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.materialToolbar.inflateMenu(R.menu.menu_menu)

        applyCityInMenu()

        CoroutineScope(Dispatchers.Default).launch {
            menus.add(Menus(1, resources.getString(R.string.menu_profile), R.drawable.ic_user))
            menus.add(Menus(2, resources.getString(R.string.stores), R.drawable.ic_store))

            if ((activity as MainActivity).getDiscountCard() != null) {
                menus.add(
                    Menus(
                        3,
                        resources.getString(R.string.employee_card),
                        R.drawable.ic_employees_card
                    )
                )
            }
//            menus.add(Menus(4, resources.getString(R.string.menu_company), R.drawable.ic_company))
            menus.add(Menus(5, resources.getString(R.string.support), R.drawable.ic_support))
            menus.add(Menus(6, resources.getString(R.string.menu_exit), R.drawable.ic_exit))

            withContext(Dispatchers.Main) {
                // устанавливаем для списка адаптер
                menuAdapter = MenuAdapter(requireContext(), menus, activity as MainActivity)
                recyclerView.adapter = menuAdapter
            }
        }

        recyclerView = view.findViewById<View>(R.id.recycleMenu) as RecyclerView

        // создаем адаптер
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(requireActivity().applicationContext)
        recyclerView.layoutManager = layoutManager


        val itemDecor = DividerItemDecoration(context, LinearLayout.VERTICAL)
        recyclerView.addItemDecoration(itemDecor)

        val version = view.findViewById<TextView>(R.id.textView14)
        version.text =
            resources.getString(R.string.menu_developer_version, BuildConfig.VERSION_NAME)

        version.setOnClickListener {
            if (countDev == 9) {
                openAlertDev()
                countDev = 0
            } else {
                countDev += 1
            }
        }
        //END onViewCreated
    }

    private fun applyCityInMenu() {
        val menu: Menu = binding.materialToolbar.menu
        CoroutineScope(Dispatchers.Default).launch {
            val userCity = (activity as MainActivity).getUserCity()
            withContext(Dispatchers.Main) {
                val menuView = menu.findItem(R.id.city).actionView
                val cityTextView = menuView?.findViewById<TextView>(R.id.cityTextView)
                cityTextView?.text = userCity ?: context?.resources?.getString(R.string.your_city)
                menuView?.setOnClickListener {
                    Intent(context, StoresActivity::class.java).apply {
                        context?.startActivity(this)
                    }
                }
            }
        }
    }


    private fun openAlertDev() {
        DialogSheet(activity)
            .setTitle(resources.getString(R.string.menu_developer_title))
            .setMessage(resources.getString(R.string.menu_developer_text) + "\n\n")
            .setIconResource(R.drawable.ic_raccoon)
            .setCancelable(false)
            .setRoundedCorners(true)
            .setButtonsColorRes(R.color.red)
            .setPositiveButton(android.R.string.ok) { }
            .show()


    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showLuckyWheelBanner()
        recyclerView.adapter = MenuAdapter(requireContext(), menus, activity as MainActivity)
        applyCityInMenu()
    }

    //END FRAGMENT
}