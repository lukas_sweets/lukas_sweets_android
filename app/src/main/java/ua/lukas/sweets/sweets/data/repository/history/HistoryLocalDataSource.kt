package ua.lukas.sweets.sweets.data.repository.history

import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.data.db.HistoryDao
import ua.lukas.sweets.sweets.data.model.HistoryAdapterModel
import ua.lukas.sweets.sweets.data.model.HistoryDtModel
import ua.lukas.sweets.sweets.data.model.HistoryModel
import ua.lukas.sweets.sweets.data.model.history.dt.HistoryDTDao

interface HistoryLocalDataSourceInterface {
    suspend fun addHistories(histories: ArrayList<HistoryModel>)
    suspend fun deleteAllHistories()
    suspend fun getHistories(limit: Int): List<HistoryAdapterModel>
    suspend fun getCountHistories(): Int
    suspend fun getHistory(history_id: Int): HistoryModel
    suspend fun getHistoryDts(history_id: Int): List<HistoryDtModel>
    suspend fun deleteHistoryDts(history_id: Int)
    suspend fun addHistoryDt(historyDt: HistoryDtModel)

}

class HistoryLocalDataSource(
    private val historyDao: HistoryDao = LukasSweetsApp.getDatabase().historiesDao(),
    private val historyDtsDao: HistoryDTDao = LukasSweetsApp.getDatabase().historyDTsDao(),
) : HistoryLocalDataSourceInterface {

    override suspend fun addHistories(histories: ArrayList<HistoryModel>) {
        historyDao.addHistories(histories)
    }

    override suspend fun deleteAllHistories() {
        historyDao.deleteAllHistories()
        historyDtsDao.deleteAllHistoryDts()
    }

    override suspend fun getHistories(limit: Int): List<HistoryAdapterModel> {
        return historyDao.getHistories(limit)
    }

    override suspend fun getCountHistories(): Int {
        return historyDao.getCountHistories()
    }

    override suspend fun getHistory(history_id: Int): HistoryModel {
        return historyDao.getHistory(history_id)
    }

    override suspend fun getHistoryDts(history_id: Int): List<HistoryDtModel> {
        return historyDtsDao.getHistoryDTs(history_id)
    }

    override suspend fun deleteHistoryDts(history_id: Int) {
        return historyDtsDao.deleteHistoryDTs(history_id)
    }

    override suspend fun addHistoryDt(historyDt: HistoryDtModel) {
        historyDtsDao.addHistoryDT(historyDt)
    }


}