package ua.lukas.sweets.sweets.view

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.InputFilter
import android.text.InputType
import android.view.*
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.example.tastytoast.TastyToast
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kaopiz.kprogresshud.KProgressHUD
import com.marcoscg.dialogsheet.DialogSheet
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.registrations.RegisterPhoneActivity
import ua.lukas.sweets.sweets.data.api.UserApiModel
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.sms.SMSRepository
import ua.lukas.sweets.sweets.data.repository.user.UserRepository
import ua.lukas.sweets.sweets.databinding.ProfileLayoutBinding
import java.util.*


class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ProfileLayoutBinding

    private val userRepository = UserRepository()
    private val preferences = MyPreferences()
    private val smsRepository = SMSRepository()
    private val cardRepository = CardRepository()
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null

    private lateinit var phone: String

    private var sexS: Int = 0
    private var positionSex = -1
    private var calendarBirthday: Calendar = Calendar.getInstance()
    private lateinit var picker: DatePickerDialog
    private var changeSex = 0
    private var send_eco_check = false
    private var enterEmail = false

    private lateinit var edittext: EditText
    private var intentFilter: IntentFilter? = null
    private var smsReceiver: SMSReceiver? = null
    private val myCode = "6257"
    private var code: String = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProfileLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        binding.collapsingToolbarLayout.title = resources.getString(R.string.profile_title)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.profile_title)

        alerts = Alerts(this)
        loadDialog = alerts.load()

//        binding.loadingLayout.root.visibility = View.GONE

        binding.checkBox2.setOnClickListener {
            send_eco_check = binding.checkBox2.isChecked == true
        }

        HideKeyboard.setupUI(binding.parent, this)


        enterEmail = intent.getBooleanExtra("enterEmail", false)

        if (enterEmail) {
            binding.mailEdit.requestFocus()

            send_eco_check = true

            binding.mailEdit.postDelayed({
                val keyboard: InputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                keyboard.showSoftInput(binding.mailEdit, 0)
            }, 500)
            binding.checkBox2.isChecked = true
        }



        binding.sexRadioGroup.setOnCheckedChangeListener { radioGroup, i ->
            val radioButton = radioGroup.findViewById<View>(i)
            val index = radioGroup.indexOfChild(radioButton)
            positionSex = index + 1

            if (changeSex >= 3) {
                changesSex()
                changeSex = 0
            } else {
                changeSex += 1
            }
        }


        /***********************
        // Download data from DB
         ************************/

        loadDialog?.show()
        CoroutineScope(Dispatchers.Default).launch {
            val user = userRepository.getUser()
            withContext(Dispatchers.Main) {
                loadDialog?.dismiss()
                with(user) {
                    with(binding) {
                        nameEdit.setText(name)
                        surnameEdit.setText(surname)
                        lastnameEdit.setText(patronymic)
                        birthdayEdit.setText(birthday?.toDateWithDots())
                        mailEdit.setText(email)
                        if (sex != 0 && sex < 3) {
                            (sexRadioGroup.getChildAt(sex - 1) as RadioButton).isChecked =
                                true
                        }

                        birthday?.let { birthday ->
                            calendarBirthday.time = birthday.toDate()
                        }

                        if (send_eco_check) {
                            checkBox2.isChecked = true
                            ecoTextView.text = resources.getString(R.string.eco_check_text_ok)
                            this@ProfileActivity.send_eco_check = true
                        }

                        phoneEdit.setText("38${phone}")
                        this@ProfileActivity.phone = phone
                        setupDatePicker()
                    }
                }
            }
        }

        binding.birthdayEdit.inputType = InputType.TYPE_NULL
        binding.birthdayEdit.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                HideKeyboard.hideSoftKeyboard(this@ProfileActivity)
                picker.show()
            }
        }

        binding.deleteProfileButton.setOnClickListener {
            DialogSheet(this)
                .setTitle(resources.getString(R.string.attention))
                .setMessage(resources.getString(R.string.delete_card_text) + "\n\n")
                //.setIconResource(R.drawable.ic_raccoon)
                .setCancelable(false)
                .setRoundedCorners(true)
                .setButtonsColorRes(R.color.red)
                .setPositiveButton(android.R.string.ok) {
                    sendCodeToDeleteCard()
                }
                .setNegativeButton(resources.getString(R.string.cancel)) {
                }
                .show()

        }

    }

    private fun checkData() {
        if (enterEmail || send_eco_check) {
            if (binding.mailEdit.text?.isEmpty() == true) {
                binding.mailEdit.error = resources.getString(R.string.enter_email)
                binding.mailEdit.requestFocus()
                return
            }
        }

        if (binding.nameEdit.text?.isEmpty() == true) {
            binding.nameTextInput.error = resources.getString(R.string.profile_enter_name)

        } else {
            loadDialog?.show()
            binding.nameTextInput.error = null
            val newName = binding.nameEdit.text.toString().trim()
            val newSecondName: String? =
                binding.lastnameEdit.text.toString().trim().ifEmpty { null }
            val newLastname: String? = binding.surnameEdit.text.toString().trim().ifEmpty { null }
            val newBirthday = binding.birthdayEdit.text.toString().toApiDate()
            val newEmail =
                binding.mailEdit.text.toString().replace(" ", "").trim().ifEmpty { null }


            val newSex: Int = if (positionSex != -1) {
                positionSex
            } else {
                sexS
            }

            if (!send_eco_check) preferences.hideEcoCheck = false


            saveData(
                UserApiModel(
                    phone,
                    newName,
                    newLastname,
                    newSecondName,
                    newSex,
                    newBirthday,
                    newEmail,
                    send_eco_check,
                    null
                )
            )
        }
    }


    private fun saveData(user: UserApiModel) {
        loadDialog?.show()
        CoroutineScope(Dispatchers.Default).launch {
            val response = userRepository.updateUser(user)
            withContext(Dispatchers.Main) {
                loadDialog?.dismiss()
                when (response) {
                    ResponseCode.OK -> {
                        TastyToast.success(
                            this@ProfileActivity,
                            resources.getString(R.string.ok_data),
                            TastyToast.LENGTH_SHORT,
                            TastyToast.SHAPE_ROUND,
                            false
                        );
                        finish()
                    }
                    else -> {
                        alerts.showAlerterNoInternet()
                    }
                }
            }
        }
    }


    private fun notification() {
        binding.checkBox.isChecked =
            NotificationManagerCompat.from(this).areNotificationsEnabled()

        binding.checkBox.setOnClickListener {

            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                val uri: Uri = Uri.fromParts("package", packageName, null)
                data = uri
                startActivity(this)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        binding.nameEdit.clearFocus()

        notification()

    }


    private fun changesSex() {
        DialogSheet(this)
            .setTitle(resources.getString(R.string.profile_sex))
            .setMessage(
                resources.getString(R.string.profile_change_sex) + " " + resources.getString(
                    R.string.think_emoji
                ) + "\n\n"
            )
            .setCancelable(false)
            .setRoundedCorners(true)
            .setButtonsColorRes(R.color.red)
            .setPositiveButton(R.string.yes) {}
            .setNegativeButton(R.string.no) {}
            .show()

    }


    @SuppressLint("SetTextI18n")
    private fun setupDatePicker() {

        val day = calendarBirthday[Calendar.DAY_OF_MONTH]
        val month = calendarBirthday[Calendar.MONTH]
        val year = calendarBirthday[Calendar.YEAR]

        // date picker dialog
        picker = DatePickerDialog(
            this@ProfileActivity, R.style.datepicker,
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                binding.birthdayEdit.setText(
                    String.format("%02d", dayOfMonth) + "." + String.format(
                        "%02d",
                        monthOfYear + 1
                    ) + "." + year
                )
                binding.birthdayEdit.clearFocus()
            }, year, month, day
        )
        picker.setOnCancelListener { binding.birthdayEdit.clearFocus() }
    }


    private fun sendCodeToDeleteCard() {
        loadDialog?.show()
        CoroutineScope(Dispatchers.Default).launch {
            code = String.format("%04d", Random().nextInt(10000))
            val response = smsRepository.sendOtpCode(
                code,
                preferences.phone
            )
            withContext(Dispatchers.Main) {
                loadDialog?.dismiss()
                when (response) {
                    ResponseCode.OK -> {
                        deleteCard()
                    }
                    else -> {
                        alerts.showAlerterNoInternet()
                    }
                }
            }
        }
    }

    private fun deleteCard() {
        initSmsListener()
        initBroadCast()
        registerReceiver(smsReceiver, intentFilter)

        edittext = EditText(this)

        edittext.inputType = InputType.TYPE_CLASS_NUMBER
        edittext.gravity = Gravity.CENTER
        edittext.setTextColor(
            ContextCompat.getColor(
                this,
                android.R.color.black
            )
        )
        edittext.background = null
        edittext.textSize = 30F
        edittext.letterSpacing = 0.3F
        edittext.isCursorVisible = false
        edittext.hint = "- - - -"
        edittext.filters =
            arrayOf<InputFilter>(InputFilter.LengthFilter(4))

        val builder = MaterialAlertDialogBuilder(this)
        with(builder) {
            setTitle(R.string.confirm_code)
            setMessage(R.string.confirm_code_text)
            setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                if (edittext.text.toString() == code || edittext.text.toString() == myCode) {
                    //delete
                    loadDialog?.show()
                    CoroutineScope(Dispatchers.Default).launch {
                        cardRepository.getBonusCard()?.let { bonus_card ->
//                            val response = cardRepository.deleteCard(bonus_card.card_number)
                            val response = ResponseCode.OK
                            withContext(Dispatchers.Main) {
                                loadDialog?.dismiss()
                                when (response) {
                                    ResponseCode.OK -> {

                                        CoroutineScope(Dispatchers.Default).launch {
                                            LukasSweetsApp.getDatabase().clearAllTables()

                                            withContext(Dispatchers.Main){
                                                preferences.clear()
                                                Intent(context, RegisterPhoneActivity::class.java).apply {
                                                    context.startActivity(this)
                                                }
//                                                context.startActivity(Intent(context, RegisterPhoneActivity::class.java))
                                            }
                                        }

                                    }
                                    else -> {
                                        alerts.showAlerterNoInternet()
                                    }
                                }
                            }
                        } ?: run {
                            alerts.error()
                        }

                    }
                } else {
                    wrongCodeAlert()
                }
            }
            setNegativeButton(R.string.cancel, null)
        }

        val dialogs = builder.create()
        dialogs.setView(edittext)
        edittext.requestFocus()

        dialogs.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        dialogs.show()

        val button = dialogs.getButton(DialogInterface.BUTTON_POSITIVE)
        with(button) {
            setTextColor(
                ContextCompat.getColor(
                    this@ProfileActivity,
                    R.color.dark_gray
                )
            )
        }

        val buttonNegative =
            dialogs.getButton(DialogInterface.BUTTON_NEGATIVE)
        with(buttonNegative) {
            setTextColor(
                ContextCompat.getColor(
                    this@ProfileActivity,
                    R.color.red
                )
            )
        }
    }

    private fun initBroadCast() {
        intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        smsReceiver = SMSReceiver()
        smsReceiver?.setOTPListener(object : SMSReceiver.OTPReceiveListener {
            override fun onOTPReceived(otp: String?) {
                //showToast("OTP Received: $otp")
                edittext.setText(otp)
            }
        })
    }

    private fun initSmsListener() {
        val client = SmsRetriever.getClient(this)
        client.startSmsRetriever()
    }

    override fun onDestroy() {
        super.onDestroy()
        smsReceiver = null
    }

    override fun onStop() {
        super.onStop()
        try {
            if (smsReceiver != null) {
                unregisterReceiver(smsReceiver)
            }
        } catch (_: Exception) {
        }
    }

    private fun wrongCodeAlert() {
        DialogSheet(this)
            .setTitle(resources.getString(R.string.error))
            .setMessage(resources.getString(R.string.error_error_code) + "\n\n")
            //.setIconResource(R.drawable.ic_raccoon)
            .setCancelable(false)
            .setRoundedCorners(true)
            .setButtonsColorRes(R.color.red)
            .setPositiveButton(android.R.string.ok) {
            }
            .show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        if (item.itemId == R.id.save) {
            checkData()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.save_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


}