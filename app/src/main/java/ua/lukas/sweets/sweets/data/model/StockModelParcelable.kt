package ua.lukas.sweets.sweets.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StockModelParcelable(
    val id: Int,
    val name: String,
    val short_name: String?,
    val description: String,
    val old_price: String?,
    val new_price: String?,
    val time: String,
    val picture: String,
    val top_position: Boolean,
    val show_label: Boolean,
    val label_text: String?,
    val label_color: String?,
    val label_bg_color: String?,
    val order: Int?,
    val productsPackId: Int?
): Parcelable
