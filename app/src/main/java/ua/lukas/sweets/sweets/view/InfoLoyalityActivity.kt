package ua.lukas.sweets.sweets.view

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import ua.lukas.sweets.sweets.databinding.InfoLoyalityActivityBinding

class InfoLoyalityActivity : AppCompatActivity() {

    private lateinit var binding: InfoLoyalityActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = InfoLoyalityActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        binding.faqCardView.setOnClickListener {
            val intent = Intent(this@InfoLoyalityActivity, SupportActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
