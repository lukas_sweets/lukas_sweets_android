package ua.lukas.sweets.sweets.module

import android.content.res.Resources
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun Int.dpToPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

fun String.toDateWithDots(): String {
    return run {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        var dates: Date? = null
        try {
            dates = dateFormat.parse(this)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        simpleDateFormat.format(dates)
    }
}

fun String.toApiDate(): String? {
    if (this.isEmpty()){
        return null
    } else {
        return run {
            val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            var dates: Date? = null
            try {
                dates = dateFormat.parse(this)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            if (dates != null) {
                simpleDateFormat.format(dates)
            } else {
                null
            }
        }
    }
}

fun String.toDate() : Date {
    val formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return  formatter.parse(this) as Date
}
