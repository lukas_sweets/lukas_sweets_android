package ua.lukas.sweets.sweets.view.catalog

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ua.lukas.sweets.sweets.module.HideKeyboard
import ua.lukas.sweets.sweets.data.api.ProductAvailabilityApiModel
import ua.lukas.sweets.sweets.data.model.UserCityModel
import ua.lukas.sweets.sweets.databinding.FragmentAvailabilityProductBinding
import ua.lukas.sweets.sweets.view.catalog.adapter.ProductAvailabilityAdapter
import ua.lukas.sweets.sweets.viewmodel.ProductAvailabilityViewModel
import ua.lukas.sweets.sweets.viewmodel.UserCityViewModel

class AvailabilityProductFragment : Fragment() {

    private lateinit var binding: FragmentAvailabilityProductBinding
    private val productAvailabilityViewModel: ProductAvailabilityViewModel by activityViewModels()
    private val userCityViewModel: UserCityViewModel by activityViewModels()


    private lateinit var adapter: ProductAvailabilityAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAvailabilityProductBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ProductAvailabilityAdapter(requireContext(), arrayListOf())
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(requireContext())
        binding.recyclerView.layoutManager = layoutManager

        binding.recyclerView.adapter = adapter
        setupViewModel()
    }

    private fun setupViewModel() {
        productAvailabilityViewModel.availabilityProductModel.observe(viewLifecycleOwner) { data ->
            setupData(data)
        }

        userCityViewModel.listOfCities.observe(viewLifecycleOwner) {data ->
            settingStores(data)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun settingStores(data: UserCityModel) {
        val adapterKiosk = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            data.cities
        )


        binding.cityEdit.apply {
            setAdapter(adapterKiosk)
            if (data.userCity != null) {
                setText(data.userCity, false)
//                getStoresCity(user.city_id!!)
            }

            keyListener = null
            setOnTouchListener { v, event ->
                HideKeyboard.hideSoftKeyboard(requireActivity())
                (v as AutoCompleteTextView).showDropDown()
                false
            }
            setOnItemClickListener { parent, view, position, rowId ->
                (activity as ProductCatalog).changeCity(parent.getItemAtPosition(position) as String)
                //getStoresCity(parent.getItemAtPosition(position) as String)
//                CoroutineScope(Dispatchers.Default).launch {
//                    userRepository.updateUserCity(
//                        parent.getItemAtPosition(position) as String,
//                        user.phone
//                    )
//                    cardRepository.downloadCards()
//
                }
                clearFocus()
            }
        }

    private fun setupData(in_data: List<ProductAvailabilityApiModel>?) {
        in_data?.let { data ->
            adapter.update(data)
        } ?: run {

        }
    }

    override fun onResume() {
        super.onResume()
        binding.root.requestLayout()
    }

}