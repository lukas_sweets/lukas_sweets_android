package ua.lukas.sweets.sweets.data.model

import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import ua.lukas.sweets.sweets.data.api.NewsApiModel

data class NewsModel(
    val title: String,
    val validity: String,
    val text_news: String,
    val picture: RequestCreator,
    val picture_url: String,
    val button_show: Boolean,
    val button_text: String?,
    val button_url: String?,
)

fun NewsApiModel.toDBModel(): NewsModel {
    return NewsModel(
        this.title,
        this.validity,
        this.text_news,
        Picasso.get().load(this.picture),
        this.picture,
        this.button_show,
        this.button_text,
        this.button_url
    )
}