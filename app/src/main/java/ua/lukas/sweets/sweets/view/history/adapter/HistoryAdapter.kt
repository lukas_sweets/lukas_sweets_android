package ua.lukas.sweets.sweets.view.history.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ua.lukas.sweets.sweets.module.DateTime
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.HistoryAdapterModel
import ua.lukas.sweets.sweets.view.history.HistoryDTActivity
import java.util.*


class HistoryAdapter(private val context: Context, private val historys: List<ListItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var size = -1

    interface ListItem {
        val itemType: Int

        companion object {
            const val TYPE_ITEM = 0
            const val TYPE_HEADER = 1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v_head = LayoutInflater.from(parent.context).inflate(R.layout.header_layout, parent, false)
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)

        if (viewType == ListItem.TYPE_HEADER) {
            return VHHeader(v_head)
        } else if (viewType == ListItem.TYPE_ITEM) {
            return VHItem(v)
        }
        throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHHeader) {
            val header = historys[position] as HeaderRecycleView
            val upperMonth = header.name.substring(0, 1).uppercase() + header.name.substring(1).lowercase()
            holder.tvName.text = upperMonth
            holder.yearTextView.text = header.year
        } else if (holder is VHItem) {
            val history = historys[position] as HistoryAdapterModel
            holder.dateView.text = DateTime.dateTitle(history.date_time)
            holder.timeView.text = DateTime.timeTitle(history.date_time)
            holder.adressView.text = history.store
            holder.summaView.text = "%.2f ₴".format(history.sum_total)//history.sum_total.toString() + " ₴"
            if (history.store == null) {
                holder.adressView.text = history.comment
                holder.summaView.text = "🔥"
            }


            if (history.bonus_total > 0) {
                holder.bonusView.text = "+" + history.bonus_total.toString()
                holder.bonusView.setTextColor(ContextCompat.getColor(context, R.color.green))
            } else if (history.bonus_total < 0) {
                holder.bonusView.text = history.bonus_total.toString()
                holder.bonusView.setTextColor(ContextCompat.getColor(context, R.color.red))
            } else {
                holder.bonusView.text = history.bonus_total.toString()
                holder.bonusView.setTextColor(ContextCompat.getColor(context, R.color.yellow))
            }

            if(history.store != null) {
                holder.itemView.setOnClickListener {
                    val intent = Intent(context, HistoryDTActivity::class.java)
                    intent.putExtra("id", history.id)
                    context.startActivity(intent)
                }
            }

        }




    }

    override fun getItemViewType(position: Int): Int {
        return historys[position].itemType
    }

    override fun getItemCount(): Int {
        return size
    }

    class VHItem internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        var dateView: TextView
        var summaView: TextView
        var adressView: TextView
        var bonusView: TextView
        var timeView: TextView

        init {
            dateView = view.findViewById(R.id.dateTextView)
            timeView = view.findViewById(R.id.timeTextView)
            summaView = view.findViewById(R.id.summaTextView)
            adressView = view.findViewById(R.id.adressTextView)
            bonusView = view.findViewById(R.id.bonusTextView)
        }
    }

    class VHHeader internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val tvName: TextView
        val yearTextView: TextView

        init {
            tvName = itemView.findViewById(R.id.header)
            yearTextView = itemView.findViewById(R.id.yearTextView)
        }
    }

    companion object {
//        private lateinit var thiscontext: Context
        fun getNewListItem(context: Context, history: List<ListItem>?): List<ListItem> {
            val newList: MutableList<ListItem> = ArrayList()
            var currentMonth = ""
            if (!history.isNullOrEmpty()){
                for (i in history.indices) {
                    if (history[i].javaClass.name == HistoryAdapterModel::class.java.name) {
                        val histor = history[i] as HistoryAdapterModel
                        if (!currentMonth.contains(
                                DateTime.getMonth(
                                    DateTime.stringTOdate(histor.date_time),
                                    context.resources.getString(R.string.locale)
                                )
                            )
                        ) {
                            newList.add(
                                HeaderRecycleView(
                                    DateTime.getMonth(
                                        DateTime.stringTOdate(histor.date_time),
                                        context.resources.getString(R.string.locale)
                                    ),
                                    DateTime.getYear(
                                        DateTime.stringTOdate(histor.date_time)
                                    )
                                )
                            )
                            newList.add(history[i])
                            currentMonth = DateTime.getMonth(
                                DateTime.stringTOdate(histor.date_time),
                                context.resources.getString(R.string.locale)
                            )
                        } else {
                            newList.add(history[i])
                        }
                    }
                }
            }
            return newList
        }
    }


    init {
//        thiscontext = context
        size = historys.size
    }
}