package ua.lukas.sweets.sweets.view.catalog

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.tastytoast.TastyToast
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.kaopiz.kprogresshud.KProgressHUD
import com.marcoscg.dialogsheet.DialogSheet
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.UpdateConfiguration
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.selector.off
import io.fotoapparat.selector.torch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.repository.catalog.CatalogRepository
import ua.lukas.sweets.sweets.databinding.ScanBarcodeActivityBinding

class ScanProductActivity : AppCompatActivity() {
    private lateinit var binding: ScanBarcodeActivityBinding

    private lateinit var fotoapparat: Fotoapparat
    private val barcodeScanner: BarcodeScanner = BarcodeScanning.getClient()

    private val catalogRepository = CatalogRepository()
    private lateinit var dialog: KProgressHUD
    private var scannedBarcode: String? = null
    private var flashLight = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ScanBarcodeActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        dialog = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)

        activityResultLauncher.launch(
            arrayOf(Manifest.permission.CAMERA)
        )
        initFrameProcessor()

        onBackPressedDispatcher.addCallback(this) {
            finish()
        }
    }


    private fun initFrameProcessor() {
        fotoapparat = Fotoapparat.with(this)
            .into(binding.cameraView)
            .previewScaleType(ScaleType.CenterCrop)
            .frameProcessor { it ->
                val inputImage = InputImage.fromByteArray(
                    it.image,
                    it.size.width,
                    it.size.height,
                    it.rotation,
                    InputImage.IMAGE_FORMAT_NV21
                )
                barcodeScanner.process(inputImage)
                    .addOnSuccessListener { barCodes ->
                        if (barCodes.isNotEmpty() and scannedBarcode.isNullOrEmpty()) {
                            scannedBarcode = barCodes.first().rawValue
                            fotoapparat.stop()
                            getPackFromBarcode(barCodes.first().rawValue.toString())
                        }
                    }
                    .addOnFailureListener {
                        println(">>>>>>>>>>>> Error")
                        it.printStackTrace()
                    }
            }
            .build()
    }

    override fun onStart() {
        super.onStart()
        fotoapparat.start()
    }

    override fun onStop() {
        super.onStop()
        fotoapparat.stop()
    }

    override fun onResume() {
        super.onResume()
        Log.e("", "onResume")
        initFrameProcessor()
        fotoapparat.start()
    }

    private fun getPackFromBarcode(barcode: String) {
        dialog.show()
        CoroutineScope(Dispatchers.Default).launch {
            val packID = catalogRepository.getPackFromBarcode(barcode)
            withContext(Dispatchers.Main) {
                dialog.dismiss()
                if (packID == 0) {
                    TastyToast.error(
                        this@ScanProductActivity,
                        resources.getString(R.string.product_not_find),
                        TastyToast.LENGTH_SHORT,
                        TastyToast.SHAPE_ROUND,
                        false
                    );
                    finish()
                } else {
                    val intent = Intent(this@ScanProductActivity, ProductCatalog::class.java)
                    intent.putExtra("pack_id", packID)
                    startActivity(intent)
                }
            }
        }
    }

    private val activityResultLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        )
        { permissions ->
            // Handle Permission granted/rejected
            permissions.entries.forEach {
                val permissionName = it.key
                val isGranted = it.value
                if (isGranted) {
                    // Permission is granted
                } else {
                    DialogSheet(this)
                        .setTitle(resources.getString(R.string.permission))
                        .setMessage(resources.getString(R.string.permission_text))
                        .setCancelable(false)
                        .setRoundedCorners(true)
                        .setButtonsColorRes(R.color.red)
                        .setPositiveButton(R.string.yes,
                            DialogSheet.OnPositiveClickListener {
                                Intent(ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:${packageName}")).apply {
                                    addCategory(Intent.CATEGORY_DEFAULT)
                                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    startActivity(this)
                                }
                            })
                        .setNegativeButton(R.string.no,
                            DialogSheet.OnNegativeClickListener {
                                finish()
                            })
                        .show()
                }
            }
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.flash -> {
                flashLight = !flashLight
                fotoapparat.updateConfiguration(
                    UpdateConfiguration(
                        flashMode = if (flashLight) torch() else off()
                    )
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.scan_product_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}