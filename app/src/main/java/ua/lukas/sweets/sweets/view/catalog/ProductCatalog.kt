package ua.lukas.sweets.sweets.view.catalog

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.kaopiz.kprogresshud.KProgressHUD
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.Alerts
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.PackApiModel
import ua.lukas.sweets.sweets.data.model.UserCityModel
import ua.lukas.sweets.sweets.data.model.UserModel
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.catalog.CatalogRepository
import ua.lukas.sweets.sweets.data.repository.store.StoreRepository
import ua.lukas.sweets.sweets.data.repository.user.UserRepository
import ua.lukas.sweets.sweets.databinding.ActivityProductCatalogBinding
import ua.lukas.sweets.sweets.view.catalog.adapter.AllergensProductAdapter
import ua.lukas.sweets.sweets.view.catalog.adapter.PacksAdapter
import ua.lukas.sweets.sweets.view.catalog.adapter.ProductModalBottomSheet
import ua.lukas.sweets.sweets.view.connectionDialog.NoConnectionDialog
import ua.lukas.sweets.sweets.viewmodel.ProductAvailabilityViewModel
import ua.lukas.sweets.sweets.viewmodel.UserCityViewModel

interface DataProduct {
    fun changeCity(city_id: String)
}

class ProductCatalog : AppCompatActivity(), DataProduct {

    private lateinit var binding: ActivityProductCatalogBinding
    private val availabilityViewModel: ProductAvailabilityViewModel by viewModels()
    private val cityViewModel: UserCityViewModel by viewModels()

    private var catalogRepository = CatalogRepository()
    private var cardRepository = CardRepository()
    private val userRepository = UserRepository()
    private val storeRepository = StoreRepository()
    private lateinit var alerts: Alerts
//    private var loadDialog: KProgressHUD? = null
    private lateinit var adapterPacks: PacksAdapter
    private lateinit var adapterAllergens: AllergensProductAdapter
    private var userModel: UserModel? = null
    private var packID: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_catalog)
        setContentView(binding.root)

        packID = intent.getIntExtra("pack_id", 0)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        binding.mainView.visibility = View.GONE
        binding.progressBar.visibility = View.GONE

        val modalBottomSheet = ProductModalBottomSheet()

        binding.availabilityButton.setOnClickListener {
            modalBottomSheet.show(supportFragmentManager, ProductModalBottomSheet.TAG)
        }

        alerts = Alerts(this)
//        loadDialog = alerts.load(cancellable = true)

        adapterPacks = PacksAdapter(this, arrayListOf())
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.packsRecyclerView.layoutManager = layoutManager
        binding.packsRecyclerView.adapter = adapterPacks

        adapterPacks.setPackClickListener(object : PacksAdapter.ItemOnClickListener {
            override fun onPackClick(id: Int) {
                getProduct(id, userModel?.city_id)
                availabilityProduct(userModel?.city_id, id)
            }
        })

        adapterAllergens = AllergensProductAdapter(this, arrayListOf())
        val layoutManager2 = FlexboxLayoutManager(this)
        layoutManager2.flexDirection = FlexDirection.ROW
        layoutManager2.justifyContent = JustifyContent.FLEX_START
        binding.allergensRecyclerView.layoutManager = layoutManager2
        binding.allergensRecyclerView.adapter = adapterAllergens

        onBackPressedDispatcher.addCallback(this) {
            finish()
        }

    }

    override fun onResume() {
        super.onResume()
        CoroutineScope(Dispatchers.Default).launch {
            userModel = userRepository.getUser()
            withContext(Dispatchers.Main) {
                getProduct(packID, userModel?.city_id)
                availabilityProduct(userModel?.city_id, packID)
                getCities()
            }
        }
    }

    private fun getProduct(pack_id: Int, city_id: String?) {
//            loadDialog?.show()
        binding.progressBar.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Default).launch {
            val (response, data) = catalogRepository.getPack(pack_id, city_id)
            withContext(Dispatchers.Main) {
//                loadDialog?.dismiss()
                binding.progressBar.visibility = View.GONE

                if (response == ResponseCode.ERROR || data == null) {
                    alerts.showAlerterNoInternet()
                    finish()
                } else {
                    setData(data)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData(data: PackApiModel) {
        binding.mainView.visibility = View.VISIBLE
        binding.product = data.pack
        Picasso.get().load(data.pack.image).into(binding.packImageView)

        if (data.pack.price == 0.0)
            binding.priceLinearLayout.visibility = View.INVISIBLE
        else
            binding.priceTextView.text =
                resources.getString(R.string.grn_text, "%.2f".format(data.pack.price))
        adapterPacks.updateList(data.analog)

        adapterAllergens.updateList(data.pack.allergens)
    }

    private fun availabilityProduct(city_id: String?, pack_id: Int) {
        CoroutineScope(Dispatchers.Default).launch {

            withContext(Dispatchers.Main) {
                availabilityViewModel.setAvailabilityProduct(null)
            }

            city_id?.let { city_id ->
                val (_, array) = catalogRepository.getAvailabilityProduct(city_id, pack_id)
                withContext(Dispatchers.Main) {
                    availabilityViewModel.setAvailabilityProduct(array)
                }
            } ?: run {
                // NO user city
                withContext(Dispatchers.Main) {
                    availabilityViewModel.setAvailabilityProduct(null)
                }
            }
        }
    }

    private fun getCities() {
        CoroutineScope(Dispatchers.Default).launch {
            val cities = storeRepository.getCities()

            withContext(Dispatchers.Main) {
                cityViewModel.update(UserCityModel(userModel?.city_id, cities))
            }
        }
    }

    override fun changeCity(city_id: String) {
        CoroutineScope(Dispatchers.Default).launch {
            // 1. Get availability pack
            availabilityProduct(city_id, packID)

            // 2. Save new city for user
            userModel?.let { user ->
                userRepository.updateUserCity(
                    city_id,
                    user.phone
                )
            }
            cardRepository.downloadCards()

            withContext(Dispatchers.Main){
                getProduct(packID, city_id)
            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}