package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class UserCardsApiModel(
//    @SerializedName("user")
    val user: UserApiModel,

//    @SerializedName("card")
    val card: ArrayList<CardApiModel>
)
