package ua.lukas.sweets.sweets.view.news

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.module.ifLet
import ua.lukas.sweets.sweets.data.model.NewsModelParcelable
import ua.lukas.sweets.sweets.databinding.OneNewsLayoutBinding


class OneNewsActivity : AppCompatActivity() {

    private lateinit var binding: OneNewsLayoutBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OneNewsLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val news = if (Build.VERSION.SDK_INT >= 33) {
            intent.getParcelableExtra("data", NewsModelParcelable::class.java)
        } else {
            intent.getParcelableExtra("data")
        }

        news?.let {
            with(binding) {
                with(news) {
                    supportActionBar?.title = title
                    binding.collapsingToolbarLayout.title = title

                    dateTextView.text = validity
                    descriptionTextView.text = text_news

                    button.visibility = View.GONE

                    if (button_show) {
                        ifLet(button_text, button_url) { (text, url) ->
                            button.visibility = View.VISIBLE
                            button.text = text
                            button.setOnClickListener {
                                Intent(Intent.ACTION_VIEW, Uri.parse(url)).apply {
                                    startActivity(this)
                                }
                            }
                        }
                    }

                    Picasso.get().load(picture).into(image)
                }
            }
        } ?: run {
            finish()
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}