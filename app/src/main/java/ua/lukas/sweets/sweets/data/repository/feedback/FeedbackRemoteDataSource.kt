package ua.lukas.sweets.sweets.data.repository.feedback

import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.FeedbackApiModel
import ua.lukas.sweets.sweets.data.model.FeedbackModel
import ua.lukas.sweets.sweets.data.model.NewFeedbackModel
import java.util.concurrent.TimeUnit

interface FeedbackRemoteDataSourceInterface {
    suspend fun downloadFeedback(
        readMessage: List<Int> = arrayListOf()
    ): Pair<ResponseCode, ArrayList<FeedbackApiModel>>

    suspend fun sendNewFeedback(feedback: NewFeedbackModel): ResponseCode
    suspend fun sendAnswerFeedback(feedback: FeedbackModel): ResponseCode
}

class FeedbackRemoteDataSource(
    private val api: Api = Api(),
    private val preferences: MyPreferences = MyPreferences(),
) : FeedbackRemoteDataSourceInterface {


    override suspend fun downloadFeedback(
        readMessage: List<Int>
    ): Pair<ResponseCode, ArrayList<FeedbackApiModel>> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.feedback)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("phone", preferences.phone)
            .addQueryParameter("read_messages", readMessage.joinToString(","))
            .build()
        val response = request.executeForObjectList(FeedbackApiModel::class.java)


        if (response.isSuccess) {
            return Pair(ResponseCode.OK, response.result as ArrayList<FeedbackApiModel>)
        }

        return Pair(ResponseCode.ERROR, arrayListOf())
    }

    override suspend fun sendNewFeedback(feedback: NewFeedbackModel): ResponseCode {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.post(api.newFeedback)
            .setOkHttpClient(okHttpClient)
            .addBodyParameter(feedback)
            .build()
        val response = request.executeForString()


        if (response.isSuccess)
            return ResponseCode.OK
        else
            Log.e("TAG", response.error.errorBody.toString())


        return ResponseCode.ERROR
    }

    override suspend fun sendAnswerFeedback(feedback: FeedbackModel): ResponseCode {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.post(api.answerFeedback)
            .setOkHttpClient(okHttpClient)
            .addBodyParameter(feedback)
            .build()
        val response = request.executeForString()


        if (response.isSuccess)
            return ResponseCode.OK
        else
            Log.e("TAG", response.error.errorBody.toString())


        return ResponseCode.ERROR
    }

}