package ua.lukas.sweets.sweets.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ua.lukas.sweets.sweets.data.model.UserModel

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUser(user: UserModel)

    @Query("DELETE FROM users")
    fun deleteUsers()

    @Query("SELECT * FROM users")
    fun getUser(): UserModel

    @Query("SELECT send_eco_check FROM users")
    fun getUserEcoCheck(): Boolean

    @Query("SELECT city_id FROM users")
    fun getUserCity(): String?
}