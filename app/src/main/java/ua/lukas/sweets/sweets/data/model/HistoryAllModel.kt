package ua.lukas.sweets.sweets.data.model



data class HistoryAllModel(
    val id: Int,
    val date_time: String,
    val sum_total: Double,
    val bonus_total: Int,
    var store: String?,
    val comment: String?,
    val reason: Int,
    val qr: String?,
    val num: String,
    val bonus_add: Int,
    val pay_cash: Double,
    val pay_card: Double,
    val pay_bonus: Double,
    val pay_certificate: Double,
    val details: List<HistoryDtModel>
)


