package ua.lukas.sweets.sweets.data.repository.news

import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.model.NewsModel
import ua.lukas.sweets.sweets.data.model.toDBModel

interface NewsRepositoryInterface {
    suspend fun downloadNews(): Pair<ResponseCode, ArrayList<NewsModel>>
}

class NewsRepository(
    private val newsRemoteDataSource: NewsRemoteDataSource = NewsRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
    ) : NewsRepositoryInterface {

    override suspend fun downloadNews(): Pair<ResponseCode, ArrayList<NewsModel>> = withContext(scope.coroutineContext) {
            val (response, newsArray) = newsRemoteDataSource.getNews()

            val modelNews: ArrayList<NewsModel> = arrayListOf()
            for (news in newsArray)
                modelNews.add(news.toDBModel())

            return@withContext Pair(response, modelNews)
        }
}