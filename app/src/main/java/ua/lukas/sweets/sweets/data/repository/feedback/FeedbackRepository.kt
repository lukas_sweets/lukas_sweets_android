package ua.lukas.sweets.sweets.data.repository.feedback

import android.util.Log
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.model.FeedbackModel
import ua.lukas.sweets.sweets.data.model.NewFeedbackModel
import ua.lukas.sweets.sweets.data.model.toDBModel

interface FeedbackRepositoryInterface {
    suspend fun downloadFeedbacksAsync(
        readMessage: List<Int> = arrayListOf()
    ): Deferred<ResponseCode>

    suspend fun getNewFeedbacksCount(): Int
    suspend fun sendNewFeedback(feedback: NewFeedbackModel): ResponseCode
    suspend fun getFeedbacks(reload: Boolean = false): Pair<ResponseCode, List<FeedbackModel>>
    suspend fun hasNewFeedback(feedback_id: Int): Boolean
    suspend fun getFeedbackMessages(feedback_id: Int): List<FeedbackModel>
    suspend fun getNewMessageFromFeedback(feedback_id: Int): List<Int>
    suspend fun sendAnswerFeedback(feedback: FeedbackModel):ResponseCode
}

class FeedbackRepository(
    private val feedbackRemoteDataSource: FeedbackRemoteDataSource = FeedbackRemoteDataSource(),
    private val feedbackLocalDataSource: FeedbackLocalDataSource = FeedbackLocalDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : FeedbackRepositoryInterface {

    private val TAG = "FeedbackRepository"


    override suspend fun downloadFeedbacksAsync(
        readMessage: List<Int>
    ): Deferred<ResponseCode> = scope.async {

        val (response, arrayFeedback) = feedbackRemoteDataSource.downloadFeedback(
            readMessage
        )

        if (response == ResponseCode.OK) {
            feedbackLocalDataSource.deleteAllFeedbacks()
        }
        val feedbackDB: ArrayList<FeedbackModel> = arrayListOf()
        for (feedback in arrayFeedback)
            feedbackDB.add(feedback.toDBModel())

        feedbackLocalDataSource.addFeedbacks(feedbackDB)

        Log.e(TAG, "downloadStoresAsync")
        return@async response
    }

    override suspend fun getNewFeedbacksCount(): Int = withContext(scope.coroutineContext) {
        return@withContext feedbackLocalDataSource.getNewFeedbacksCount()
    }

    override suspend fun sendNewFeedback(feedback: NewFeedbackModel): ResponseCode =
        withContext(scope.coroutineContext) {
            return@withContext feedbackRemoteDataSource.sendNewFeedback(feedback)
        }

    override suspend fun getFeedbacks(reload: Boolean): Pair<ResponseCode, List<FeedbackModel>> =
        withContext(scope.coroutineContext) {
            var response = ResponseCode.OK
            if (reload)
                response = downloadFeedbacksAsync().await()

            return@withContext Pair(response, feedbackLocalDataSource.getFeedbacks())
        }

    override suspend fun hasNewFeedback(feedback_id: Int): Boolean =
        withContext(scope.coroutineContext) {
            return@withContext feedbackLocalDataSource.hasNewFeedback(feedback_id)
        }

    override suspend fun getFeedbackMessages(feedback_id: Int): List<FeedbackModel> = withContext(scope.coroutineContext) {
        return@withContext feedbackLocalDataSource.getFeedbackMessages(feedback_id)
    }

    override suspend fun getNewMessageFromFeedback(feedback_id: Int): List<Int> = withContext(scope.coroutineContext) {
        return@withContext feedbackLocalDataSource.getNewMessageFromFeedback(feedback_id)
    }

    override suspend fun sendAnswerFeedback(feedback: FeedbackModel) = withContext(scope.coroutineContext){
        return@withContext feedbackRemoteDataSource.sendAnswerFeedback(feedback)
    }


}