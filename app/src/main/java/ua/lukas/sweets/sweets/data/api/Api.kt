package ua.lukas.sweets.sweets.data.api


interface ApiInterface {
    val client: String
    val client_city: String
    val card: String
    val store: String
    val stock: String
    val history: String
    val historyDt: String
    val feedback: String
    val newFeedback: String
    val answerFeedback: String
    val news: String
    val otp: String
    val catalog: String
    val catalogPack: String
    val catalogAvailability: String
    val catalogFindBarcode: String
    val luckyWheelCount: String
    val luckyWheelCoupons: String
    val luckyWheelStart: String
}

class Api : ApiInterface {
    val api = "https://sweets.lukas.ua/api"
    val version = "/v1"
    val path = api + version


    override val client: String
        get() = "$path/client"

    override val client_city: String
        get() = "$path/client/city"

    override val card: String
        get() = "$path/cards"

    override val store: String
        get() = "$path/stores/export"

    override val stock: String
        get() = "$path/stocks/export"

    override val history: String
        get() = "$path/histories"

    override val historyDt: String
        get() = "$path/history/details"

    override val feedback: String
        get() = "$path/feedback/mobile"

    override val newFeedback: String
        get() = "$path/feedback/new"

    override val answerFeedback: String
        get() = "$path/feedback/answer"

    override val news: String
        get() = "$path/news/export"

    override val otp: String
        get() = "$api/sms/otp"

    override val catalog: String
        get() = "$path/catalog"

    override val catalogPack: String
        get() = "$path/catalog/pack"

    override val catalogAvailability: String
        get() = "$path/catalog/availabilities"

    override val catalogFindBarcode: String
        get() = "$path/catalog/find"

    override val luckyWheelCount: String
        get() = "$path/wheel/count"

    override val luckyWheelCoupons: String
        get() = "$path/wheel/coupons"

    override val luckyWheelStart: String
        get() = "$path/wheel/start"

}