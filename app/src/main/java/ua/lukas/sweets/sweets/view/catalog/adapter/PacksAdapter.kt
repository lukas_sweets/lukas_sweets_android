package ua.lukas.sweets.sweets.view.catalog.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.view.ContextThemeWrapper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.Analog
import ua.lukas.sweets.sweets.databinding.ListItemAnalogPacksBinding

class PacksAdapter(
    private val context: Context,
    private var packs: List<Analog>
) : RecyclerView.Adapter<PacksAdapter.PackItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackItemViewHolder {
        val binding =
            ListItemAnalogPacksBinding.inflate(LayoutInflater.from(context), parent, false)
        return PackItemViewHolder(binding, listener)
    }

    interface ItemOnClickListener {
        fun onPackClick(id: Int)
    }

    private lateinit var listener: ItemOnClickListener

    fun setPackClickListener(listener: ItemOnClickListener) {
        this.listener = listener
    }


    override fun onBindViewHolder(holder: PackItemViewHolder, position: Int) {
        val item = packs[position]
        holder.bind(item, context)
    }

    override fun getItemCount(): Int {
        return packs.size
    }

    fun updateList(list: List<Analog>) {
        packs = list
        notifyDataSetChanged()
    }


    class PackItemViewHolder(
        categoryItemLayoutBinding: ListItemAnalogPacksBinding,
        private val listener: ItemOnClickListener
    ) :
        RecyclerView.ViewHolder(categoryItemLayoutBinding.root) {

        private val binding = categoryItemLayoutBinding

        fun bind(packItem: Analog, context: Context) {
            binding.parent.removeAllViews()

            val attr = if (packItem.current)
                R.attr.materialButtonStyle
            else
                R.attr.materialIconButtonFilledTonalStyle

            val btnTag = MaterialButton(context, null, attr)


            btnTag.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            btnTag.text = packItem.name
            binding.parent.addView(btnTag)

            btnTag.setOnClickListener {
                listener.onPackClick(packItem.id)
            }

        }


    }
}