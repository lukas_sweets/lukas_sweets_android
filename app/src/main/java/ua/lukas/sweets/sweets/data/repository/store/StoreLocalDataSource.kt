package ua.lukas.sweets.sweets.data.repository.store

import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.data.db.StoreDao
import ua.lukas.sweets.sweets.data.model.StoreModel

interface StoreLocalDataSourceInterface {
    suspend fun addStores(stores: ArrayList<StoreModel>)
    suspend fun deleteAllStores()
    suspend fun getMapStores(): List<StoreModel>
    suspend fun getStore(id: Int): StoreModel
    suspend fun getCities(): List<String>
    suspend fun getStoresInCity(city: String): List<StoreModel>
}

class StoreLocalDataSource(
    private val storeDao: StoreDao = LukasSweetsApp.getDatabase().storesDao()
) : StoreLocalDataSourceInterface {

    override suspend fun addStores(stores: ArrayList<StoreModel>) {
        storeDao.addStores(stores)
    }

    override suspend fun deleteAllStores() {
        storeDao.deleteStores()
    }

    override suspend fun getMapStores(): List<StoreModel> {
        return storeDao.getStores()
    }

    override suspend fun getStore(id: Int): StoreModel {
        return storeDao.getStore(id)
    }

    override suspend fun getCities(): List<String> {
        return storeDao.getCities()
    }

    override suspend fun getStoresInCity(city: String): List<StoreModel> {
        return storeDao.getStoresInCity(city)
    }

}