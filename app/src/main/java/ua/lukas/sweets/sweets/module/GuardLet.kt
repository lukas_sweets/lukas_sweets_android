package ua.lukas.sweets.sweets.module


inline fun <T: Any> guardLet (vararg elements: T?, closure: () -> Nothing) : List<T> {
    return if (elements.all {it != null}) {
        elements.filterNotNull()
    } else {
        closure()
    }
}

inline fun <T: Any> ifLet(vararg args: T?, closure: (List<T>) -> Unit): Unit? {
    return if (args.all { it != null }) closure.invoke(args.filterNotNull())
    else null
}

