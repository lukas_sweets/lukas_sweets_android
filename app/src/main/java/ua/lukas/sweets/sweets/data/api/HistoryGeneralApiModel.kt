package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class HistoryGeneralApiModel(
    val countHistory: Int,
    val history: ArrayList<HistoryApiModel>,

)
