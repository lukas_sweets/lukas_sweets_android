package ua.lukas.sweets.sweets.data.model.history.dt

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ua.lukas.sweets.sweets.data.model.HistoryDtModel

@Dao
interface HistoryDTDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addHistoryDT(historyDT: HistoryDtModel)

    @Query("DELETE FROM history_dts WHERE history_id = :history_id")
    suspend fun deleteHistoryDTs(history_id: Int)

    @Query("SELECT * FROM history_dts WHERE history_id = :history_id")
    suspend fun getHistoryDTs(history_id: Int) : List<HistoryDtModel>

    @Query("DELETE FROM history_dts")
    suspend fun deleteAllHistoryDts()


}