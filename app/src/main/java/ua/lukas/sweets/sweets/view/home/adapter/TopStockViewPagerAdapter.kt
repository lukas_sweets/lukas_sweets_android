package ua.lukas.sweets.sweets.view.home.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.view.stock.StockActivity
import ua.lukas.sweets.sweets.data.model.StockModel
import ua.lukas.sweets.sweets.data.model.StockModelParcelable
import ua.lukas.sweets.sweets.databinding.ViewTopStockBinding


class TopStockViewPagerAdapter(
    private val context: Context,
    private val list: List<StockModel>
) : RecyclerView.Adapter<TopStockViewPagerAdapter.TopStockViewHolder>() {

    inner class TopStockViewHolder(val binding: ViewTopStockBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopStockViewHolder {
        val binding = ViewTopStockBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return TopStockViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TopStockViewHolder, position: Int) {
        with(holder) {
            with(list[position]) {
                binding.labelTextView.text = label_text
                Picasso.get().load(picture).into(binding.stockImageView)
                var bgText = ""
                for (i in 0..500) {
                    bgText += "$short_name "
                }
                binding.bgTextView.text = bgText
                binding.root.setOnClickListener {
                    Intent(context, StockActivity::class.java).apply {
                        putExtra(
                            "data", StockModelParcelable(
                                id,
                                name,
                                short_name,
                                description,
                                old_price,
                                new_price,
                                time,
                                picture,
                                top_position,
                                show_label,
                                label_text,
                                label_color,
                                label_bg_color,
                                order,
                                productsPackId
                            )
                        )
                        context.startActivity(this)
                    }
                }
            }
        }
    }

    override fun getItemCount() = list.size

}