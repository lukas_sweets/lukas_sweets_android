package ua.lukas.sweets.sweets.view.menu.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.allenliu.badgeview.BadgeFactory
import com.marcoscg.dialogsheet.DialogSheet
import com.marcoscg.dialogsheet.DialogSheet.OnNegativeClickListener
import com.marcoscg.dialogsheet.DialogSheet.OnPositiveClickListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.dpToPx
import ua.lukas.sweets.sweets.view.ProfileActivity
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.registrations.RegisterPhoneActivity
import ua.lukas.sweets.sweets.view.SupportActivity
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.feedback.FeedbackRepository
import ua.lukas.sweets.sweets.view.CompanyActivity
import ua.lukas.sweets.sweets.view.menu.EmployeeCardModalBottomSheet
import ua.lukas.sweets.sweets.view.store.StoresActivity


class MenuAdapter(
    private val context: Context,
    private val menu: List<Menus>,
    private val activity: Activity
) :
    RecyclerView.Adapter<MenuAdapter.ViewHolder>() {
    private val preferences = MyPreferences()
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val feedbackRepository = FeedbackRepository()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = inflater.inflate(R.layout.menu_row, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (id, name, image) = menu[position]
        holder.nameView.text = name
        holder.imageView.setImageResource(image)



        if (id == 5) {
            CoroutineScope(Dispatchers.Default).launch {
                val countNewFeedbacks = feedbackRepository.getNewFeedbacksCount()
                withContext(Dispatchers.Main) {
                    if (countNewFeedbacks > 0) {
                        BadgeFactory.createDot(context).bind(holder.imageView)
                        holder.imageView.setPadding(
                            12.dpToPx(),
                            12.dpToPx(),
                            12.dpToPx(),
                            12.dpToPx()
                        )
                    }
                }
            }
        }



        holder.itemView.setOnClickListener {
            when (id) {
                1 -> {
                    Intent(context, ProfileActivity::class.java).apply {
                        context.startActivity(this)
                    }
                }
                2 -> {
                    Intent(context, StoresActivity::class.java).apply {
                        context.startActivity(this)
                    }
                }
                3 -> {
                    val modalBottomSheet = EmployeeCardModalBottomSheet(preferences.employeeCardNumber)
                    modalBottomSheet.show((activity as AppCompatActivity).supportFragmentManager, EmployeeCardModalBottomSheet.TAG)
                }
                4 -> {
                    Intent(context, CompanyActivity::class.java).apply {
                        context.startActivity(this)
                    }
                }
                5 -> {
                    Intent(context, SupportActivity::class.java).apply {
                        context.startActivity(this)
                    }
                }
                6 -> {
                    showAlert()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return menu.size
    }

    class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageButton = view.findViewById(R.id.iconImageButton)
        val nameView: TextView = view.findViewById(R.id.textView12)
    }

    private fun showAlert() {
        DialogSheet(context)
            .setTitle(context.resources.getString(R.string.menu_exit))
            .setMessage(context.resources.getString(R.string.menu_accept_exit))
            .setCancelable(false)
            .setRoundedCorners(true)
            .setButtonsColorRes(R.color.red)
            .setPositiveButton(R.string.menu_exit_button, OnPositiveClickListener {
                preferences.clear()
                CoroutineScope(Dispatchers.IO).launch {
                    LukasSweetsApp.getDatabase().clearAllTables()
                }

                Intent(context, RegisterPhoneActivity::class.java).apply {
                    context.startActivity(this)
                }

//                context.startActivity(Intent(context, RegisterPhoneActivity::class.java))
            })
            .setNegativeButton(R.string.no,
                OnNegativeClickListener { })
            .show()

    }


}




data class Menus(
    val id: Int,
    val name: String,
    val image: Int
)

