package ua.lukas.sweets.sweets.data.repository.luckywheel

import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.*
import java.util.concurrent.TimeUnit

interface LuckyWheelRemoteDataSourceInterface {
    suspend fun getLuckyWheelCount(): Pair<ResponseCode, LuckyWheelCountApiModel>
    suspend fun getLuckyWheelCoupons(): Pair<ResponseCode, List<LuckyWheelCouponsApiModel>>

    suspend fun startWheel():Pair<ResponseCode, LuckyWheelPlayApiModel?>
}

class LuckyWheelRemoteDataSource(
    private val api: Api = Api(),
    private val preferences: MyPreferences = MyPreferences(),
) : LuckyWheelRemoteDataSourceInterface {

    override suspend fun getLuckyWheelCount(): Pair<ResponseCode, LuckyWheelCountApiModel> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.luckyWheelCount)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("phone", preferences.phone)
            .build()

        val response = request.executeForObject(LuckyWheelCountApiModel::class.java)

        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as LuckyWheelCountApiModel)
        else
            Log.e("TAG", response.error.toString())


        return Pair(ResponseCode.ERROR, LuckyWheelCountApiModel())
    }

    override suspend fun getLuckyWheelCoupons(): Pair<ResponseCode, List<LuckyWheelCouponsApiModel>> {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.luckyWheelCoupons)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("phone", preferences.phone)
            .build()

        val response = request.executeForObjectList(LuckyWheelCouponsApiModel::class.java)

        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as List<LuckyWheelCouponsApiModel>)
        else
            Log.e("TAG", response.error.toString())


        return Pair(ResponseCode.ERROR, arrayListOf())
    }

    override suspend fun startWheel(): Pair<ResponseCode, LuckyWheelPlayApiModel?> {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.luckyWheelStart)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("phone", preferences.phone)
            .build()

        val response = request.executeForObject(LuckyWheelPlayApiModel::class.java)

        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as LuckyWheelPlayApiModel)
        else
            Log.e("TAG", response.error.toString())


        return Pair(ResponseCode.ERROR, null)
    }
}