package ua.lukas.sweets.sweets.view.luckyWheel

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.annotation.OptIn
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import nl.dionsegijn.konfetti.core.*
import nl.dionsegijn.konfetti.core.emitter.Emitter
import ua.lukas.sweets.sweets.module.Alerts
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.LuckyWheelArticleApiModel
import ua.lukas.sweets.sweets.data.api.LuckyWheelPrisesApiModel
import ua.lukas.sweets.sweets.data.repository.luckywheel.LuckyWheelRepository
import ua.lukas.sweets.sweets.databinding.LuckyWheelActivityBinding
import ua.lukas.sweets.sweets.view.luckyWheel.adapter.LuckyWheelInfoModalBottomSheet
import ua.lukas.sweets.sweets.view.luckyWheel.adapter.LuckyWheelPriseModalBottomSheet
import ua.lukas.sweets.sweets.view.luckyWheel.lib.model.LuckyItem
import java.util.concurrent.TimeUnit


class LuckyWheelActivity : AppCompatActivity() {

    private val luckyWheelRepository = LuckyWheelRepository()
    private lateinit var alerts: Alerts

    private lateinit var binding: LuckyWheelActivityBinding
    private var codeList = listOf<LuckyWheelArticleApiModel>()
    private var couponsBadge: BadgeDrawable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LuckyWheelActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = ""

        alerts = Alerts(this)

        buttonPlayStatus(false)

        //Start Lucky Wheel
        val data: MutableList<LuckyWheelPrisesApiModel> = mutableListOf()
        for (i in 0..7) {
            val luckyItem = LuckyWheelPrisesApiModel(i, "", "")
            data.add(luckyItem)
        }

        setPrisesToWheel(data)

        binding.play.setOnClickListener {
            buttonPlayStatus(false)
            CoroutineScope(Dispatchers.Default).launch {
                val (response, model) = luckyWheelRepository.startWheel()

                withContext(Dispatchers.Main) {
                    when (response) {
                        ResponseCode.OK -> {
                            if (model != null) {
                                setPrisesToWheel(model.prises)
                                Handler(Looper.getMainLooper()).postDelayed(
                                    {
                                        binding.luckyWheel.startLuckyWheelWithTargetIndex(model.win_prise.index + 1)
                                    },
                                    1300 // value in milliseconds
                                )
                                binding.luckyWheel.setLuckyRoundItemSelectedListener {
                                    binding.konfettiView.start(
                                        listOf(
                                            Party(
                                                speed = 10f,
                                                angle = Angle.RIGHT - 45,
                                                maxSpeed = 30f,
                                                damping = 0.9f,
                                                spread = Spread.SMALL,
                                                colors = listOf(
                                                    0xfce18a,
                                                    0xff726d,
                                                    0xf4306d,
                                                    0xb48def
                                                ),
                                                emitter = Emitter(
                                                    duration = 5,
                                                    TimeUnit.SECONDS
                                                ).perSecond(50),
                                                position = Position.Relative(0.0, 0.2),
                                            ),
                                            Party(
                                                speed = 10f,
                                                angle = Angle.LEFT + 45,
                                                maxSpeed = 30f,
                                                damping = 0.9f,
                                                spread = Spread.SMALL,
                                                colors = listOf(
                                                    0xfce18a,
                                                    0xff726d,
                                                    0xf4306d,
                                                    0xb48def
                                                ),
                                                emitter = Emitter(
                                                    duration = 5,
                                                    TimeUnit.SECONDS
                                                ).perSecond(40),
                                                position = Position.Relative(1.0, 0.2),
                                            )
                                        )
                                    )
                                    buttonPlayStatus(true)
                                    val luckyWheelPriseModalBottomSheet =
                                        LuckyWheelPriseModalBottomSheet(model.win_prise)
                                    luckyWheelPriseModalBottomSheet.show(
                                        supportFragmentManager,
                                        luckyWheelPriseModalBottomSheet.tag
                                    )
                                    setCounts(model.count, model.coupons)
                                }
                            }
                        }
                        else -> {
                            alerts.showAlerterNoInternet()
                            buttonPlayStatus(true)
                        }
                    }
                }
            }
        }
    }

    private fun setPrisesToWheel(prisesList: List<LuckyWheelPrisesApiModel>?) {
        if (!prisesList.isNullOrEmpty()) {
            val arrayPriseItems: MutableList<LuckyItem> = mutableListOf()

            var cnt = 0
            val colorList = resources.getIntArray(R.array.luckyWheelColors).toList()
            for (prise in prisesList) {
                val luckyItem = LuckyItem()
                luckyItem.text = prise.name
                luckyItem.color =
                    colorList[cnt]
                arrayPriseItems.add(luckyItem)
                cnt = if (cnt + 1 > colorList.size - 1) 0 else cnt + 1
            }
            binding.luckyWheel.setData(arrayPriseItems)
        }
    }

    private fun buttonPlayStatus(status: Boolean) {
        if (status) {
            binding.play.isEnabled = true
            binding.play.setColorFilter(
                ContextCompat.getColor(
                    this,
                    R.color.blueButtonPlayLuckyWheel
                )
            );
        } else {
            binding.play.isEnabled = false
            binding.play.setColorFilter(Color.GRAY)
        }

    }

    private fun rotateBg() {
        val animation: Animation = AnimationUtils.loadAnimation(baseContext, R.anim.loop_rotate)
        animation.interpolator = LinearInterpolator()
        animation.repeatCount = Animation.INFINITE
        animation.duration = 30000
        binding.bgImageView.startAnimation(animation)
    }

    private fun animateStart() {
        val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.move_lucky_wheel)
        animation.startOffset = 800
        binding.luckyWheel.startAnimation(animation)
        binding.play.startAnimation(animation)

        val animation2 =
            AnimationUtils.loadAnimation(applicationContext, R.anim.move_logo_lucky_wheel)
        binding.logoImageView.startAnimation(animation2)

        val animation3 = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_lucky_wheel)
        animation3.startOffset = 1200
        binding.countTextView.startAnimation(animation3)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == R.id.info) {
            val luckyWheelInfoModalBottomSheet = LuckyWheelInfoModalBottomSheet(codeList)
            luckyWheelInfoModalBottomSheet.show(
                supportFragmentManager,
                luckyWheelInfoModalBottomSheet.tag
            )
        }
        if (id == R.id.coupons) {
            Intent(this, LuckyWheelCoupons::class.java).apply {
                startActivity(this)
            }
        }

        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        rotateBg()
        animateStart()
        getCountAndCouponsAndCodes()
    }


    private fun getCountAndCouponsAndCodes() {
        CoroutineScope(Dispatchers.Default).launch {
            val (_, model) = luckyWheelRepository.getLuckyWheelCount()
            codeList = model.article

            withContext(Dispatchers.Main) {
                setCounts(model.count, model.coupons)
            }
        }
    }

    @OptIn(markerClass = [ExperimentalBadgeUtils::class])
    private fun setCounts(wheel: Int, coupons: Int = 0) {
        if (wheel > 0) buttonPlayStatus(true) else buttonPlayStatus(false)

        binding.countTextView.text =
            resources.getString(R.string.lucky_wheel_count, wheel)

        couponsBadge = if (coupons > 0) {
            BadgeDrawable.create(this).also {
                BadgeUtils.attachBadgeDrawable(it, binding.materialToolbar, R.id.coupons)
            }
        } else {
            BadgeUtils.detachBadgeDrawable(
                couponsBadge,
                binding.materialToolbar,
                R.id.coupons
            )
            null
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.lucky_wheel_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

}

