package ua.lukas.sweets.sweets

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.activity.addCallback
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.zxing.BarcodeFormat
import com.google.zxing.oned.Code128Writer
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.view.registrations.RegisterPhoneActivity
import ua.lukas.sweets.sweets.data.api.CatalogApiModel
import ua.lukas.sweets.sweets.data.api.PackApiModel
import ua.lukas.sweets.sweets.data.api.UserApiModel
import ua.lukas.sweets.sweets.data.model.*
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.catalog.CatalogRepository
import ua.lukas.sweets.sweets.data.repository.feedback.FeedbackRepository
import ua.lukas.sweets.sweets.data.repository.history.HistoryRepository
import ua.lukas.sweets.sweets.data.repository.news.NewsRepository
import ua.lukas.sweets.sweets.data.repository.stock.StockRepository
import ua.lukas.sweets.sweets.data.repository.store.StoreRepository
import ua.lukas.sweets.sweets.data.repository.user.UserRepository
import ua.lukas.sweets.sweets.databinding.ActivityMainBinding
import ua.lukas.sweets.sweets.view.catalog.CatalogFragment
import ua.lukas.sweets.sweets.view.feedback.FeedbackFragment
import ua.lukas.sweets.sweets.view.home.HomeFragment
import ua.lukas.sweets.sweets.view.luckyWheel.LuckyWheelActivity
import ua.lukas.sweets.sweets.view.main.BarcodeModalBottomSheet
import ua.lukas.sweets.sweets.view.main.adapter.ViewPagerAdapterFragments
import ua.lukas.sweets.sweets.view.menu.MenuFragment


interface MainActivityInterface {
    fun showLuckyWheelBanner(show: Boolean = true)
    fun updateBadge()
    suspend fun getBonusCard(reload: Boolean = false): CardModel?
    suspend fun getDiscountCard(reload: Boolean = false): CardModel?
    suspend fun getNews(): Pair<ResponseCode, ArrayList<NewsModel>>
    suspend fun getStocks(): Pair<List<StockModel>, List<StockModel>>
    suspend fun getHistories(
        reload: Boolean = false,
        limit: Int = 10
    ): Pair<ResponseCode, List<HistoryAdapterModel>>

    suspend fun getCountHistories(): Int
    suspend fun getMapStoresAsync(): Deferred<List<StoreModel>>
    suspend fun getCitiesAsync(): Deferred<List<String>>
    suspend fun getStoresInCity(city: String): List<StoreModel>
    suspend fun getUserAsync(): Deferred<UserModel>
    suspend fun sendNewFeedback(feedbackModel: NewFeedbackModel): ResponseCode
    suspend fun getCatalog(reload: Boolean = false): Pair<ResponseCode, CatalogApiModel?>
    suspend fun getPack(pack_id: Int, city_id: String?): Pair<ResponseCode, PackApiModel?>
    suspend fun getUserCity(): String?
    suspend fun getUser(): UserModel
    suspend fun updateUser(user: UserApiModel): ResponseCode
}

class MainActivity : AppCompatActivity(), MainActivityInterface {

    private lateinit var binding: ActivityMainBinding

    private val scopeIO = CoroutineScope(Dispatchers.IO)
    private val preferences = MyPreferences()
    private val feedbackRepository = FeedbackRepository()
    private val cardRepository = CardRepository()
    private val newsRepository = NewsRepository()
    private val stockRepository = StockRepository()
    private val historyRepository = HistoryRepository()
    private val storeRepository = StoreRepository()
    private val userRepository = UserRepository()
    private val catalogRepository = CatalogRepository()

    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.viewpager2.isUserInputEnabled = false
        binding.viewpager2.offscreenPageLimit = 3


        setupViewPager()

//        bottomNavigationView = findViewById(R.id.bottomNavigationView)


        onBackPressedDispatcher.addCallback(this) {
            if (binding.viewpager2.currentItem == 0) {
                moveTaskToBack(true)
            } else {
                binding.viewpager2.setCurrentItem(0, false)
                binding.bottomNavigationView.menu.findItem(R.id.action_home).isChecked = true
            }
        }




        binding.bottomNavigationView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_home -> {
                    binding.viewpager2.setCurrentItem(0, false)
                    binding.bottomNavigationView.menu.findItem(R.id.action_home).isChecked = true
                }
                R.id.action_catalog -> {
                    binding.viewpager2.setCurrentItem(1, false)
                    binding.bottomNavigationView.menu.findItem(R.id.action_catalog).isChecked = true
                }
                R.id.action_feedback -> {
                    binding.viewpager2.setCurrentItem(2, false)
                    binding.bottomNavigationView.menu.findItem(R.id.action_feedback).isChecked =
                        true
                }
                R.id.action_menu -> {
                    binding.viewpager2.setCurrentItem(3, false)
                    binding.bottomNavigationView.menu.findItem(R.id.action_menu).isChecked = true
                }
                R.id.barcode -> {
                    val modalBottomSheet = BarcodeModalBottomSheet.instance(preferences.cardNumber)
                    modalBottomSheet.show(supportFragmentManager, BarcodeModalBottomSheet.TAG)
                }
            }
            false
        }


        binding.viewpager2.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                when (position) {
                    0 -> binding.bottomNavigationView.menu.findItem(R.id.action_home).isChecked =
                        true
                    1 -> binding.bottomNavigationView.menu.findItem(R.id.action_catalog).isChecked =
                        true
                    2 -> binding.bottomNavigationView.menu.findItem(R.id.action_feedback).isChecked =
                        true
                    3 -> binding.bottomNavigationView.menu.findItem(R.id.action_menu).isChecked =
                        true
                }
            }
        })

        binding.luckyWheelView.setOnClickListener {
            Intent(this, LuckyWheelActivity::class.java).apply {
                startActivity(this)
            }
        }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            permissionsBuilder(Manifest.permission.POST_NOTIFICATIONS).build().send()

    }


    override fun onResume() {
        super.onResume()
        setBadge()
    }

    override fun showLuckyWheelBanner(show: Boolean) {
        if (preferences.cardNumber == "0000000000025"
            || preferences.cardNumber == "0000000000008"
            || preferences.cardNumber == "0000000001994"
        )
            binding.luckyWheelView.visibility = if (show) View.VISIBLE else View.GONE
        else
            binding.luckyWheelView.visibility = View.GONE


    }


    override fun updateBadge() {
        CoroutineScope(Dispatchers.Default).launch {
            if (feedbackRepository.downloadFeedbacksAsync().await() == ResponseCode.OK) {
                withContext(Dispatchers.Main) {
                    setBadge()
                }
            }
        }
    }


    override suspend fun getNews(): Pair<ResponseCode, ArrayList<NewsModel>> =
        withContext(scopeIO.coroutineContext) {
            newsRepository.downloadNews()
        }

    override suspend fun getStocks(): Pair<List<StockModel>, List<StockModel>> =
        withContext(scopeIO.coroutineContext) {
            return@withContext stockRepository.getStocks()
        }

    override suspend fun getHistories(
        reload: Boolean,
        limit: Int
    ): Pair<ResponseCode, List<HistoryAdapterModel>> = withContext(scopeIO.coroutineContext) {
        return@withContext historyRepository.getHistories(reload, limit)
    }

    override suspend fun getCountHistories(): Int =
        withContext(scopeIO.coroutineContext) {
            return@withContext historyRepository.getCountHistories()
        }

    override suspend fun getMapStoresAsync(): Deferred<List<StoreModel>> =
        scopeIO.async {
            return@async storeRepository.getMapStores()
        }

    override suspend fun getCitiesAsync(): Deferred<List<String>> = scopeIO.async {
        return@async storeRepository.getCities()
    }

    override suspend fun getStoresInCity(city: String): List<StoreModel> =
        withContext(scopeIO.coroutineContext) {
            return@withContext storeRepository.getStoresInCity(city)
        }

    override suspend fun getUserAsync(): Deferred<UserModel> = scopeIO.async {
        return@async userRepository.getUser()
    }

    override suspend fun sendNewFeedback(feedbackModel: NewFeedbackModel): ResponseCode =
        withContext(scopeIO.coroutineContext) {
            return@withContext feedbackRepository.sendNewFeedback(feedbackModel)
        }

    override suspend fun getCatalog(reload: Boolean): Pair<ResponseCode, CatalogApiModel?> =
        withContext(scopeIO.coroutineContext) {
            return@withContext catalogRepository.getCatalog()
        }

    override suspend fun getPack(pack_id: Int, city_id: String?): Pair<ResponseCode, PackApiModel?> =
        withContext(scopeIO.coroutineContext) {
            return@withContext catalogRepository.getPack(pack_id, city_id)
        }

    override suspend fun getUserCity(): String? = withContext(scopeIO.coroutineContext) {
        return@withContext userRepository.getUserCity()
    }

    override suspend fun getUser(): UserModel = withContext(scopeIO.coroutineContext) {
        return@withContext userRepository.getUser()
    }

    override suspend fun updateUser(user: UserApiModel): ResponseCode {
        return userRepository.updateUser(user)
    }


    override suspend fun getBonusCard(reload: Boolean): CardModel? =
        withContext(scopeIO.coroutineContext) {

            if (reload)
                cardRepository.downloadCards()

            cardRepository.getBonusCard()?.let { data ->
                return@withContext data
            } ?: run {
                startActivity(Intent(this@MainActivity, RegisterPhoneActivity::class.java))
            }
            return@withContext null
        }

    override suspend fun getDiscountCard(reload: Boolean): CardModel? =
        withContext(scopeIO.coroutineContext) {
            return@withContext cardRepository.getDiscountCard()
        }


    private fun setBadge() {
        CoroutineScope(Dispatchers.Default).launch {
            val countNewMessage = feedbackRepository.getNewFeedbacksCount()
            withContext(Dispatchers.Main) {
                binding.bottomNavigationView
                val view = binding.bottomNavigationView.getOrCreateBadge(R.id.action_menu)
                view.backgroundColor = ContextCompat.getColor(this@MainActivity, R.color.red)
                if (countNewMessage > 0) {
                    view.isVisible = true
                } else {
                    view.clearNumber()
                    view.isVisible = false
                }
            }
        }
    }

    private fun setupViewPager() {
        val adapter =
            ViewPagerAdapterFragments(
                supportFragmentManager,
                lifecycle
            )
        val homeFragment = HomeFragment()
        val catalogFragment = CatalogFragment()
        val feedbackFragment = FeedbackFragment()
        val menuFragment = MenuFragment()
        adapter.addFragment(homeFragment)
        adapter.addFragment(catalogFragment)
        adapter.addFragment(feedbackFragment)
        adapter.addFragment(menuFragment)
        binding.viewpager2.adapter = adapter
    }


//End MainActivity
}