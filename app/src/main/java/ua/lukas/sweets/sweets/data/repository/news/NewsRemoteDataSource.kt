package ua.lukas.sweets.sweets.data.repository.news

import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.NewsApiModel
import java.util.concurrent.TimeUnit

interface NewsRemoteDataSourceInterface {
    suspend fun getNews(): Pair<ResponseCode, ArrayList<NewsApiModel>>
}

class NewsRemoteDataSource(
    private val api: Api = Api(),
) : NewsRemoteDataSourceInterface {

    override suspend fun getNews(): Pair<ResponseCode, ArrayList<NewsApiModel>> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.news)
            .setOkHttpClient(okHttpClient)
            .build()

        val response = request.executeForObjectList(NewsApiModel::class.java)
        if (response.isSuccess) {
            return Pair(ResponseCode.OK, response.result as ArrayList<NewsApiModel>)
        }

        return Pair(ResponseCode.ERROR, arrayListOf())
    }
}