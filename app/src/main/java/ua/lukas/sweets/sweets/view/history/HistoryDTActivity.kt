package ua.lukas.sweets.sweets.view.history

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.WriterException
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.HistoryDtModel
import ua.lukas.sweets.sweets.data.repository.history.HistoryRepository
import ua.lukas.sweets.sweets.databinding.ActivityHistoryDtBinding


class HistoryDTActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHistoryDtBinding
    private val historyRepository = HistoryRepository()
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null
    private var id_history = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryDtBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        alerts = Alerts(this)
        loadDialog = alerts.load()

        id_history = intent.getIntExtra("id", 0)


        binding.rowsRecycleView.layoutManager = LinearLayoutManager(this)

        getHistoryDT()

    }


    @SuppressLint("SetTextI18n")
    private fun getHistoryDT(reload: Boolean = false) {
        loadDialog?.show()
        CoroutineScope(Dispatchers.Default).launch {

            val (response, history) = historyRepository.getHistory(reload, id_history)
            withContext(Dispatchers.Main) {
                loadDialog?.dismiss()

                if (reload && response == ResponseCode.ERROR)
                    alerts.showAlerterNoInternet()

                binding.numCheckTextView.text = history.num
                binding.addressTextView.text = history.store
                binding.dateTextView.text = DateTime.dateFormat(
                    history.date_time, "dd.MM.yyyy HH:mm:ss", resources.getString(
                        R.string.locale
                    )
                )
                binding.totalTextView.text = "%.2f ₴".format(history.sum_total)//.toString() + "  ₴"
                binding.bonusTextView.text = history.bonus_add.toString()

                if (history.reason == 1) {
                    binding.reasonTextView.text = resources.getString(R.string.reason_sale)
                } else if (history.reason == 2) {
                    binding.reasonTextView.text = resources.getString(R.string.reason_refuse)
                    binding.reasonTextView.setTextColor(resources.getColor(R.color.red))
                }
                binding.rowsRecycleView.adapter = RowsHistoryDTAdapter(history.details)
                if (history.qr == null) {
                    binding.qrLinearLayout.visibility = View.GONE
                } else {
                    qr(history.qr)
                    binding.imageView7.setOnClickListener {
                        val browserIntent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(history.qr)
                        )
                        startActivity(browserIntent)
                    }
                }

                if (history.pay_cash != 0.0) {
                    binding.payCashLinearLayout.visibility = View.VISIBLE
                    binding.cashSumTextView.text =
                        "%.2f ₴".format(history.pay_cash)//.toString() + " ₴"
                }

                if (history.pay_card != 0.0) {
                    binding.payCardLinearLayout.visibility = View.VISIBLE
                    binding.cardSumTextView.text =
                        "%.2f ₴".format(history.pay_card)
                }

                if (history.pay_bonus != 0.0) {
                    binding.payBonusLinearLayout.visibility = View.VISIBLE
                    binding.bonusSumTextView.text =
                        "%.2f ₴".format(history.pay_bonus)
                }

                if (history.pay_certificate != 0.0) {
                    binding.payCertificateLinearLayout.visibility = View.VISIBLE
                    binding.certificateSumTextView.text =
                        "%.2f ₴".format(history.pay_certificate)
                }
                loadDialog?.dismiss()
            }
        }
    }


    fun qr(url: String) {

        val width = Resources.getSystem().displayMetrics.widthPixels;
        val height = Resources.getSystem().displayMetrics.heightPixels;

        var smallerDimension = if (width < height) width else height
        smallerDimension = smallerDimension * 3 / 4;

        val qrgEncoder = QRGEncoder(url, null, QRGContents.Type.TEXT, smallerDimension);
        try {
            val bitmap = qrgEncoder.bitmap;
            binding.imageView7.setImageBitmap(bitmap);
        } catch (e: WriterException) {
            Log.v(null, e.toString());
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        if (item.itemId == R.id.reload) {
            getHistoryDT(true)
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.update_history_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


}


class RowsHistoryDTAdapter(private val rowsList: List<HistoryDtModel>) :
    RecyclerView.Adapter<RowsHistoryDTAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var good: TextView? = null
        var cnt: TextView? = null
        var summ: TextView? = null

        init {
            good = itemView.findViewById(R.id.goodsTextView)
            cnt = itemView.findViewById(R.id.cntTextView)
            summ = itemView.findViewById(R.id.summTextView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_rows_history,
            parent,
            false
        )
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return rowsList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.good?.text = rowsList[position].goods_name
        holder.summ?.text = "%.2f ₴".format(rowsList[position].sum_total)//.toString() + " ₴"


        if ((rowsList[position].cnt - rowsList[position].cnt.toInt()) == 0.toDouble()) {
            holder.cnt?.text = rowsList[position].cnt.toInt().toString()
        } else {
            holder.cnt?.text = rowsList[position].cnt.toString()
        }


    }

}

