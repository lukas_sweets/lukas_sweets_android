package ua.lukas.sweets.sweets.data.api


data class ProductAvailabilityApiModel(
    val address: String,
    val time_work: String,
    val availabilities: Int,
)