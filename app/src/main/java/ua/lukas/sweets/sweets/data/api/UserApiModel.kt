package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName
import ua.lukas.sweets.sweets.data.model.UserModel

data class UserApiModel (

//    @SerializedName("phone")
    val phone: String,

//    @SerializedName("name")
    val name: String,

//    @SerializedName("surname")
    val surname: String?,

//    @SerializedName("patronymic")
    val patronymic: String?,

//    @SerializedName("sex")
    val sex: Int,

//    @SerializedName("birthday")
    var birthday: String?,

//    @SerializedName("email")
    val email: String?,

//    @SerializedName("send_eco_check")
    val send_eco_check: Boolean,

//    @SerializedName("city_id")
    val city_id: String?
)

