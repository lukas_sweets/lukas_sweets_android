package ua.lukas.sweets.sweets.data.repository.store

import android.util.Log
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.data.model.StoreModel
import ua.lukas.sweets.sweets.data.model.toDBModel

interface StoreRepositoryInterface {
    suspend fun downloadStoresAsync(): Deferred<ResponseCode>
    suspend fun getMapStores(): List<StoreModel>
    suspend fun getStore(id: Int): StoreModel
    suspend fun getCities(): List<String>
    suspend fun getStoresInCity(city: String): List<StoreModel>
}

class StoreRepository(
    private val storeLocalDataSource: StoreLocalDataSource = StoreLocalDataSource(),
    private val storeRemoteDataSource: StoreRemoteDataSource = StoreRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : StoreRepositoryInterface {

    private val TAG = "StoreRepository"

    override suspend fun downloadStoresAsync(): Deferred<ResponseCode> = scope.async {

        val (response, arrayStores) = storeRemoteDataSource.downloadStores()

        if (response == ResponseCode.OK) {
            storeLocalDataSource.deleteAllStores()
        }

        val storesDB: ArrayList<StoreModel> = arrayListOf()

        for (store in arrayStores) {
            storesDB.add(store.toDBModel())
        }
        storeLocalDataSource.addStores(storesDB)
        Log.e(TAG, "downloadStoresAsync")
        return@async response
    }


    override suspend fun getMapStores(): List<StoreModel> = withContext(scope.coroutineContext) {
        return@withContext storeLocalDataSource.getMapStores()
    }


    override suspend fun getStore(id: Int): StoreModel =
        withContext(scope.coroutineContext) {
            return@withContext storeLocalDataSource.getStore(id)
        }

    override suspend fun getCities(): List<String> = withContext(scope.coroutineContext){
        return@withContext storeLocalDataSource.getCities()
    }

    override suspend fun getStoresInCity(city: String): List<StoreModel> = withContext(scope.coroutineContext) {
        return@withContext storeLocalDataSource.getStoresInCity(city)
    }


}