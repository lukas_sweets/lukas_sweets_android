package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ua.lukas.sweets.sweets.view.history.adapter.HistoryAdapter
import ua.lukas.sweets.sweets.data.api.HistoryApiModel


interface HistoryModelInterface {
    fun toAllModel(historyDtModel: List<HistoryDtModel>) : HistoryAllModel
}

@Entity(tableName = "histories",
    indices = [
        Index(
            value = ["id"],
            unique = true
        )
    ])
data class HistoryModel(
    @PrimaryKey
    val id: Int,
    val date_time: String,
    val sum_total: Double,
    val bonus_total: Int,
    var store: String?,
    val comment: String?,
    val reason: Int,
    val qr: String?,
    val num: String,
    val bonus_add: Int,
    val pay_cash: Double,
    val pay_card: Double,
    val pay_bonus: Double,
    val pay_certificate: Double
): HistoryModelInterface {

    override fun toAllModel(historyDtModel: List<HistoryDtModel>): HistoryAllModel {
        return HistoryAllModel(
            this.id,
            this.date_time,
            this.sum_total,
            this.bonus_total,
            this.store,
            this.comment,
            this.reason,
            this.qr,
            this.num,
            this.bonus_add,
            this.pay_cash,
            this.pay_card,
            this.pay_bonus,
            this.pay_certificate,
            historyDtModel
        )
    }
}

data class HistoryAdapterModel(
    val id: Int,
    val date_time: String,
    val sum_total: Double,
    val bonus_total: Int,
    val store: String?,
    val comment: String?,
    val reason: Int,
    val qr: String?,
    val num: String,
    val bonus_add: Int,
    val pay_cash: Double,
    val pay_card: Double,
    val pay_bonus: Double,
    val pay_certificate: Double
) : HistoryAdapter.ListItem {
    override val itemType: Int
        get() = HistoryAdapter.ListItem.TYPE_ITEM
}

fun HistoryApiModel.toDBModel() : HistoryModel {
    return HistoryModel(
        this.id,
        this.date_time,
        this.sum_total,
        this.bonus_total,
        this.store,
        this.comment,
        this.reason,
        this.qr,
        this.num,
        this.bonus_add,
        this.pay_cash,
        this.pay_card,
        this.pay_bonus,
        this.pay_certificate
    )
}