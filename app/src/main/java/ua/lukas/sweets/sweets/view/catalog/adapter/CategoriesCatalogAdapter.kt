package ua.lukas.sweets.sweets.view.catalog.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ua.lukas.sweets.sweets.data.api.CategoriesModel
import ua.lukas.sweets.sweets.databinding.ListItemCategoryCatalogBinding

class CategoriesCatalogAdapter(
    private val context: Context,
    private var allergens: List<CategoriesModel>
) :
    RecyclerView.Adapter<CategoriesCatalogAdapter.CategoriesItemViewHolder>() {

    interface ItemOnClickListener {
        fun onCategoriesClick(title: String, state: Boolean);
    }

    private lateinit var listener: ItemOnClickListener

    fun setCategoryClickListener(listener: ItemOnClickListener) {
        this.listener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesItemViewHolder {
        val binding =
            ListItemCategoryCatalogBinding.inflate(LayoutInflater.from(context), parent, false)
        return CategoriesItemViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: CategoriesItemViewHolder, position: Int) {
        val foodItem = allergens[position]
        holder.bind(foodItem)
    }

    override fun getItemCount(): Int {
        return allergens.size
    }

    fun updateList(list: List<CategoriesModel>) {
        allergens = list
        notifyDataSetChanged()
    }


    class CategoriesItemViewHolder(
        categoryItemLayoutBinding: ListItemCategoryCatalogBinding,
        private val listener: ItemOnClickListener
    ) :
        RecyclerView.ViewHolder(categoryItemLayoutBinding.root) {

        private val binding = categoryItemLayoutBinding

        fun bind(categoryItem: CategoriesModel) {
            binding.categoryView.title = categoryItem.name
            binding.categoryView.checked = false
            binding.categoryView.image = categoryItem.icon

            binding.categoryView.setOnClickListener {
                binding.categoryView.checked = !binding.categoryView.checked
                listener.onCategoriesClick(categoryItem.name, binding.categoryView.checked)
            }
        }


    }
}