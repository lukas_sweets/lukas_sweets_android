package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class FeedbackApiModel(
    val message_id: Int,
    val feedback_id: Int,
    val date_time: String,
    val subject: Int,
    val status: Int,
    val text: String,
    val author: Int,
    val is_read: Int,
)
