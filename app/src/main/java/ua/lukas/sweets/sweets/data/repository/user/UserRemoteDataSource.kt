package ua.lukas.sweets.sweets.data.repository.user

import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.UserApiModel
import java.util.concurrent.TimeUnit


interface UserRemoteDataSourceInterface {
    suspend fun updateUser(user: UserApiModel): ResponseCode
    suspend fun updateUserCity(city: String, user_phone: String)
}

class UserRemoteDataSource(
    private val api: Api = Api(),
) : UserRemoteDataSourceInterface {

    override suspend fun updateUser(user: UserApiModel): ResponseCode {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.put(api.client)
            .setOkHttpClient(okHttpClient)
            .addBodyParameter(user)
            .build()

        val response = request.executeForString()
        if (response.isSuccess) {
            return ResponseCode.OK
        } else {
            Log.e("R", response.error.errorCode.toString())
        }
        return ResponseCode.ERROR
    }

    override suspend fun updateUserCity(city: String, user_phone: String) {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.put(api.client_city)
            .setOkHttpClient(okHttpClient)
            .addBodyParameter("phone", user_phone)
            .addBodyParameter("city", city)
            .build()

        val response = request.executeForString()
        if (response.isSuccess) {
            Log.e("R", "OK")
        } else {
            Log.e("R", response.error.errorCode.toString())
        }

    }
}