package ua.lukas.sweets.sweets.view.luckyWheel.adapter

import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ua.lukas.sweets.sweets.module.afterMeasured
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.LuckyWheelArticleApiModel
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetLuckyWheelInfoBinding

class LuckyWheelInfoModalBottomSheet(
    private val codeList: List<LuckyWheelArticleApiModel>
) : BottomSheetDialogFragment() {

    private lateinit var binding: ModalBottomSheetLuckyWheelInfoBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ModalBottomSheetLuckyWheelInfoBinding.inflate(layoutInflater)
        return binding.root
    }
    //? = inflater.inflate(R.layout.modal_bottom_sheet_product, container, false)

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheetBehavior.peekHeight =
            (Resources.getSystem().displayMetrics.heightPixels * 0.9).toInt()

        view.afterMeasured {
            view.layoutParams = FrameLayout.LayoutParams(
                view.width,
                (Resources.getSystem().displayMetrics.heightPixels * 0.9).toInt()
            )
        }




        setupView()


    }

    private fun setupView() {
        var text = ""
        for ((index, code) in codeList.withIndex()) {
            text += "${index + 1}. ${code.name}\n"
        }
        binding.codeTextView.text =
            resources.getString(R.string.title_stocks_positions_lucky_wheel, text)

        context?.let{ c ->
            val bottomSheet = view?.parent as View
            bottomSheet.backgroundTintMode = PorterDuff.Mode.SRC_OVER
            bottomSheet.backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(c, R.color.blueLuckyWheel))
        }

        binding.rulesButton.setOnClickListener {
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://lukas.ua/pravyla-ta-umovy-aktsii-koleso-udachi/")
            ).apply {
                startActivity(this)
            }
        }

        binding.closeButton.setOnClickListener {
            dismiss()
        }
    }


}