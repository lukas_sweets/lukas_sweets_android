package ua.lukas.sweets.sweets.view.questions

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.annotations.SerializedName
import ua.lukas.sweets.sweets.view.questions.adapter.QuestionAdapter
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.databinding.QuestionLayoutBinding
import java.util.*

class QuestionActivity : AppCompatActivity() {

    private lateinit var binding: QuestionLayoutBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = QuestionLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val questions = ArrayList<Questions>()
        questions.add(Questions(resources.getString(R.string.faq_q_1), resources.getString(R.string.faq_a_1)))
        questions.add(Questions(resources.getString(R.string.faq_q_2), resources.getString(R.string.faq_a_2)))
        questions.add(Questions(resources.getString(R.string.faq_q_3), resources.getString(R.string.faq_a_3)))
        questions.add(Questions(resources.getString(R.string.faq_q_4), resources.getString(R.string.faq_a_4)))

        // Create the adapter to convert the array to views

        val adapter =
            QuestionAdapter(
                this,
                questions
            )
        binding.listView.adapter = adapter

        setListViewHeightBasedOnChildren(binding.listView)

        binding.listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val intent = Intent(this@QuestionActivity, QAActivity::class.java)
                intent.putExtra("question", questions[position].question)
                intent.putExtra("answer", questions[position].answer)
                intent.putExtra("numberQ", position)
                startActivity(intent)
            }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish() // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.adapter ?: return
        val desiredWidth = View.MeasureSpec.makeMeasureSpec(
            listView.width,
            View.MeasureSpec.UNSPECIFIED
        )
        var totalHeight = 0
        var view: View? = null
        for (i in 0 until listAdapter.count) {
            view = listAdapter.getView(i, view, listView)
            if (i == 0) view.layoutParams = ViewGroup.LayoutParams(
                desiredWidth,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
            totalHeight += view.measuredHeight
        }
        val params = listView.layoutParams
        params.height = (totalHeight
                + listView.dividerHeight * (listAdapter.count - 1))
        listView.layoutParams = params
        listView.requestLayout()
    }
}

data class Questions(
    @SerializedName("question")
    val question: String,

    @SerializedName("answer")
    val answer: String
)

