package ua.lukas.sweets.sweets.data.repository.stock

import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.model.StockModel
import ua.lukas.sweets.sweets.data.model.toDBModel

interface StockRepositoryInterface {
    //    suspend fun downloadStocks(): ResponseCode
    suspend fun getStocks(): Pair<List<StockModel>, List<StockModel>>
//    suspend fun getTopStocks(): List<StockModel>
}

class StockRepository(
    private val stockLocalDataSource: StockLocalDataSource = StockLocalDataSource(),
    private val stockRemoteDataSource: StockRemoteDataSource = StockRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : StockRepositoryInterface {

    private val TAG = "StockRepository"

//    override suspend fun downloadStocks(): ResponseCode = withContext(scope.coroutineContext) {
//        val (response, arrayStocks) = stockRemoteDataSource.downloadStocks()
//
//        if (response == ResponseCode.OK) {
//            stockLocalDataSource.deleteAllStocks()
//        }
//
//        val stocksDB: ArrayList<StockModel> = arrayListOf()
//        for (stock in arrayStocks) {
//            stocksDB.add(
//                StockModel(
//                    stock.id,
//                    stock.name,
//                    stock.descriptions,
//                    stock.old_price,
//                    stock.new_price,
//                    stock.time,
//                    stock.picture,
//                    stock.top_position,
//                    stock.show_label,
//                    stock.label_text,
//                    stock.label_color,
//                    stock.label_bg_color,
//                )
//            )
//        }
//        stockLocalDataSource.addStocks(stocksDB)
//
//        return@withContext response
//
//    }

//    override suspend fun getStocks(): List<StockModel> = withContext(scope.coroutineContext) {
//        return@withContext stockLocalDataSource.getStocks()
//    }
//
//    override suspend fun getTopStocks(): List<StockModel> = withContext(scope.coroutineContext) {
//        return@withContext stockLocalDataSource.getTopStocks()
//    }

    override suspend fun getStocks(): Pair<List<StockModel>, List<StockModel>> {
//            Log.e(TAG, "getStocks()")

            val (response, arrayStocks) = stockRemoteDataSource.downloadStocks()
            val stockGeneral: ArrayList<StockModel> = arrayListOf()
            val stockTop: ArrayList<StockModel> = arrayListOf()

            if (response == ResponseCode.OK) {
                stockLocalDataSource.deleteAllStocks()

                val stocksDB: ArrayList<StockModel> = arrayListOf()
                for (stock in arrayStocks)
                    stocksDB.add(stock.toDBModel())

                stockLocalDataSource.addStocks(stocksDB)
            }

            stockGeneral.addAll(stockLocalDataSource.getStocks())
            stockTop.addAll(stockLocalDataSource.getTopStocks())

            return Pair(stockGeneral.toList(), stockTop.toList())
        }


}