package ua.lukas.sweets.sweets.data.repository.card

import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.model.CardModel
import ua.lukas.sweets.sweets.data.model.toDBModel
import ua.lukas.sweets.sweets.data.repository.user.UserRepository

interface CardRepositoryInterface {
    suspend fun downloadCards(registration: Boolean = false, phone: String? = null): ResponseCode
    suspend fun hasBonusCard(): Boolean
    suspend fun getUpdatedCards(): ArrayList<CardModel>
    suspend fun getBonusCard(): CardModel?
    suspend fun getDiscountCard(): CardModel?
    suspend fun registerBonusCard(name: String, phone: String): ResponseCode
    suspend fun deleteCard(cardNumber: String): ResponseCode
}

class CardRepository(
    private val cardRemoteDataSource: CardRemoteDataSource = CardRemoteDataSource(),
    private val cardLocalDataSource: CardLocalDataSource = CardLocalDataSource(),
    private val userRepository: UserRepository = UserRepository(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
    private val preferences: MyPreferences = MyPreferences(),
) : CardRepositoryInterface {



    override suspend fun downloadCards(registration: Boolean, phone: String?): ResponseCode = withContext(scope.coroutineContext) {
            val (response, userCardsApiModel) = cardRemoteDataSource.getCards(registration, phone)

            userCardsApiModel?.let { userCards ->
                val cardDB: ArrayList<CardModel> = arrayListOf()
                for (card in userCards.card) cardDB.add(card.toDBModel())
                cardLocalDataSource.addCards(cardDB)
                userRepository.addUser(userCards.user)
                preferences.phone = userCards.user.phone
                for (card in userCards.card) {
                    if (card.bonus_card == 1) preferences.cardNumber = card.card_number
                    if (card.bonus_card == 0) preferences.employeeCardNumber = card.card_number

                }
            }
            return@withContext response
        }


    override suspend fun hasBonusCard(): Boolean = withContext(scope.coroutineContext) {
        return@withContext cardLocalDataSource.hasBonusCards()
    }

    override suspend fun getUpdatedCards(): ArrayList<CardModel> {
        TODO("Not yet implemented")
    }

    override suspend fun getBonusCard(): CardModel? = withContext(scope.coroutineContext) {
        return@withContext cardLocalDataSource.getBonusCard()
    }

    override suspend fun getDiscountCard(): CardModel? = withContext(scope.coroutineContext) {
        return@withContext cardLocalDataSource.getDiscountCard()
    }

    override suspend fun registerBonusCard(name: String, phone: String): ResponseCode = withContext(scope.coroutineContext) {
            val response = cardRemoteDataSource.registerBonusCard(name, phone)
            if (response == ResponseCode.OK) {
                preferences.phone = phone
            }
            return@withContext response
        }

    override suspend fun deleteCard(cardNumber: String): ResponseCode = withContext(scope.coroutineContext) {
            return@withContext cardRemoteDataSource.deleteCard(cardNumber)
        }

}