package ua.lukas.sweets.sweets.data.repository.catalog

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.CatalogApiModel
import ua.lukas.sweets.sweets.data.api.PackApiModel
import ua.lukas.sweets.sweets.data.api.ProductAvailabilityApiModel

interface CatalogRepositoryInterface {
    suspend fun getCatalog(): Pair<ResponseCode, CatalogApiModel?>
    suspend fun getPack(pack_id: Int, city_id: String?): Pair<ResponseCode, PackApiModel?>
    suspend fun getAvailabilityProduct(
        city_id: String,
        pack_id: Int
    ): Pair<ResponseCode, List<ProductAvailabilityApiModel>?>
    suspend fun getPackFromBarcode (barcode: String) : Int

}

class CatalogRepository(
    private val catalogRemoteDataSource: CatalogRemoteDataSource = CatalogRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : CatalogRepositoryInterface {

    override suspend fun getCatalog(): Pair<ResponseCode, CatalogApiModel?> =
        withContext(scope.coroutineContext) {
            return@withContext catalogRemoteDataSource.getCatalog()
        }

    override suspend fun getPack(pack_id: Int, city_id: String?): Pair<ResponseCode, PackApiModel?> =
        withContext(scope.coroutineContext) {
            return@withContext catalogRemoteDataSource.getPack(pack_id, city_id)
        }

    override suspend fun getAvailabilityProduct(
        city_id: String,
        pack_id: Int
    ): Pair<ResponseCode, List<ProductAvailabilityApiModel>?> = withContext(scope.coroutineContext) {
        return@withContext catalogRemoteDataSource.getAvailabilityProduct(city_id, pack_id)
    }

    override suspend fun getPackFromBarcode(barcode: String): Int = withContext(scope.coroutineContext) {
        return@withContext catalogRemoteDataSource.getPackFromBarcode(barcode)
    }

}