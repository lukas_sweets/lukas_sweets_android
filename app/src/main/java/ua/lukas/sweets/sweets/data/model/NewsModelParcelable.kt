package ua.lukas.sweets.sweets.data.model

import android.os.Parcelable
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import kotlinx.parcelize.Parcelize
import ua.lukas.sweets.sweets.data.api.NewsApiModel

@Parcelize
data class NewsModelParcelable(
    val title: String,
    val validity: String,
    val text_news: String,
    val picture: String,
    val button_show: Boolean,
    val button_text: String?,
    val button_url: String?,
): Parcelable