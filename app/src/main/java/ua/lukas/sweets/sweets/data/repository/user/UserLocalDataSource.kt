package ua.lukas.sweets.sweets.data.repository.user

import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.data.db.UserDao
import ua.lukas.sweets.sweets.data.model.UserModel

interface UserLocalDataSourceInterface {
    suspend fun getUser(): UserModel
    suspend fun addUser(user: UserModel)
    suspend fun deleteUsers()
    suspend fun getUserEcoCheck(): Boolean
    suspend fun getUserCity(): String?
}

class UserLocalDataSource(
    private val userDao: UserDao = LukasSweetsApp.getDatabase().usersDao(),
) : UserLocalDataSourceInterface {

    override suspend fun getUser(): UserModel {
        return userDao.getUser()
    }

    override suspend fun addUser(user: UserModel) {
//        val userDB = UserModel(
//            user.phone,
//            user.name,
//            user.surname,
//            user.patronymic,
//            user.sex,
//            user.birthday,
//            user.email,
//            user.send_eco_check as Int
//        )
        userDao.addUser(user)
    }

    override suspend fun deleteUsers() {
        userDao.deleteUsers()
    }

    override suspend fun getUserEcoCheck(): Boolean {
        return userDao.getUserEcoCheck()
    }

    override suspend fun getUserCity(): String? {
        return userDao.getUserCity()
    }
}