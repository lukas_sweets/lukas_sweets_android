package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class LuckyWheelCountApiModel(
    val count: Int = 0,
    val coupons: Int = 0,
    val article: List<LuckyWheelArticleApiModel> = listOf(),

    )


data class LuckyWheelArticleApiModel(
    val id: Int,
    val name: String,
)


data class LuckyWheelCouponsApiModel(
    val picture: String,
    val name: String,
    val date_to: String,
    val store: String,
    val is_used: Boolean,
    val is_off: Boolean,
    var color: Int?,
)

data class LuckyWheelPlayApiModel(
    val count: Int,
    val coupons: Int,
    val win_prise: LuckyWheelWinPriseApiModel,
    val prises: List<LuckyWheelPrisesApiModel>,
)

data class LuckyWheelWinPriseApiModel(
    val name: String,
    val picture: String,
    val index: Int,
    val is_bonus: Boolean,
)

data class LuckyWheelPrisesApiModel(
    val id: Int,
    val name: String,
    val icon: String,
)