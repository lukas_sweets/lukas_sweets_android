package ua.lukas.sweets.sweets.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.MainActivity
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.view.registrations.RegisterPhoneActivity
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.feedback.FeedbackRepository
import ua.lukas.sweets.sweets.data.repository.history.HistoryRepository
import ua.lukas.sweets.sweets.data.repository.store.StoreRepository
import ua.lukas.sweets.sweets.databinding.StartLayoutBinding
import java.util.*


class StartActivity : AppCompatActivity() {

    private val cardRepository = CardRepository()
    private val storeRepository = StoreRepository()
    private val historyRepository = HistoryRepository()
    private val feedbackRepository = FeedbackRepository()
    private val preferences = MyPreferences()


    private lateinit var binding: StartLayoutBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = StartLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)


        if (!DateTime.isCurrentDateBetween("2021-12-20", "2022-01-14")) {
            binding.imageView3.visibility = View.GONE
        }

        if (preferences.deviceID.isEmpty()) {
            Log.e(this.localClassName, "DeviceID is empty")
            preferences.deviceID = UUID.randomUUID().toString()
            Log.e(this.localClassName, "set DeviceID " + UUID.randomUUID().toString())
        }

//        AppUpdateManagerFactory.create(this).appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
//            Log.e(TAG, "AppUpdateManagerFactory")
//            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
//
//                DialogSheet(this@StartActivity)
//                    .setTitle(resources.getString(R.string.title_new_version))
//                    .setMessage(resources.getString(R.string.force_text_new_version) + "\n\n")
//                    .setIconResource(R.drawable.ic_raccoon)
//                    .setCancelable(false)
//                    .setRoundedCorners(true)
//                    .setButtonsColorRes(R.color.red)
//                    .setPositiveButton(resources.getString(R.string.ok_new_version)) {
//                        startActivity(
//                            Intent(
//                                Intent.ACTION_VIEW,
//                                Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
//                            )
//                        )
//                    }
//                    //.setNegativeButton(resources.getString(R.string.cancel_new_version)) { }
//                    .show()
//            } else {
        CoroutineScope(Dispatchers.Default).launch {
            val result = cardRepository.downloadCards()
            withContext(Dispatchers.Main) {
                when (result) {
                    ResponseCode.OK -> {
                        val listAsync = listOf(
                            historyRepository.downloadHistoriesAsync(),
                            storeRepository.downloadStoresAsync(),
                            feedbackRepository.downloadFeedbacksAsync()
                        )
                        listAsync.awaitAll()
                        startActivity(Intent(this@StartActivity, MainActivity::class.java))
                    }

                    ResponseCode.NO_CARD -> {
                        CoroutineScope(Dispatchers.IO).launch {
                            LukasSweetsApp.getDatabase().clearAllTables()
                        }
                        preferences.clear()
                        startActivity(
                            Intent(
                                this@StartActivity,
                                RegisterPhoneActivity::class.java
                            )
                        )
                    }

                    ResponseCode.ERROR_DEVICE_ID -> {
                        withContext(Dispatchers.Default){
                            LukasSweetsApp.getDatabase().clearAllTables()
                        }
                        preferences.clear()
                        startActivity(
                            Intent(
                                this@StartActivity,
                                RegisterPhoneActivity::class.java
                            )
                        )
                    }

                    ResponseCode.ERROR -> {
                        if (cardRepository.hasBonusCard()) {
                            startActivity(Intent(this@StartActivity, MainActivity::class.java))
                        } else {
                            startActivity(
                                Intent(
                                    this@StartActivity,
                                    RegisterPhoneActivity::class.java
                                )
                            )
                        }
                    }
                }
            }
        }
    }

}