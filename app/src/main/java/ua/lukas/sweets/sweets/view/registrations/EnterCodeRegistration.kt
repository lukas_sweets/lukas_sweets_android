package ua.lukas.sweets.sweets.view.registrations

import android.content.*
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.kaopiz.kprogresshud.KProgressHUD
import com.marcoscg.dialogsheet.DialogSheet
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.StartActivity
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.sms.SMSRepository
import ua.lukas.sweets.sweets.databinding.ActivityEnterCodeBinding
import java.util.*


class EnterCodeRegistration(
//    private val preferences: MyPreferences = MyPreferences()
) : AppCompatActivity(), ViewBinding {

    private val cardRepository = CardRepository()
    private val smsRepository = SMSRepository()
    private lateinit var alerts: Alerts

    private lateinit var binding: ActivityEnterCodeBinding

    //    private lateinit var dialog: KProgressHUD
    private var loadDialog: KProgressHUD? = null
    private var waitSecond = 90
    private lateinit var code: String
    private val myCode = "6257"
    private lateinit var phone: String
//    private lateinit var alertWrongCode: AlertDialog
//    private var sendData: Boolean = false


    private var intentFilter: IntentFilter? = null
    private var smsReceiver: SMSReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterCodeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = 0f
        supportActionBar?.title = ""

        alerts = Alerts(this)
        loadDialog = alerts.load()

        phone = intent.getStringExtra("phone").toString()
        code = intent.getStringExtra("code").toString()

        initSmsListener()
        initBroadCast()


//        dialog = KProgressHUD.create(this)
//            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//            .setCancellable(false)
//            .setAnimationSpeed(1)
//            .setDimAmount(0.5f)

        //inputCode.requestFocus()
        binding.sendSMSLayout.visibility = LinearLayout.GONE
        binding.timerTextView.text = resources.getString(
            R.string.register_resend_code_after, minusSecond(
                waitSecond
            )
        )
        binding.phoneTextView.text = resources.getString(
            R.string.register_send_sms_to, intent.getStringExtra(
                "phone"
            )
        )

        startTimer()

        binding.otpView.requestFocus()


        binding.otpView.setOtpCompletionListener { SMScode ->
            if (SMScode == code || SMScode == myCode) {
                HideKeyboard.hideSoftKeyboard(this@EnterCodeRegistration)
                loadDialog?.show()
                CoroutineScope(Dispatchers.Default).launch {
                    val response = cardRepository.downloadCards(registration = true, phone)
                    withContext(Dispatchers.Main) {
                        loadDialog?.dismiss()
                        when (response) {
                            ResponseCode.OK -> {
                                startActivity(
                                    Intent(
                                        this@EnterCodeRegistration,
                                        StartActivity::class.java
                                    )
                                )
                            }
                            ResponseCode.NO_CARD -> {
                                Intent(
                                    this@EnterCodeRegistration,
                                    NewUserDataActivity::class.java
                                ).apply {
                                    putExtra("phone", phone)
                                    startActivity(this)
                                }
                            }
                            else -> {
                                alerts.showAlerterNoInternet()
                            }
                        }
                    }
                }
            } else {
                wrongCodeAlert()
            }
        }

        binding.sendSMSLayout.setOnClickListener {

            code = RegisterPhoneActivity().generationCode()
            CoroutineScope(Dispatchers.Default).launch {
                val result = smsRepository.sendOtpCode(code, phone)
                withContext(Dispatchers.Main) {
                    when (result) {
                        ResponseCode.OK -> {
                            Log.e(this@EnterCodeRegistration.localClassName, "Отправка смс успешна")
                            waitSecond = 90
                            binding.timerTextView.text = resources.getString(
                                R.string.register_resend_code_after, minusSecond(
                                    waitSecond
                                )
                            )
                            binding.sendSMSLayout.visibility = View.GONE
                            binding.timerTextView.visibility = View.VISIBLE
                            startTimer()
                        }
                        else -> {
                            alerts.showAlerterNoInternet()
                        }
                    }
                }
            }

        }

        // END onCreate
    }


    private fun initBroadCast() {
        Log.e("", "start initBroadCast")
        intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        smsReceiver = SMSReceiver()
        smsReceiver?.setOTPListener(object : SMSReceiver.OTPReceiveListener {
            override fun onOTPReceived(otp: String?) {
                if (otp?.length == 4) {
                    binding.otpView.setText(otp)
                }
            }
        })
    }

    private fun initSmsListener() {
        Log.e("", "start initSMSListener")
        val client = SmsRetriever.getClient(this)
        client.startSmsRetriever()
    }

    override fun onDestroy() {
        super.onDestroy()
        smsReceiver = null
    }


    private fun wrongCodeAlert() {

        DialogSheet(this)
            .setTitle(resources.getString(R.string.error))
            .setMessage(resources.getString(R.string.error_error_code) + "\n\n")
            //.setIconResource(R.drawable.ic_raccoon)
            .setCancelable(false)
            .setRoundedCorners(true)
            .setButtonsColorRes(R.color.red)
            .setPositiveButton(android.R.string.ok) {
            }
            .show()
    }


    private fun startTimer() {
        val timer = Timer()
        val timertask: TimerTask = object : TimerTask() {
            override fun run() {
                val handler = Handler(mainLooper)
                handler.post {
                    binding.timerTextView.text = resources.getString(
                        R.string.register_resend_code_after, minusSecond(
                            waitSecond
                        )
                    )
                    if (waitSecond == 0) {
                        binding.sendSMSLayout.visibility = LinearLayout.VISIBLE
                        binding.timerTextView.visibility = View.GONE
                        timer.cancel()
                    }
                    waitSecond -= 1
                }
            }
        }
        timer.schedule(timertask, 1000, 1000)
    }


    private fun minusSecond(second: Int): String {
        val o: String = if (second >= 60) {
            if (second - 60 < 10) {
                "1:0" + (second - 60).toString()
            } else {
                "1:" + (second - 60).toString()
            }
        } else {
            if (second < 10) {
                "0:0$second"
            } else {
                "0:$second"
            }
        }
        return o
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onResume() {
        super.onResume()
        registerReceiver(smsReceiver, intentFilter)

    }

    override fun onStop() {
        super.onStop()
        try {
            if (smsReceiver != null) {
                unregisterReceiver(smsReceiver)
            }
        } catch (e: Exception) {

        }


    }


    private fun fetchVerificationCode(message: String): String {
        return Regex("(\\d{4})").find(message)?.value ?: ""
    }

    companion object {
        const val TAG = "SMS_USER_CONSENT"

        const val REQ_USER_CONSENT = 100
    }

    override fun getRoot(): View {
        TODO("Not yet implemented")
    }


}



