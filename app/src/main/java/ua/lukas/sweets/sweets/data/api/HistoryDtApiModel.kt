package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName
import ua.lukas.sweets.sweets.data.model.HistoryDtModel

interface HistoryDtApiModelInterface {
    fun toDbModel() : HistoryDtModel
}

data class HistoryDtApiModel(
    val id: Int,
    val history_id: Int,
    val goods_name: String,
    val cnt: Double,
    val sum_total: Double
): HistoryDtApiModelInterface {

    override fun toDbModel(): HistoryDtModel {
        return HistoryDtModel(
            this.id,
            this.history_id,
            this.goods_name,
            this.cnt,
            this.sum_total
        )
    }

}

