package ua.lukas.sweets.sweets.view.store.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ua.lukas.sweets.sweets.view.maps.MapsActivity
import ua.lukas.sweets.sweets.data.model.StoreModel
import ua.lukas.sweets.sweets.databinding.ListItemStoresBinding

import ua.lukas.sweets.sweets.BuildConfig

class StoresAdapter(private val context: Context, private var stores: List<StoreModel>) :
    RecyclerView.Adapter<StoresAdapter.StoresItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoresItemViewHolder {
        val binding = ListItemStoresBinding.inflate(LayoutInflater.from(context), parent, false)
        return StoresItemViewHolder(context, binding)
    }

    override fun onBindViewHolder(holder: StoresItemViewHolder, position: Int) {
        val foodItem = stores[position]
        holder.bind(foodItem)
    }

    override fun getItemCount(): Int {
        return stores.size
    }

    fun update(filterList: List<StoreModel>) {
        stores = filterList
        notifyDataSetChanged()
    }



    class StoresItemViewHolder(private val context: Context, foodItemLayoutBinding: ListItemStoresBinding) :
        RecyclerView.ViewHolder(foodItemLayoutBinding.root) {

        private val binding = foodItemLayoutBinding

        fun bind(storeItem: StoreModel) {
            binding.addressTextView.text = storeItem.address
            binding.timeWorkTextView.text = storeItem.time_work

            binding.root.setOnClickListener {
                val i = Intent(context, MapsActivity::class.java)
                i.putExtra("id", storeItem.id)
                context.startActivity(i)
            }

        }

    }
}