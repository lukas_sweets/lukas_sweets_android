package ua.lukas.sweets.sweets.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ua.lukas.sweets.sweets.data.model.StockModel

@Dao
interface StockDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addStocks(stock: ArrayList<StockModel>)

    @Query("SELECT * FROM stocks WHERE top_position = 0 ORDER BY `order`")
    fun getStocks(): List<StockModel>

    @Query("SELECT * FROM stocks WHERE top_position = 1 ORDER BY `order`")
    fun getTopStocks(): List<StockModel>

    @Query("DELETE FROM stocks")
    fun deleteStocks()

}