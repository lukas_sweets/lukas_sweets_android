package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import ua.lukas.sweets.sweets.data.api.CardApiModel

@Entity(tableName = "cards")
data class CardModel(
    @PrimaryKey
    val card_number: String,
    val balance: Int?,
    val bonus_card: Int,
    val purchase_amount: Double?,
    val loyal_size: Int
)

fun CardApiModel.toDBModel() : CardModel {
    return CardModel(
        this.card_number,
        this.balance,
        this.bonus_card,
        this.purchase_amount,
        this.loyal_size
    )
}

