package ua.lukas.sweets.sweets.view.connectionDialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.databinding.NoInternetLayoutBinding

interface ReloadDialogListener {
    fun onReloadClick()
}

interface DismissDialogListener {
    fun onDismissClick()
}

class NoConnectionDialog : DialogFragment() {
    private lateinit var binding: NoInternetLayoutBinding

    lateinit var reloadListener: ReloadDialogListener
    lateinit var dismissListener: DismissDialogListener

    fun setOnReloadClickListener(listener: ReloadDialogListener) {
        this.reloadListener = listener
    }

    fun setOnDismissClickListener(listener: DismissDialogListener) {
        this.dismissListener = listener
    }

    inline fun reloadClickListener(crossinline continuation: (ReloadDialogListener) -> Unit): NoConnectionDialog {
        setOnReloadClickListener(object : ReloadDialogListener {
            override fun onReloadClick() {
                continuation(reloadListener)
            }
        })
        return this
    }

    inline fun dismissClickListener(crossinline continuation: (DismissDialogListener) -> Unit): NoConnectionDialog {
        setOnDismissClickListener(object : DismissDialogListener {
            override fun onDismissClick() {
                continuation(dismissListener)
            }
        })
        return this
    }

    companion object {
        fun create(): NoConnectionDialog {
            return NoConnectionDialog()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = NoInternetLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener {
            dismissListener.onDismissClick()
        }
        binding.reloadButton.setOnClickListener {
            reloadListener.onReloadClick()
        }
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog?.window?.setLayout(width, height)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);

    }

    fun show(supportFragmentManager: FragmentManager): NoConnectionDialog {
        this.show(supportFragmentManager, "tag")
        return this
    }
}