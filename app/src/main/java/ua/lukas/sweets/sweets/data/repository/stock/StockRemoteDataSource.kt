package ua.lukas.sweets.sweets.data.repository.stock

import com.androidnetworking.AndroidNetworking
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.StockApiModel
import java.util.concurrent.TimeUnit

interface StockRemoteDataSourceInterface {
    suspend fun downloadStocks(): Pair<ResponseCode, ArrayList<StockApiModel>>
}

class StockRemoteDataSource(
    private val api: Api = Api(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : StockRemoteDataSourceInterface {

    private val TAG = "StockRemoteDataSource"


    override suspend fun downloadStocks(): Pair<ResponseCode, ArrayList<StockApiModel>> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.stock)
            .setOkHttpClient(okHttpClient)
            .build()
        val response = request.executeForObjectList(StockApiModel::class.java)
        if (response.isSuccess) {
            return Pair(ResponseCode.OK, response.result as ArrayList<StockApiModel>)
        }

        return Pair(ResponseCode.ERROR, arrayListOf())

    }

}