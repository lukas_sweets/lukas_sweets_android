package ua.lukas.sweets.sweets.view.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fondesa.kpermissions.allGranted
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.fondesa.kpermissions.extension.send
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.shape.MaterialShapeDrawable
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.ifLet
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.StoreModel
import ua.lukas.sweets.sweets.data.repository.store.StoreRepository
import ua.lukas.sweets.sweets.databinding.ActivityMapsBinding
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, OnMapLoadedCallback, LocationListener,
    GoogleMap.OnMarkerClickListener {

    private lateinit var binding: ActivityMapsBinding
    private val storeRepository = StoreRepository()
    private lateinit var standardBottomSheetBehavior: BottomSheetBehavior<FrameLayout>

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var googleMap: GoogleMap


    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult) {
            p0 ?: return
            for (location in p0.locations) {
                selectStore?.let {
                    goToSelectedStore()
                }
                    ?: run {
                        Log.e("locationCallback", "locationCallback")
                        moveToLocation(location)
                    }
            }
        }
    }

    private fun moveToLocation(location: Location, zoomTo: Float = 12f) {
        val center = CameraUpdateFactory.newLatLng(
            LatLng(
                location.latitude, location.longitude
            )
        )
        val zoom = CameraUpdateFactory.zoomTo(zoomTo)
        googleMap.moveCamera(center)
        googleMap.animateCamera(zoom, null)
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }


    private var store_list: List<StoreModel> = listOf()
    private var selectStore: StoreModel? = null

    private var locationRequest: LocationRequest =
        LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 10000).apply {
            setMinUpdateDistanceMeters(10F)
            setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
            setWaitForAccurateLocation(true)
        }.build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        standardBottomSheetBehavior = BottomSheetBehavior.from(binding.standardBottomSheet)
        standardBottomSheetBehavior.isHideable = true
        standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
//        standardBottomSheetBehavior.peekHeight = 0
//        binding.standartBottomSheet


        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)


        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.appBarLayout.statusBarForeground =
            MaterialShapeDrawable.createWithElevationOverlay(this);

    }


    override fun onMapLoaded() {
        if (ActivityCompat.checkSelfPermission(
                this@MapsActivity, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@MapsActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
            )
        } else {
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest, locationCallback, Looper.getMainLooper()
            )
            googleMap.isMyLocationEnabled = true
        }
    }


    override fun onLocationChanged(location: Location) {}


    @SuppressLint("NotifyDataSetChanged")
    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        googleMap.uiSettings.isZoomControlsEnabled = false
        googleMap.uiSettings.isMyLocationButtonEnabled = false

        CoroutineScope(Dispatchers.Default).launch {
            selectStore = intent.getIntExtra("id", 0).let { storeRepository.getStore(it) }
            store_list = storeRepository.getMapStores()
            withContext(Dispatchers.Main) {

                googleMap.setOnMapLoadedCallback(this@MapsActivity)
                googleMap.setOnMarkerClickListener(this@MapsActivity)

                for ((i, store) in store_list.withIndex()) {
                    ifLet(store.lat, store.lon) { (lat, lon) ->
                        val marker = googleMap.addMarker(
                            MarkerOptions().position(LatLng(lat.toDouble(), lon.toDouble()))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                        )
                        marker?.tag = i
                    }
                }

                val center = CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        49.0139, 31.2858
                    ), 5F
                )
                googleMap.moveCamera(center)

            }
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(
            requestCode, permissions, grantResults
        )
        if (requestCode == 1) {
            // Checking whether user granted the permission or not.
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Showing the toast message
                recreate()
            } else {
                selectStore?.let {
                    goToSelectedStore()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setDataBottomSheet(storeModel: StoreModel) {
//        val bottomSheetMapsStore = MapsStoreModalBottomSheet(storeModel)
//        bottomSheetMapsStore.show(supportFragmentManager, "tag_maps_store")
        standardBottomSheetBehavior.isHideable = true
        standardBottomSheetBehavior.isFitToContents = true
        standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        binding.addressTextView.text = storeModel.address
        binding.timeWorkTextView.text = storeModel.time_work
//        binding.standardBottomSheet.viewTreeObserver.addOnGlobalLayoutListener {
//            binding.standardBottomSheet.requestLayout()
//        }


        binding.callButton.setOnClickListener {
            permissionsBuilder(Manifest.permission.CALL_PHONE).build().send { permission ->
                if (permission.allGranted()) {
                    this.startActivity(
                        Intent(
                            Intent.ACTION_CALL,
                            Uri.parse("tel:" + storeModel.phone)
                        )
                    )
                }
            }
        }

        binding.routeButton.setOnClickListener {
            val uri =
                "http://maps.google.com/maps?daddr=" + storeModel.lat + "," + storeModel.lon + " (" + "Lukas Sweets" + ")"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            try {
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                try {
                    val unrestrictedIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    startActivity(unrestrictedIntent)
                } catch (innerEx: ActivityNotFoundException) {
                    Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG)
                        .show()
                }
            }
        }
    }

    private fun goToSelectedStore() {
        ifLet(selectStore?.lat, selectStore?.lon) { (lat, lon) ->
            val storeLocation = Location(LocationManager.GPS_PROVIDER)
            storeLocation.latitude = lat.toDouble()
            storeLocation.longitude = lon.toDouble()
            moveToLocation(storeLocation, zoomTo = 18f)
        }
        setDataBottomSheet(selectStore!!)
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val position = marker.tag as Int
        setDataBottomSheet(store_list[position])

        return false
    }

    private fun checkPermission(permission: String, requestCode: Int) {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
        } else {
            Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show()
        }
    }

}


