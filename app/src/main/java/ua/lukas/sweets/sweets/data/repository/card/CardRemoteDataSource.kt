package ua.lukas.sweets.sweets.data.repository.card

import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.UserCardsApiModel
import java.util.concurrent.TimeUnit


interface CardRemoteDataSourceInterface {
    //    suspend fun getCards(callback: (ResponseCode, UserCardsApiModel?) -> Unit)
    suspend fun getCards(registration: Boolean, phone: String?): Pair<ResponseCode, UserCardsApiModel?>
    suspend fun registerBonusCard(name: String, phone: String): ResponseCode
    suspend fun deleteCard(cardNumber: String): ResponseCode
}

class CardRemoteDataSource(
    private val api: Api = Api(),
    private val preferences: MyPreferences = MyPreferences(),
) : CardRemoteDataSourceInterface {
    private val TAG = "CardRemoteDataSource"


    override suspend fun getCards(registration: Boolean, phone: String?): Pair<ResponseCode, UserCardsApiModel?> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.card)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("phone", phone ?: preferences.phone)
            .addQueryParameter("deviceID", preferences.deviceID)
            .addQueryParameter("platform", "android")
            .addQueryParameter("token", preferences.token)
            .addQueryParameter("registration", if (phone == null) "false" else "true")
            .build()

        Log.e(TAG,request.requestBody.toString())


        val response = request.executeForObject(UserCardsApiModel::class.java)
        Log.e(TAG,request.executeForString().toString())

        if (response.isSuccess) {
            return Pair(ResponseCode.OK, response.result as UserCardsApiModel)
        } else {
//            Log.e(TAG, response.error.errorBody)
            Log.e(TAG, response.error.errorDetail)
            Log.e(TAG, response.error.toString())
            Log.e(TAG, response.error.errorCode.toString())

            if (response.error.errorCode == 401 || response.error.errorCode == 402) {
                return Pair(ResponseCode.NO_CARD, null)
            } else if (response.error.errorCode == 403) {
                return Pair(ResponseCode.ERROR_DEVICE_ID, null)
            }
        }

        Log.e(TAG, response.toString())
        Log.e(TAG, response.error.errorDetail)
        Log.e(TAG, response.error.toString())
        Log.e(TAG, response.error.errorCode.toString())
        return Pair(ResponseCode.ERROR, null)
    }


    override suspend fun registerBonusCard(name: String, phone: String): ResponseCode {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

            val request = AndroidNetworking.post(api.card)
                .setOkHttpClient(okHttpClient)
                .addQueryParameter("phone", phone)
                .addQueryParameter("name", name)
                .addQueryParameter("token", preferences.token)
                .addQueryParameter("deviceID", preferences.deviceID)
                .addQueryParameter("platform", "ios")
                .build()

            val response = request.executeForString()

            if (response.isSuccess) {
                return ResponseCode.OK
            }

            return ResponseCode.ERROR
        }

    override suspend fun deleteCard(cardNumber: String): ResponseCode {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

            val request = AndroidNetworking.delete(api.card)
                .setOkHttpClient(okHttpClient)
                .addQueryParameter("card_number", cardNumber)
                .addQueryParameter("is_delete", "1")
                .build()

            val response = request.executeForString()

            if (response.isSuccess)
                return ResponseCode.OK


            return ResponseCode.ERROR
        }
}