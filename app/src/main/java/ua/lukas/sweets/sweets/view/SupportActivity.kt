package ua.lukas.sweets.sweets.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.data.repository.feedback.FeedbackRepository
import ua.lukas.sweets.sweets.databinding.ActivitySupportBinding
import ua.lukas.sweets.sweets.view.feedback.MyFeedbackActivity
import ua.lukas.sweets.sweets.view.questions.QuestionActivity

class SupportActivity: AppCompatActivity() {

    private lateinit var binding: ActivitySupportBinding
    private val feedbackRepository = FeedbackRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySupportBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.badgeTextView.visibility = View.GONE

        binding.feedbackView.setOnClickListener {
            val intent = Intent(this, MyFeedbackActivity::class.java)
            startActivity(intent)
        }

        binding.faqView.setOnClickListener {
            val intent = Intent(this, QuestionActivity::class.java)
            startActivity(intent)
        }

        binding.rulesView.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://sweets.lukas.ua/terms"));
            startActivity(browserIntent);
        }

        binding.privacyView.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://sweets.lukas.ua/privacy"));
            startActivity(browserIntent);
        }

        binding.hotlineView.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
            ) {
                this.startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:0800505091")))
            } else {
                checkPermission(Manifest.permission.CALL_PHONE, 1)
            }
        }

        binding.facebookImageView.setOnClickListener {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.facebook.com/lukas.company/")
            )
            startActivity(browserIntent)
        }

        binding.instagramImageView.setOnClickListener {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://instagram.com/lukas.from.ukraine")
            )
            startActivity(browserIntent)
        }

        binding.youtubeImageView.setOnClickListener {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/channel/UC08try613MHlV6y8qq8B7kg/")
            )
            startActivity(browserIntent)
        }


    }


    override fun onResume() {
        super.onResume()
        CoroutineScope(Dispatchers.Default).launch {
            val badge = feedbackRepository.getNewFeedbacksCount()
            withContext(Dispatchers.Main){
                if(badge > 0) {
//                    BadgeFactory.createDot(this@SupportActivity).bind(binding.textView591)
                    binding.badgeTextView.visibility = View.VISIBLE
//                    binding.badgeTextView.text = badge.toString()
                } else {
                    binding.badgeTextView.visibility = View.GONE
                }
            }
        }
    }



    private fun checkPermission(permission: String, requestCode: Int) {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
        } else {
            Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show()
        }
    }







    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}