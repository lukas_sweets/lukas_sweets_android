package ua.lukas.sweets.sweets.view.catalog.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.data.api.PacksProductsModel
import ua.lukas.sweets.sweets.databinding.ListItemCatalogBinding
import ua.lukas.sweets.sweets.databinding.ListItemHeaderCatalogBinding
import ua.lukas.sweets.sweets.view.catalog.ProductCatalog


interface ListItem {
    val itemType: Int
    companion object {
        const val ITEM = 0
        const val HEADER = 1
    }
}

data class Category (
    val name: String,
) : ListItem {
    override val itemType: Int
        get() = ListItem.HEADER
}

class CatalogAdapter(
    private val context: Context,
    private var catalog: List<ListItem>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {//CatalogItemViewHolder {
        if (viewType == ListItem.HEADER) {
            val binding =
                ListItemHeaderCatalogBinding.inflate(LayoutInflater.from(context), parent, false)
            return VHHeader(binding)
        } else if (viewType == ListItem.ITEM){
            val binding =
                ListItemCatalogBinding.inflate(LayoutInflater.from(context), parent, false)
            return VHItem(binding)
        }
        throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHHeader) {
            val item = catalog[position] as Category
            holder.bind(item)
        } else if (holder is VHItem) {
            val item = catalog[position] as PacksProductsModel
            holder.bind(item, context)
        }
    }

    override fun getItemCount(): Int {
        return catalog.size
    }

    override fun getItemViewType(position: Int): Int {
        return catalog[position].itemType
    }

    fun updateList(list: List<ListItem>) {
        catalog = list
        notifyDataSetChanged()
    }

    class VHItem internal constructor(catalogItemLayoutBinding: ListItemCatalogBinding,) : RecyclerView.ViewHolder(catalogItemLayoutBinding.root) {
        private val binding = catalogItemLayoutBinding

        fun bind(categoryItem: PacksProductsModel, context: Context) {
            Picasso.get().load(categoryItem.image).into(binding.productImageView)
            binding.nameTextView.text = categoryItem.name

            binding.root.setOnClickListener {
                val intent = Intent(context, ProductCatalog::class.java)
                intent.putExtra("pack_id", categoryItem.id)
                context.startActivity(intent)
            }
        }
    }



    class VHHeader internal constructor(headerCatalogItemLayoutBinding: ListItemHeaderCatalogBinding,) : RecyclerView.ViewHolder(headerCatalogItemLayoutBinding.root) {
        private val binding = headerCatalogItemLayoutBinding
        fun bind(item: Category) {
            binding.nameTextView.text = item.name
        }
    }
}