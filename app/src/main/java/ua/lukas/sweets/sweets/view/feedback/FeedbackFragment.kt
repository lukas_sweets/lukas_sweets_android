package ua.lukas.sweets.sweets.view.feedback

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.Fragment
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.MainActivity
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.NewFeedbackModel
import ua.lukas.sweets.sweets.data.model.StoreModel
import ua.lukas.sweets.sweets.data.model.UserModel
import ua.lukas.sweets.sweets.databinding.FragmentFeedbackBinding
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.module.HideKeyboard.hideSoftKeyboard
import java.util.*


class FeedbackFragment : Fragment() {

    private lateinit var _binding: FragmentFeedbackBinding
    private val binding get() = _binding

    private val mainActivity = MainActivity()
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null



    //    private var mTextView: AutoCompleteTextView? = null
    private var feedback: NewFeedbackModel = NewFeedbackModel()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFeedbackBinding.inflate(inflater, container, false)
        return binding.root
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        HideKeyboard.setupUI(view.findViewById(R.id.parent), requireActivity())

        activity?.let { activity ->
            alerts = Alerts(activity)
            loadDialog = alerts.load()
        }


        ///Send feedback
        binding.button.setOnClickListener OnClickListener@{
            binding.nameTextField.clearFocus()
            binding.textTextField.clearFocus()
            binding.autoCompleteTextView.clearFocus()

            if (binding.nameTextField.editText?.text?.isEmpty() == true) {
                binding.nameTextField.error = resources.getString(R.string.feedback_enter_name)
                return@OnClickListener
            } else {
                feedback.name = binding.nameTextField.editText?.text.toString()
                binding.nameTextField.error = null
            }
            if (binding.textTextField.editText?.text?.trim()?.isEmpty() == true) {
                binding.textTextField.error = resources.getString(R.string.feedback_enter_text)
                return@OnClickListener
            } else {
                feedback.text = binding.textTextField.editText?.text?.trim().toString()
                binding.textTextField.error = null
            }
            if (feedback.subject == null) {
                binding.autoCompleteTextView.error =
                    resources.getString(R.string.feedback_enter_subject)
                return@OnClickListener
            } else {
                binding.autoCompleteTextView.error = null
            }


            loadDialog?.show()
            CoroutineScope(Dispatchers.Default).launch {
                val response = mainActivity.sendNewFeedback(feedback)

                withContext(Dispatchers.Main) {
                    loadDialog?.dismiss()
                    when (response) {
                        ResponseCode.OK -> {
                            alerts.showAlertFeedbackIsSend()
                            binding.textEdit.text = null
                            binding.autoCompleteTextView.text = null
                            binding.cityAutoCompleteTextView.text = null
                            binding.kioskAutoCompleteTextView.text = null
                        }
                        else -> {
                            alerts.showAlerterNoInternet()
                        }
                    }
                }
            }
        }
        //END onViewCreated
    }


    @Suppress("UNCHECKED_CAST")
    private fun getDataFromDB() {
        CoroutineScope(Dispatchers.Main).launch {
            val (user, cities) = listOf(
                mainActivity.getUserAsync(),
                mainActivity.getCitiesAsync()
            ).awaitAll()

            settingCities(cities as List<String>)
            settingSubjects()
            settingUser(user as UserModel)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun settingStores(storeList: List<StoreModel>) {
        val adapterKiosk = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_list_item_1,
            storeList.map { it.address }
        )

        binding.kioskAutoCompleteTextView.apply {
            setAdapter(adapterKiosk)
            keyListener = null
            setOnTouchListener { v, event ->
                hideSoftKeyboard(activity)
                (v as AutoCompleteTextView).showDropDown()
                v.performClick()
                v.onTouchEvent(event)
            }
            setOnItemClickListener { parent, view, position, rowId ->
//                textKiosk = parent.getItemAtPosition(position) as String
                //textKiosk = storeList[position].code_1C
                feedback.store = storeList[position].id
                clearFocus()
            }
        }

        binding.kioskTextField.setEndIconOnClickListener {
            binding.kioskAutoCompleteTextView.text = null
            feedback.store = null
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun settingSubjects() {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            listOf(
                resources.getString(R.string.feedback_subject_gratitude),
                resources.getString(R.string.feedback_subject_question),
                resources.getString(R.string.feedback_subject_complaint),
                resources.getString(R.string.feedback_subject_offer),
                resources.getString(R.string.feedback_subject_app)

            )
        )

        binding.autoCompleteTextView.apply {
            setAdapter(adapter)
            keyListener = null
            setOnTouchListener { v, event ->
                (v as AutoCompleteTextView).showDropDown()
                v.performClick()
                v.onTouchEvent(event)
            }
            onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, rowId ->
                feedback.subject = position + 1
                binding.autoCompleteTextView.clearFocus()
            }
        }
    }

    private fun settingCities(citiesList: List<String>) {
        val adapter = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_list_item_1,
            citiesList.map { it }
        )

        binding.cityAutoCompleteTextView.apply {
            setAdapter(adapter)
            keyListener = null
            setOnTouchListener { v, event ->
                hideSoftKeyboard(activity)
                (v as AutoCompleteTextView).showDropDown()
                v.performClick()
                v.onTouchEvent(event)
            }
            setOnItemClickListener { parent, view, position, rowId ->
//                textKiosk = parent.getItemAtPosition(position) as String
                //textKiosk = storeList[position].code_1C
//                feedback.store = storeList[position].code_1c
                CoroutineScope(Dispatchers.Default).launch {
                    val listStores =
                        mainActivity.getStoresInCity(parent.getItemAtPosition(position) as String)
                    withContext(Dispatchers.Main) {
                        feedback.city = parent.getItemAtPosition(position) as String
                        settingStores(listStores)
                    }
                }
                clearFocus()
            }
        }

        binding.cityTextField.setEndIconOnClickListener {
            binding.cityAutoCompleteTextView.text = null
            feedback.city = null
            settingStores(listOf())
        }
    }

    private fun settingUser(user: UserModel) {
        feedback.phone = user.phone
        binding.nameEdit.setText(
            resources.getString(
                R.string.feedback_name_surname,
                user.name,
                user.surname ?: ""
            ).trim()
        )
    }


    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showLuckyWheelBanner()
        getDataFromDB()
    }


    //END FRAGMENT
}