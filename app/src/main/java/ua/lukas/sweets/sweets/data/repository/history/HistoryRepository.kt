package ua.lukas.sweets.sweets.data.repository.history

import android.util.Log
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.model.HistoryAdapterModel
import ua.lukas.sweets.sweets.data.model.HistoryAllModel
import ua.lukas.sweets.sweets.data.model.HistoryModel
import ua.lukas.sweets.sweets.data.model.toDBModel

interface HistoryRepositoryInterface {
    suspend fun downloadHistoriesAsync(): Deferred<ResponseCode>
    suspend fun downloadHistoryDtsAsync(history_id: Int): Deferred<ResponseCode>
    suspend fun getHistories(
        reload: Boolean,
        limit: Int
    ): Pair<ResponseCode, List<HistoryAdapterModel>>

    suspend fun getCountHistories(): Int
    suspend fun getHistory(
        reload: Boolean = false,
        history_id: Int
    ): Pair<ResponseCode, HistoryAllModel>
}

class HistoryRepository(
    private val historyRemoteDataSource: HistoryRemoteDataSource = HistoryRemoteDataSource(),
    private val historyLocalDataSource: HistoryLocalDataSource = HistoryLocalDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : HistoryRepositoryInterface {

    private val TAG = "HistoryRepository"

//    override suspend fun downloadHistoryAsync(): Deferred<ResponseCode> = scope.async {
//
//        val (response, historyGeneralApiModel) = historyRemoteDataSource.downloadHistory()
//
//        if (response == ResponseCode.OK) {
//            historyLocalDataSource.deleteAllHistories()
//        }
//
//        historyGeneralApiModel?.let { historiesGeneral ->
//            val historyDB: ArrayList<HistoryModel> = arrayListOf()
//            for (history in historiesGeneral.history)
//                historyDB.add(history.toDBModel())
//
//            historyLocalDataSource.addHistories(historyDB)
//        }
//
//        return@async response
//    }

    override suspend fun downloadHistoriesAsync(): Deferred<ResponseCode> = scope.async {
        val (response, historyGeneralApiModel) = historyRemoteDataSource.downloadHistories()

        if (response == ResponseCode.OK) {
            historyLocalDataSource.deleteAllHistories()
        }

        historyGeneralApiModel?.let { historiesGeneral ->
            val historyDB: ArrayList<HistoryModel> = arrayListOf()
            for (history in historiesGeneral.history)
                historyDB.add(history.toDBModel())

            historyLocalDataSource.addHistories(historyDB)
        }
        Log.e(TAG, "downloadHistoriesAsync")
        return@async response
    }

    override suspend fun downloadHistoryDtsAsync(history_id: Int): Deferred<ResponseCode> =
        scope.async {
            val (response, historyDtsApiModel) = historyRemoteDataSource.downloadHistoryDTs(
                history_id
            )

            if (response == ResponseCode.OK) {
                historyLocalDataSource.deleteHistoryDts(history_id)

                for (historyDtApiModel in historyDtsApiModel)
                    historyLocalDataSource.addHistoryDt(historyDtApiModel.toDbModel())
            }

            return@async response

        }


    override suspend fun getHistories(
        reload: Boolean,
        limit: Int
    ): Pair<ResponseCode, List<HistoryAdapterModel>> = withContext(scope.coroutineContext) {
        var response = ResponseCode.OK
        if (reload)
            response = downloadHistoriesAsync().await()

        return@withContext Pair(response, historyLocalDataSource.getHistories(limit))
    }

    override suspend fun getCountHistories(): Int = withContext(scope.coroutineContext) {
        return@withContext historyLocalDataSource.getCountHistories()
    }

    override suspend fun getHistory(
        reload: Boolean,
        history_id: Int
    ): Pair<ResponseCode, HistoryAllModel> = withContext(scope.coroutineContext) {
        var response = ResponseCode.OK

        if (reload)
            response = downloadHistoriesAsync().await()

        var historyDt = historyLocalDataSource.getHistoryDts(history_id)
        if (historyDt.isEmpty())
            response = downloadHistoryDtsAsync(history_id).await()

        historyDt = historyLocalDataSource.getHistoryDts(history_id)

        val allData = historyLocalDataSource.getHistory(history_id).toAllModel(historyDt)


        return@withContext Pair(response, allData)

    }


}