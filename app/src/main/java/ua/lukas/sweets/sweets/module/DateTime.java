package ua.lukas.sweets.sweets.module;

import android.os.Build;
import android.os.LocaleList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class DateTime {

    public static String getMonth(Date date, String locale) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("LLLL",  new Locale(locale));

        return simpleDateFormat.format(date);
    }

    public static String getYear(Date date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy",  Locale.getDefault());

        return simpleDateFormat.format(date);
    }

    public static Date stringTOdate(String date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date dates = null;
        try {
            dates = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dates;
    }

    public static String dateTitle (String date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

        return simpleDateFormat.format(stringTOdate(date));
    }

    public static String timeTitle (String date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        return simpleDateFormat.format(stringTOdate(date));
    }

    public static String dateFormat(String date, String format, String locale){

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale(locale));
        Date dates = null;
        try {
            dates = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, new Locale(locale));
        String getDatesString = simpleDateFormat.format(dates);

        return getDatesString;
    }




    public static String getCurrentDateFormat() {

        Calendar calCur = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return simpleDateFormat.format(calCur.getTime());
    }


    public static String getCurrentLanguage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return LocaleList.getDefault().get(0).getLanguage();
        } else{
            return Locale.getDefault().getLanguage();
        }
    }



    public static String formatDateFeedback(String dateString) {

        Calendar calCur = Calendar.getInstance();
        calCur.set(Calendar.HOUR_OF_DAY, 0);
        calCur.set(Calendar.MINUTE, 0);
        calCur.set(Calendar.SECOND, 0);
        calCur.set(Calendar.MILLISECOND, 0);

        Calendar calYesterday = Calendar.getInstance();
        calYesterday.set(Calendar.HOUR_OF_DAY, 0);
        calYesterday.set(Calendar.MINUTE, 0);
        calYesterday.set(Calendar.SECOND, 0);
        calYesterday.set(Calendar.MILLISECOND, 0);
        calYesterday.add(Calendar.DAY_OF_YEAR, -1);

        Calendar calHistory = Calendar.getInstance();
        calHistory.set(Calendar.HOUR_OF_DAY, 0);
        calHistory.set(Calendar.MINUTE, 0);
        calHistory.set(Calendar.SECOND, 0);
        calHistory.set(Calendar.MILLISECOND, 0);
        calHistory.add(Calendar.DAY_OF_YEAR, -2);


        Calendar cal = Calendar.getInstance();
        cal.setTime(Objects.requireNonNull(stringTOdate(dateString)));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);





        if(cal.getTime().equals(calCur.getTime())){
            return "Сегодня";
        }
        else if(cal.getTime().equals(calYesterday.getTime())){
            return "Вчера";
        }
        else if(cal.getTime().equals(calHistory.getTime())){
            return "Позавчера";
        } else {
            return dateTitle(dateString);
        }

    }


    public static String formatDateWithTimeFeedback(String dateString) {

        Calendar calCur = Calendar.getInstance();
        calCur.set(Calendar.HOUR_OF_DAY, 0);
        calCur.set(Calendar.MINUTE, 0);
        calCur.set(Calendar.SECOND, 0);
        calCur.set(Calendar.MILLISECOND, 0);

        Calendar calYesterday = Calendar.getInstance();
        calYesterday.set(Calendar.HOUR_OF_DAY, 0);
        calYesterday.set(Calendar.MINUTE, 0);
        calYesterday.set(Calendar.SECOND, 0);
        calYesterday.set(Calendar.MILLISECOND, 0);
        calYesterday.add(Calendar.DAY_OF_YEAR, -1);

        Calendar calHistory = Calendar.getInstance();
        calHistory.set(Calendar.HOUR_OF_DAY, 0);
        calHistory.set(Calendar.MINUTE, 0);
        calHistory.set(Calendar.SECOND, 0);
        calHistory.set(Calendar.MILLISECOND, 0);
        calHistory.add(Calendar.DAY_OF_YEAR, -2);


        Calendar cal = Calendar.getInstance();
        cal.setTime(Objects.requireNonNull(stringTOdate(dateString)));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);





        if(cal.getTime().equals(calCur.getTime())){
            return "Сегодня, " + timeTitle(dateString);
        }
        else if(cal.getTime().equals(calYesterday.getTime())){
            return "Вчера, " + timeTitle(dateString);
        }
        else if(cal.getTime().equals(calHistory.getTime())){
            return "Позавчера, " + timeTitle(dateString);
        } else {
            return dateTitle(dateString)+", " + timeTitle(dateString);
        }



    }



    public static Boolean isCurrentDateBetween (String date1, String date2) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            Date time1 = sdf.parse(date1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.set(Calendar.HOUR_OF_DAY, 0);
            calendar1.set(Calendar.MINUTE, 0);
            calendar1.set(Calendar.SECOND, 0);
            calendar1.set(Calendar.MILLISECOND, 0);
            //calendar1.add(Calendar.DATE, 1);



            Date time2 = sdf.parse(date2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.set(Calendar.HOUR_OF_DAY, 0);
            calendar2.set(Calendar.MINUTE, 0);
            calendar2.set(Calendar.SECOND, 0);
            calendar2.set(Calendar.MILLISECOND, 0);
            //calendar2.add(Calendar.DATE, 1);

            Calendar calCur = Calendar.getInstance();
            calCur.set(Calendar.HOUR_OF_DAY, 0);
            calCur.set(Calendar.MINUTE, 0);
            calCur.set(Calendar.SECOND, 0);
            calCur.set(Calendar.MILLISECOND, 0);

            Date x = calCur.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                System.out.println(true);
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }



}
