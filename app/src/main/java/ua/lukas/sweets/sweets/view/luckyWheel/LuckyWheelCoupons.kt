package ua.lukas.sweets.sweets.view.luckyWheel

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.Alerts
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.repository.luckywheel.LuckyWheelRepository
import ua.lukas.sweets.sweets.databinding.ActivityLuckyWheelCouponBinding
import ua.lukas.sweets.sweets.view.connectionDialog.NoConnectionDialog
import ua.lukas.sweets.sweets.view.luckyWheel.adapter.LuckyWheelCouponRecyclerViewAdapter

class LuckyWheelCoupons : AppCompatActivity() {

    private lateinit var binding: ActivityLuckyWheelCouponBinding
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null
    private lateinit var noConnectionDialog: NoConnectionDialog

    private val luckyWheelRepository = LuckyWheelRepository()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLuckyWheelCouponBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        onBackPressedDispatcher.addCallback(this) {
            finish()
        }

        binding.progressBar.visibility = View.VISIBLE

        alerts = Alerts(this)
        loadDialog = alerts.load()
        noConnectionDialog = NoConnectionDialog
            .create()
            .dismissClickListener {
                finish()
            }
            .reloadClickListener {
                recreate()
            }

        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recycleView.layoutManager = layoutManager

        binding.swipeRefreshLayout.setColorSchemeColors(
            ContextCompat.getColor(
                this,
                R.color.red
            )
        )

        binding.swipeRefreshLayout.setOnRefreshListener {
            getCoupons()
        }

        getCoupons()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun getCoupons() {
        CoroutineScope(Dispatchers.Default).launch {
            val (response, couponsList) = luckyWheelRepository.getLuckyWheelCoupons()

            withContext(Dispatchers.Main) {
                if (binding.swipeRefreshLayout.isRefreshing) binding.swipeRefreshLayout.isRefreshing =
                    false
                binding.progressBar.visibility = View.GONE

                when (response) {
                    ResponseCode.OK -> {
                        val adapter =
                            LuckyWheelCouponRecyclerViewAdapter(this@LuckyWheelCoupons, couponsList)
                        binding.recycleView.adapter = adapter
                    }
                    else -> noConnectionDialog.show(supportFragmentManager)
                }
            }


        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish() // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item)
    }

}