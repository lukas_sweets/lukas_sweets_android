package ua.lukas.sweets.sweets.data.repository.feedback

import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.data.db.FeedbackDao
import ua.lukas.sweets.sweets.data.model.FeedbackModel

interface FeedbackLocalDataSourceInterface {
    suspend fun addFeedbacks(feedbacks: ArrayList<FeedbackModel>)
    suspend fun deleteAllFeedbacks()
    suspend fun getNewFeedbacksCount(): Int
    suspend fun getFeedbacks(): List<FeedbackModel>
    suspend fun getFeedbackMessages(feedback_id: Int): List<FeedbackModel>
    suspend fun hasNewFeedback(feedback_id: Int) : Boolean
    suspend fun getNewMessageFromFeedback(feedback_id: Int): List<Int>
}

class FeedbackLocalDataSource(
    private val feedbackDao: FeedbackDao = LukasSweetsApp.getDatabase().feedbacksDao()
) : FeedbackLocalDataSourceInterface {

    override suspend fun addFeedbacks(feedbacks: ArrayList<FeedbackModel>) {
        feedbackDao.addFeedbacks(feedbacks)
    }

    override suspend fun deleteAllFeedbacks() {
        feedbackDao.deleteAllFeedbacks()
    }

    override suspend fun getNewFeedbacksCount(): Int {
        return feedbackDao.getNewFeedbacksCount()
    }

    override suspend fun getFeedbacks(): List<FeedbackModel> {
        return feedbackDao.getFeedbacks()
    }

    override suspend fun getFeedbackMessages(feedback_id: Int): List<FeedbackModel> {
        return feedbackDao.getFeedbackMessages(feedback_id)
    }

    override suspend fun hasNewFeedback(feedback_id: Int): Boolean {
        return feedbackDao.hasNewFeedback(feedback_id)
    }

    override suspend fun getNewMessageFromFeedback(feedback_id: Int): List<Int> {
       return feedbackDao.getNewMessageFromFeedback(feedback_id)
    }
}