package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class HistoryApiModel(
    val id: Int,
    val date_time: String,
    val bonus_total: Int,
    val sum_total: Double,
    val comment: String?,
    val reason: Int,
    val qr: String?,
    val num: String,
    val bonus_add: Int,
    val pay_cash: Double,
    val pay_card: Double,
    val pay_bonus: Double,
    val pay_certificate: Double,
    val store: String?,

)
