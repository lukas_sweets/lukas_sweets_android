package ua.lukas.sweets.sweets.view.news.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.data.model.NewsModel
import ua.lukas.sweets.sweets.data.model.NewsModelParcelable
import ua.lukas.sweets.sweets.databinding.ViewNewsBinding
import ua.lukas.sweets.sweets.view.news.OneNewsActivity

class NewsViewPager2Adapter(
    private val context: Context,
    private val arrayNews: List<NewsModel>
) :
    RecyclerView.Adapter<NewsViewPager2Adapter.NewsViewHolder>() {

    inner class NewsViewHolder(val binding: ViewNewsBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = ViewNewsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsViewHolder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        with(holder) {
            with(arrayNews[position]) {

                Picasso.get().load(picture_url).into(binding.newsImageView)

                itemView.setOnClickListener {
                    Intent(context, OneNewsActivity::class.java).apply {
                        putExtra(
                            "data",
                            NewsModelParcelable(
                                title,
                                validity,
                                text_news,
                                picture_url,
                                button_show,
                                button_text,
                                picture_url
                            )
                        )
                        context.startActivity(this)
                    }
                }

            }
        }
    }
    override fun getItemCount() = arrayNews.size
}