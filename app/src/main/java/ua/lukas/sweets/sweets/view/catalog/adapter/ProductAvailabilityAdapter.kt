package ua.lukas.sweets.sweets.view.catalog.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.ProductAvailabilityApiModel
import ua.lukas.sweets.sweets.databinding.ListItemStoresAvailabilityProductBinding

class ProductAvailabilityAdapter (private val context: Context, private var productAvailabilityList: List<ProductAvailabilityApiModel>) :
    RecyclerView.Adapter<ProductAvailabilityAdapter.StoresItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoresItemViewHolder {
        val binding = ListItemStoresAvailabilityProductBinding.inflate(LayoutInflater.from(context), parent, false)
        return StoresItemViewHolder(context, binding)
    }

    override fun onBindViewHolder(holder: StoresItemViewHolder, position: Int) {
        val foodItem = productAvailabilityList[position]
        holder.bind(foodItem)
    }

    override fun getItemCount(): Int {
        return productAvailabilityList.size
    }

    fun update(filterList: List<ProductAvailabilityApiModel>) {
        productAvailabilityList = filterList
        notifyDataSetChanged()
    }



    class StoresItemViewHolder(private val context: Context, foodItemLayoutBinding: ListItemStoresAvailabilityProductBinding) :
        RecyclerView.ViewHolder(foodItemLayoutBinding.root) {

        private val binding = foodItemLayoutBinding

        fun bind(storeItem: ProductAvailabilityApiModel) {
            binding.addressTextView.text = storeItem.address
            binding.timeWorkTextView.text = storeItem.time_work

            when (storeItem.availabilities){
                0 -> {
                    binding.availabilityTextView.text = context.resources.getString(R.string.availability_off)
                    binding.availabilityTextView.setTextColor(ContextCompat.getColor(context, R.color.red))
                }
                1 -> {
                    binding.availabilityTextView.text = context.resources.getString(R.string.availability_min)
                    binding.availabilityTextView.setTextColor(ContextCompat.getColor(context, R.color.yellow))
                }
                2 -> {
                    binding.availabilityTextView.text = context.resources.getString(R.string.availability_ok)
                    binding.availabilityTextView.setTextColor(ContextCompat.getColor(context, R.color.green))
                }
            }


        }

    }
}