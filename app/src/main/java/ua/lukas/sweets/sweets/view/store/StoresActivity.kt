package ua.lukas.sweets.sweets.view.store

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.view.maps.MapsActivity
import ua.lukas.sweets.sweets.module.HideKeyboard
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.view.store.adapter.StoresAdapter
import ua.lukas.sweets.sweets.data.model.StoreModel
import ua.lukas.sweets.sweets.data.model.UserModel
import ua.lukas.sweets.sweets.data.repository.card.CardRepository
import ua.lukas.sweets.sweets.data.repository.store.StoreRepository
import ua.lukas.sweets.sweets.data.repository.user.UserRepository
import ua.lukas.sweets.sweets.databinding.ActivityStoresBinding

class StoresActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStoresBinding
    private val storeRepository = StoreRepository()
    private val userRepository = UserRepository()
    private val cardRepository = CardRepository()
    private lateinit var adapter: StoresAdapter
    private var storesList: List<StoreModel> = arrayListOf()
    private lateinit var user: UserModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStoresBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.stores)

        adapter = StoresAdapter(this@StoresActivity, storesList)
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this@StoresActivity.applicationContext)
        binding.recyclerView.layoutManager = layoutManager

    }


    override fun onResume() {
        super.onResume()
        getUser()

    }

    private fun getUser() {
        CoroutineScope(Dispatchers.Default).launch {
            user = userRepository.getUser()
            withContext(Dispatchers.Main){
                getStores()
            }
        }
    }

    private fun getStores() {
        CoroutineScope(Dispatchers.Default).launch {
            val cities = storeRepository.getCities()
            withContext(Dispatchers.Main) {
                settingStores(cities)
            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun settingStores(cities: List<String>) {
        val adapterKiosk = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            cities
        )


        binding.cityEdit.apply {
            setAdapter(adapterKiosk)
            if (user.city_id != null) {
                setText(user.city_id, false)
                getStoresCity(user.city_id!!)
            }

            keyListener = null
            setOnTouchListener { v, event ->
                HideKeyboard.hideSoftKeyboard(this@StoresActivity)
                (v as AutoCompleteTextView).showDropDown()
                false
            }
            setOnItemClickListener { parent, view, position, rowId ->
                getStoresCity(parent.getItemAtPosition(position) as String)
                CoroutineScope(Dispatchers.Default).launch {
                    userRepository.updateUserCity(
                        parent.getItemAtPosition(position) as String,
                        user.phone
                    )
                    cardRepository.downloadCards()

                }
                clearFocus()
            }
        }
    }

    private fun getStoresCity(city: String) {
        CoroutineScope(Dispatchers.Default).launch {
            storesList = storeRepository.getStoresInCity(city)
            withContext(Dispatchers.Main){
                binding.recyclerView.adapter = adapter
                adapter.update(storesList)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        if (item.itemId == R.id.store_in_maps) {
            startActivity(Intent(this, MapsActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.store_in_map, menu)
        return super.onCreateOptionsMenu(menu)
    }
}