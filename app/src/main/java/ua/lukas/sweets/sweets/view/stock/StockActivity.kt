package ua.lukas.sweets.sweets.view.stock

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.StockModelParcelable
import ua.lukas.sweets.sweets.databinding.ActivityStockBinding
import ua.lukas.sweets.sweets.view.catalog.ProductCatalog

class StockActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStockBinding
//    private var stock: StockModelParcelable? = null


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStockBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = ""




        binding.stock = if (Build.VERSION.SDK_INT >= 33) {
            intent.getParcelableExtra("data", StockModelParcelable::class.java)
        } else {
            intent.getParcelableExtra("data")
        }

        binding.stock?.let { stock ->
            binding.labelCardView.setCardBackgroundColor(Color.parseColor(stock.label_bg_color))
            Picasso.get().load(stock.picture).into(binding.stockImageView)
        } ?: run {
            finish()
        }



        binding.stockImageView.setOnClickListener {
            onStartAnimation()
        }

    }

    override fun onResume() {
        super.onResume()
//        onStartAnimation()
    }

    private fun onStartAnimation() {
        val downAnimator = ValueAnimator.ofFloat(0f, 20f)
        downAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            binding.stockImageView.translationY = value
            it.repeatCount = ObjectAnimator.INFINITE
            it.repeatMode = ObjectAnimator.REVERSE
        }

        downAnimator.duration = 2000
//        downAnimator.startDelay = 500
        downAnimator.start()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        if (item.itemId == R.id.info) {
            Intent(this, ProductCatalog::class.java).apply {
                putExtra("pack_id", binding.stock?.productsPackId)
                startActivity(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (binding.stock?.productsPackId != null) {
            val inflater = menuInflater
            inflater.inflate(R.menu.info_menu, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }


}