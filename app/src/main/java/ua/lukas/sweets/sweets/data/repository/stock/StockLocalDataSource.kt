package ua.lukas.sweets.sweets.data.repository.stock

import ua.lukas.sweets.sweets.module.LukasSweetsApp
import ua.lukas.sweets.sweets.data.db.StockDao
import ua.lukas.sweets.sweets.data.model.StockModel

interface StockLocalDataSourceInterface {
    suspend fun addStocks(stocks: ArrayList<StockModel>)
    suspend fun deleteAllStocks()
    suspend fun getStocks(): List<StockModel>
    suspend fun getTopStocks(): List<StockModel>
}

class StockLocalDataSource(
    private val stockDao: StockDao = LukasSweetsApp.getDatabase().stocksDao()
) : StockLocalDataSourceInterface {

    override suspend fun addStocks(stocks: ArrayList<StockModel>) {
        stockDao.addStocks(stocks)
    }

    override suspend fun deleteAllStocks() {
        stockDao.deleteStocks()

    }

    override suspend fun getStocks(): List<StockModel> {
        return stockDao.getStocks()
    }

    override suspend fun getTopStocks(): List<StockModel> {
        return stockDao.getTopStocks()
    }

}