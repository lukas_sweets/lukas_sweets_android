package ua.lukas.sweets.sweets.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ua.lukas.sweets.sweets.data.model.*
import ua.lukas.sweets.sweets.data.model.history.dt.HistoryDTDao

@Database(
    entities = [
        UserModel::class,
        CardModel::class,
        HistoryModel::class,
        HistoryDtModel::class,
        StoreModel::class,
        StockModel::class,
        FeedbackModel::class
    ], version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun usersDao() : UserDao
    abstract fun cardsDao() : CardDao
    abstract fun historiesDao() : HistoryDao
    abstract fun historyDTsDao() : HistoryDTDao
    abstract fun storesDao() : StoreDao
    abstract fun stocksDao() : StockDao
    abstract fun feedbacksDao() : FeedbackDao
   }