package ua.lukas.sweets.sweets.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ua.lukas.sweets.sweets.data.model.CardModel

@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCard(card: CardModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCards(cards: List<CardModel>)

    @Query("DELETE FROM cards")
    fun deleteCard()

    @Query("SELECT * FROM cards WHERE bonus_card = 1 LIMIT 1")
    fun getBonusCard(): CardModel?

    @Query("SELECT * FROM cards WHERE bonus_card = 0 LIMIT 1")
    fun getDiscountCard(): CardModel?

    @Query("SELECT * FROM cards WHERE card_number = :card_number")
    fun getCard(card_number: String): CardModel

    @Query("SELECT CASE COUNT(card_number) WHEN 0 THEN 0 ELSE 1 END  FROM cards WHERE bonus_card = 1")
    fun hasBonusCard() : Boolean
}