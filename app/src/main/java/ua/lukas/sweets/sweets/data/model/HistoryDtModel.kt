package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history_dts")
data class HistoryDtModel(
    @PrimaryKey
    val id: Int,
    val history_id: Int,
    val goods_name: String,
    val cnt: Double,
    val sum_total: Double
)
