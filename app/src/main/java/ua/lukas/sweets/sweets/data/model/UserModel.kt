package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import ua.lukas.sweets.sweets.data.api.UserApiModel

@Entity(tableName = "users")
data class UserModel(
    @PrimaryKey
    val phone: String,
    val name: String,
    val surname: String?,
    val patronymic: String?,
    val sex: Int,
    val birthday: String?,
    val email: String?,
    var send_eco_check: Boolean,
    val city_id: String?,
)

fun UserApiModel.toDBModel() : UserModel {
    return UserModel(
        this.phone,
        this.name,
        this.surname,
        this.patronymic,
        this.sex,
        this.birthday,
        this.email,
        this.send_eco_check,
        this.city_id
    )
}

fun UserModel.toApiModel() : UserApiModel {
    return UserApiModel(
        this.phone,
        this.name,
        this.surname,
        this.patronymic,
        this.sex,
        this.birthday,
        this.email,
        this.send_eco_check,
        this.city_id
    )
}