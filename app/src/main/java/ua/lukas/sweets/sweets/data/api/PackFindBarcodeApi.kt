package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class PackFindBarcodeApi(
    @SerializedName("pack_id")
    val packID: Int,
)
