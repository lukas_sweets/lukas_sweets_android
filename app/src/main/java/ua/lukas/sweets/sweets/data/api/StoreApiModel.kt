package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class StoreApiModel(
    val id: Int,
    val time_work: String?,
    val lat: String?,
    val lon: String?,
    val address: String,
    val phone_number: String?,
    val city_id: String
    )
