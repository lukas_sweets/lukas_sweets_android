package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName
import ua.lukas.sweets.sweets.view.catalog.adapter.ListItem

data class CatalogApiModel(
//    @SerializedName("categories")
    val categories: List<CategoriesModel>,

    @SerializedName("packs")
    val packs_products_model: List<PacksProductsModel>,
)




data class CategoriesModel(
//    @SerializedName("id")
    val id: Int,

//    @SerializedName("name")
    val name: String,

//    @SerializedName("icon")
    val icon: String,
)

data class PacksProductsModel(
//    @SerializedName("id")
    val id: Int,

//    @SerializedName("name")
    val name: String,

//    @SerializedName("image")
    val image: String,

//    @SerializedName("category")
    val category: String,
) : ListItem {
    override val itemType: Int
        get() { return ListItem.ITEM }

}