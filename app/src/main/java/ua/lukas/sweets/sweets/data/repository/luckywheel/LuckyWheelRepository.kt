package ua.lukas.sweets.sweets.data.repository.luckywheel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.LuckyWheelCountApiModel
import ua.lukas.sweets.sweets.data.api.LuckyWheelCouponsApiModel
import ua.lukas.sweets.sweets.data.api.LuckyWheelPlayApiModel

interface LuckyWheelRepositoryInterface {
    suspend fun getLuckyWheelCount(): Pair<ResponseCode, LuckyWheelCountApiModel>
    suspend fun getLuckyWheelCoupons(): Pair<ResponseCode, List<LuckyWheelCouponsApiModel>>
    suspend fun startWheel():Pair<ResponseCode, LuckyWheelPlayApiModel?>

}


class LuckyWheelRepository(
    private val luckyWheelRemoteDataSource: LuckyWheelRemoteDataSource = LuckyWheelRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : LuckyWheelRepositoryInterface {

    override suspend fun getLuckyWheelCount(): Pair<ResponseCode, LuckyWheelCountApiModel> = withContext(scope.coroutineContext) {
        return@withContext luckyWheelRemoteDataSource.getLuckyWheelCount()
    }

    override suspend fun getLuckyWheelCoupons(): Pair<ResponseCode, List<LuckyWheelCouponsApiModel>> = withContext(scope.coroutineContext){
        return@withContext luckyWheelRemoteDataSource.getLuckyWheelCoupons()
    }

    override suspend fun startWheel(): Pair<ResponseCode, LuckyWheelPlayApiModel?> = withContext(scope.coroutineContext) {
        return@withContext luckyWheelRemoteDataSource.startWheel()
    }

}