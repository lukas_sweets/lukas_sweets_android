package ua.lukas.sweets.sweets.data.api

import com.google.gson.annotations.SerializedName

data class StockApiModel(
    val id: Int,
    val name: String,
    val short_name: String?,
    val descriptions: String,
    val old_price: String?,
    val new_price: String?,
    val picture: String,
    val label_color: String?,
    val label_text: String?,
    val label_bg_color: String?,
    val show_label: Boolean,
    val top_position: Boolean,
    val time: String,
    val order: Int,
    val products_pack_id: Int?,
)
