package ua.lukas.sweets.sweets.data.repository.user

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.UserApiModel
import ua.lukas.sweets.sweets.data.model.UserModel
import ua.lukas.sweets.sweets.data.model.toDBModel


interface UserRepositoryInterface {
    suspend fun addUser(user: UserApiModel)
    suspend fun deleteUsers()
    suspend fun getUser(): UserModel
    suspend fun getUserEcoCheck(): Boolean
    suspend fun updateUser(user: UserApiModel): ResponseCode
    suspend fun updateUserCity(city: String, user_phone: String)
    suspend fun getUserCity(): String?
}

class UserRepository(
    private val userLocalDataSource: UserLocalDataSource = UserLocalDataSource(),
    private val userRemoteDataSource: UserRemoteDataSource = UserRemoteDataSource(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : UserRepositoryInterface {

    override suspend fun addUser(user: UserApiModel) = withContext(scope.coroutineContext) {
        userLocalDataSource.addUser(user.toDBModel())
    }

    override suspend fun deleteUsers() = withContext(scope.coroutineContext) {
        userLocalDataSource.deleteUsers()
    }

    override suspend fun getUser(): UserModel = withContext(scope.coroutineContext) {
        return@withContext userLocalDataSource.getUser()
    }

    override suspend fun getUserEcoCheck(): Boolean = withContext(scope.coroutineContext) {
        return@withContext userLocalDataSource.getUserEcoCheck()
    }

    override suspend fun updateUser(user: UserApiModel): ResponseCode =
        withContext(scope.coroutineContext) {
            val response = userRemoteDataSource.updateUser(user)

            if (response == ResponseCode.OK) {
                userLocalDataSource.addUser(user.toDBModel())
            }
            return@withContext response
        }

    override suspend fun updateUserCity(city: String, user_phone: String) =
        withContext(scope.coroutineContext) {
            userRemoteDataSource.updateUserCity(city, user_phone)
        }

    override suspend fun getUserCity(): String? = withContext(scope.coroutineContext) {
        return@withContext userLocalDataSource.getUserCity()
    }
}