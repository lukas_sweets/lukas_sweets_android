package ua.lukas.sweets.sweets.module

enum class ResponseCode {
    OK,
    ERROR,
    ERROR_DEVICE_ID,
    NO_CARD
}