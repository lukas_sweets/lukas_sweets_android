package ua.lukas.sweets.sweets.data.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ua.lukas.sweets.sweets.data.api.StockApiModel

@Entity(tableName = "stocks",
    indices = [
        Index(
            value = ["id"],
            unique = true
        )
    ])
data class StockModel(
    @PrimaryKey
    val id: Int,
    val name: String,
    val short_name: String?,
    val description: String,
    val old_price: String?,
    val new_price: String?,
    val time: String,
    val picture: String,
    val top_position: Boolean,
    val show_label: Boolean,
    val label_text: String?,
    val label_color: String?,
    val label_bg_color: String?,
    val order: Int?,
    val productsPackId: Int?
)

fun StockApiModel.toDBModel(): StockModel {
    return StockModel(
        this.id,
        this.name,
        this.short_name,
        this.descriptions,
        this.old_price,
        this.new_price,
        this.time,
        this.picture,
        this.top_position,
        this.show_label,
        this.label_text,
        this.label_color,
        this.label_bg_color,
        this.order,
        this.products_pack_id
    )
}