package ua.lukas.sweets.sweets.view.luckyWheel.adapter

import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.afterMeasured
import ua.lukas.sweets.sweets.data.api.LuckyWheelCouponsApiModel
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetCouponBinding


class LuckyWheelCouponModalBottomSheet(
    private val prise: LuckyWheelCouponsApiModel
) : BottomSheetDialogFragment() {

    private lateinit var binding: ModalBottomSheetCouponBinding
    private val preferences = MyPreferences()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ModalBottomSheetCouponBinding.inflate(layoutInflater)
        return binding.root
    }

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheetBehavior.peekHeight =
            (Resources.getSystem().displayMetrics.heightPixels * 0.9
                    ).toInt()

        view.afterMeasured {
            view.layoutParams = FrameLayout.LayoutParams(
                view.width,
                (Resources.getSystem().displayMetrics.heightPixels * 0.9
                        ).toInt()
            )
        }

        setupView()
    }

    private fun setupView() {
        with(binding) {
            with(prise) {

                nameTextView.text = name
                storeTextView.text = store
                dateTextView.text = date_to
                color?.let { c ->
                    val bottomSheet = view?.parent as View
                    bottomSheet.backgroundTintMode = PorterDuff.Mode.SRC_OVER
                    bottomSheet.backgroundTintList = ColorStateList.valueOf(c)
                }

                Picasso.get().load(picture).into(priseImageView)

                binding.barcodeImageView.afterMeasured {
                    val barcodeEncoder = BarcodeEncoder()
                    val bitmap = barcodeEncoder.encodeBitmap(
                        preferences.cardNumber,
                        BarcodeFormat.CODE_128,
                        binding.barcodeImageView.measuredWidth,
                        binding.barcodeImageView.measuredHeight
                    )
                    binding.barcodeImageView.setImageBitmap(bitmap)
                }

                closeButton.setOnClickListener {
                    dismiss()
                }
            }
        }


    }


}