package ua.lukas.sweets.sweets.data.repository.sms

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import java.util.concurrent.TimeUnit

interface SMSRemoteDataSourceInterface {
    suspend fun sendOtpCode(code: String, phone: String): ResponseCode
}

class SMSRemoteDataSource(
    private val api: Api = Api(),
) : SMSRemoteDataSourceInterface {

    private val TAG = "SMSRemoteDataSource"

    override suspend fun sendOtpCode(code: String, phone: String): ResponseCode {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()


        val request = AndroidNetworking.post(api.otp)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("platform", "android")
            .addQueryParameter("phone", phone)
            .addQueryParameter("code", code)
            .setPriority(Priority.HIGH)
            .build()
        val response = request.executeForString()

        if (response.isSuccess) {
            Log.e("TAG","isSuccess")
            return ResponseCode.OK
        } else {
            Log.e(TAG, response.error.errorCode.toString())
        }
        Log.e("TAG","ERROR")
        return ResponseCode.ERROR
    }
}