package ua.lukas.sweets.sweets.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ua.lukas.sweets.sweets.data.model.UserCityModel
import ua.lukas.sweets.sweets.data.model.UserModel

class UserCityViewModel : ViewModel() {

    private val localCityModel = MutableLiveData<UserCityModel>()

    val listOfCities: LiveData<UserCityModel> = localCityModel

    fun update(data: UserCityModel){
        localCityModel.value = data
    }
}