package ua.lukas.sweets.sweets.view.catalog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import koleton.api.hideSkeleton
import koleton.api.loadSkeleton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.MainActivity
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.CategoriesModel
import ua.lukas.sweets.sweets.data.api.PacksProductsModel
import ua.lukas.sweets.sweets.databinding.FragmentCatalogBinding
import ua.lukas.sweets.sweets.module.Alerts
import ua.lukas.sweets.sweets.module.HideKeyboard
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.view.catalog.adapter.CatalogAdapter
import ua.lukas.sweets.sweets.view.catalog.adapter.CategoriesCatalogAdapter
import ua.lukas.sweets.sweets.view.catalog.adapter.Category
import ua.lukas.sweets.sweets.view.catalog.adapter.ListItem


class CatalogFragment : Fragment() {

    private lateinit var _binding: FragmentCatalogBinding
    private val binding get() = _binding
    private val alerts = activity?.let { Alerts(it) }
    private val filterCatalog = arrayListOf<String>()
    private var filterProduct = ""

    private lateinit var adapterProductCategories: CategoriesCatalogAdapter
    private lateinit var adapterProducts: CatalogAdapter

    private var fullProductsCatalog: List<PacksProductsModel> = arrayListOf()
    private var categories: List<CategoriesModel> = arrayListOf()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCatalogBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        HideKeyboard.setupUI(binding.parent, activity)

        binding.materialToolbar.inflateMenu(R.menu.catalog_menu)
        binding.materialToolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.find_product -> {
                    Intent(context, ScanProductActivity::class.java).apply {
                        flags = flags or Intent.FLAG_ACTIVITY_NO_HISTORY
                        context?.startActivity(this)
                    }
                    true
                }

                R.id.search -> {
                    binding.textField.visibility =  if (binding.textField.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                    true
                }

                else -> false
            }
        }


        adapterProductCategories = CategoriesCatalogAdapter(requireContext(), arrayListOf())
        val layoutManager1: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        _binding.categoriesRecyclerView.layoutManager = layoutManager1
        _binding.categoriesRecyclerView.adapter = adapterProductCategories

        adapterProducts = CatalogAdapter(requireContext(), arrayListOf())
        _binding.productsRecyclerView.adapter = adapterProducts

        adapterProductCategories.setCategoryClickListener(object :
            CategoriesCatalogAdapter.ItemOnClickListener {
            override fun onCategoriesClick(title: String, state: Boolean) {
                if (state)
                    filterCatalog.add(title)
                else
                    filterCatalog.remove(title)
                filters()
            }
        })

        binding.searchEditText.addTextChangedListener {text ->
            filterProduct = text.toString()
            filters()
        }

    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showLuckyWheelBanner(false)
        if (fullProductsCatalog.isEmpty())
            getCatalog()
    }

    private fun updateRecyclerView() {
        val layoutManager2 = GridLayoutManager(context, 2)
        layoutManager2.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (adapterProducts.itemCount > 0)
                    return if (adapterProducts.getItemViewType(position) == ListItem.HEADER) 2 else 1
                else
                    1
            }
        }
        _binding.productsRecyclerView.layoutManager = layoutManager2
    }

    private fun skeletonGo(show: Boolean = true) {
        if (show) {
            binding.categoriesRecyclerView.loadSkeleton(R.layout.list_item_category_catalog) {
                itemCount(10)
            }
            binding.productsRecyclerView.loadSkeleton(R.layout.list_item_catalog){
                itemCount(10)
            }
        } else {
            binding.categoriesRecyclerView.hideSkeleton()
            binding.productsRecyclerView.hideSkeleton()
        }
    }

    private fun getCatalog(reload: Boolean = false) {
        skeletonGo()
        CoroutineScope(Dispatchers.Default).launch {
            val (responseCode, catalog) = (activity as MainActivity).getCatalog(reload)

            withContext(Dispatchers.Main) {
                if (responseCode == ResponseCode.ERROR)
                    alerts?.showAlerterNoInternet()

                catalog?.let { cat ->
                    categories = cat.categories
                    adapterProductCategories.updateList(cat.categories)
                    adapterProducts.updateList(getSectionList(cat.packs_products_model))
                    updateRecyclerView()
                    fullProductsCatalog = cat.packs_products_model
                    skeletonGo(false)
                }
            }
        }
    }

    private fun getSectionList(list: List<PacksProductsModel>): List<ListItem> {
        val data = mutableListOf<ListItem>()
        for (category in categories) {
            val filterPacks = list.filter { it.category == category.name }
            if (filterPacks.isNotEmpty()){
                data.add(Category(category.name.uppercase()))
                data.addAll(filterPacks)
            }
        }

        return data

//        return list.groupBy { it.category }.flatMap {
//            listOf(
//                Category(it.key.uppercase()), *(it.value.map { pack ->
//                    (pack)
//                }).toTypedArray()
//            )
//        }
    }

    private fun filters() {
        var filter = fullProductsCatalog

        if (filterCatalog.size != 0)
            filter = filter.filter { it.category in filterCatalog }

        if (filterProduct.isNotEmpty())
            filter = filter.filter { it.name.lowercase().contains(filterProduct.lowercase()) }

        adapterProducts.updateList(getSectionList(filter))
    }
}