package ua.lukas.sweets.sweets.view.home

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import app.futured.donut.DonutSection
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayoutMediator
import com.kaopiz.kprogresshud.KProgressHUD
import com.marcoscg.dialogsheet.DialogSheet
import com.robinhood.ticker.TickerUtils
import com.robinhood.ticker.TickerView
import kotlinx.coroutines.*
import ua.lukas.sweets.sweets.*
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.data.model.CardModel
import ua.lukas.sweets.sweets.data.model.NewsModel
import ua.lukas.sweets.sweets.data.model.StockModel
import ua.lukas.sweets.sweets.data.model.toApiModel
import ua.lukas.sweets.sweets.databinding.FragmentMainBinding
import ua.lukas.sweets.sweets.view.InfoLoyalityActivity
import ua.lukas.sweets.sweets.view.ProfileActivity
import ua.lukas.sweets.sweets.view.history.HistoryActivity
import ua.lukas.sweets.sweets.view.home.adapter.StockRecyclerViewAdapter
import ua.lukas.sweets.sweets.view.home.adapter.TopStockViewPagerAdapter
import ua.lukas.sweets.sweets.view.news.adapter.NewsViewPager2Adapter
import ua.lukas.sweets.sweets.view.store.StoresActivity


class HomeFragment : Fragment() {

    private val alerts = activity?.let { Alerts(it) }
    private val preferences = MyPreferences()

    private lateinit var _binding: FragmentMainBinding
    private val binding get() = _binding

    private var bonusCard: CardModel? = null

    private var newsList: List<NewsModel> = listOf()
    private var stocksList: List<StockModel> = listOf()
    private var topStocksList: List<StockModel> = listOf()
    private lateinit var topStockAdapter: TopStockViewPagerAdapter

    private var page = 0
    private lateinit var dialog: KProgressHUD
    private var colors = listOf<Int>()
    private var argbEvaluator: ArgbEvaluator = ArgbEvaluator()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        colors = requireContext().resources.getIntArray(R.array.week_cake).toList();

        binding.toolbar.inflateMenu(R.menu.main)
        binding.toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.find_product -> {
                    Toast.makeText(context, "CART", Toast.LENGTH_SHORT).show()
                    // Navigate to settings screen
                    true
                }

                else -> false
            }
        }
        binding.toolbar.setNavigationIcon(R.drawable.ic_store)
        binding.toolbar.navigationContentDescription =
            context?.resources?.getString(R.string.stores)

        binding.toolbar.setNavigationOnClickListener { _ ->
            startActivity(Intent(context, StoresActivity::class.java))
        }

        binding.tickerView2.setCharacterLists(TickerUtils.provideAlphabeticalList())
        binding.tickerView2.setPreferredScrollingDirection(TickerView.ScrollingDirection.DOWN);
        binding.tickerView2.animationDuration = 500
        binding.tickerView2.text = "₴ ${preferences.balance}"

        binding.tickerView2.viewTreeObserver.addOnGlobalLayoutListener {
            val height: Int = binding.tickerView2.measuredHeight
            binding.tickerView2.textSize = height - 30f
        }


        dialog = KProgressHUD.create(context)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(1)
            .setDimAmount(0.5f)


        binding.stocksProgressBar.visibility = View.VISIBLE
        binding.internetKioskCardView.visibility = View.GONE
        binding.stocksRecyclerView.visibility = View.VISIBLE


        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.stocksRecyclerView.layoutManager = layoutManager

        binding.infoCardView.setOnClickListener {
            val infoLoyal = Intent(view.context, InfoLoyalityActivity::class.java)
            startActivity(infoLoyal)
        }

        binding.internetKioskCardView.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://lukas-sweet.shop/"))
            startActivity(browserIntent)
        }


        binding.swipeRefreshLayout.setColorSchemeColors(
            ContextCompat.getColor(
                requireContext(),
                R.color.red
            )
        )
        binding.swipeRefreshLayout.setOnRefreshListener {
            getBonusCard(true)
            getNews()
            getStocks()
            updateFeedback()
        }


        binding.historyButton.setOnClickListener {
            startActivity(Intent(view.context, HistoryActivity::class.java))
        }


        getBonusCard()

        binding.infoLoyalButton.setOnClickListener {
            flipCard(requireContext(), binding.luckyWheelLayout2, binding.luckyWheelLayout1)
        }

        binding.closeLoyalButton.setOnClickListener {
            flipCard(requireContext(), binding.luckyWheelLayout1, binding.luckyWheelLayout2)
        }

        //END onViewCreated
    }


    override fun onResume() {
        super.onResume()
        (activity as MainActivity).showLuckyWheelBanner()
        eco_check()
        getBonusCard()
        getNews()
        getStocks()
    }


    /****************************
    //
     * CARD
    //
     ****************************/

    private fun getBonusCard(reload: Boolean = false) {
        CoroutineScope(Dispatchers.Default).launch {

            bonusCard = (activity as MainActivity).getBonusCard(reload)
            withContext(Dispatchers.Main) {

                bonusCard?.let { cardModel ->
                    setDataCard(cardModel)
                }

                if (binding.swipeRefreshLayout.isRefreshing) {
                    binding.swipeRefreshLayout.isRefreshing = false
                }
            }
        }
    }

    private fun setDataCard(cardModel: CardModel) {
        binding.tickerView2.text =
            "₴ ${cardModel.balance}"
        cardModel.balance?.let { balance ->
            preferences.balance = balance
        }
        binding.donutView.cap = 2000f
        val section1 = DonutSection(
            name = "section_1",
            color = ContextCompat.getColor(requireContext(), R.color.white),
            amount = 500f
        )
        binding.donutView.submitData(listOf(section1))


        cardModel.purchase_amount?.let { purchase_amount ->
            if (purchase_amount >= 2000) {
                binding.loyalSizePercentTextView.text = "5%"
                binding.infoLoyalButton.visibility = View.GONE
            } else {
                binding.loyalSizePercentTextView.text = "3%"
                binding.purchaseAmountTextView.text = purchase_amount.toString()
                binding.infoLoyalButton.visibility = View.VISIBLE
            }
        }
    }


    /****************************
    //
     * NEWS
    //
     ****************************/

    private fun getNews() {
        CoroutineScope(Dispatchers.Default).launch {
            val (response, newsArray) = (activity as MainActivity).getNews()
            withContext(Dispatchers.Main) {
                if (response == ResponseCode.ERROR) alerts?.showAlerterNoInternet()

                if (newsArray.isEmpty() && newsList.isEmpty()) {
                    binding.noInternet.visibility = View.VISIBLE
                    binding.newsViewPager2.visibility = View.GONE
                    binding.newsTabLayout.visibility = View.GONE
                } else if (newsArray.isNotEmpty()) {
                    newsList = newsArray

                    binding.newsViewPager2.apply {
                        adapter = NewsViewPager2Adapter(requireContext(), newsArray)
                        visibility = View.VISIBLE
                    }
//                    setupForNewsView()
                    if (newsList.size > 1) {
                        TabLayoutMediator(
                            binding.newsTabLayout,
                            binding.newsViewPager2
                        ) { tab, position ->
                            tab.text = ""
                        }.attach()
                    }

                    binding.noInternet.visibility = View.GONE
                    binding.newsTabLayout.visibility = View.VISIBLE
                    //startSlide()
                }

                binding.newsProgressBar.visibility = View.GONE
            }
        }
    }


    /****************************
    //
     * STOCKS
    //
     ****************************/

    @SuppressLint("NotifyDataSetChanged")
    private fun getStocks() {
        CoroutineScope(Dispatchers.Default).launch {
            val (generalStocksArray, topStocksArray) = (activity as MainActivity).getStocks()
            withContext(Dispatchers.Main) {
                // General
                if (generalStocksArray.isEmpty() && stocksList.isEmpty()) {
                    binding.stocksProgressBar.visibility = View.GONE
                    binding.internetKioskCardView.visibility = View.VISIBLE
                    binding.stocksRecyclerView.visibility = View.GONE
                    binding.akciiTextView.visibility = View.GONE
                } else {
                    stocksList = generalStocksArray
                    binding.internetKioskCardView.visibility = View.GONE
                    binding.akciiTextView.visibility = View.VISIBLE
                    binding.stocksRecyclerView.apply {
                        visibility = View.VISIBLE
                        adapter = StockRecyclerViewAdapter(requireContext(), generalStocksArray)
                    }
                }
                if (topStocksArray.isEmpty() && topStocksList.isEmpty()) {
                    binding.soonTopStockLayout.visibility - View.VISIBLE
                    binding.topStockViewPager.visibility = View.GONE
                } else {
                    binding.soonTopStockLayout.visibility - View.GONE
                    binding.topStockViewPager.visibility = View.VISIBLE
                    topStocksList = topStocksArray
                    binding.topStockViewPager.apply {
                        offscreenPageLimit = 2
                        visibility = View.VISIBLE
                        topStockAdapter = TopStockViewPagerAdapter(requireContext(), topStocksArray)
                        adapter = topStockAdapter
                        orientation = ViewPager2.ORIENTATION_HORIZONTAL
                        (getChildAt(0) as RecyclerView).overScrollMode =
                            RecyclerView.OVER_SCROLL_NEVER
                    }

                    if (topStockAdapter.itemCount > 1) {
                        TabLayoutMediator(
                            binding.tabLayout,
                            binding.topStockViewPager
                        ) { tab, position ->
                            tab.text = ""
                        }.attach()
                    }

                    binding.topStockViewPager.registerOnPageChangeCallback(object :
                        OnPageChangeCallback() {
                        override fun onPageScrolled(
                            position: Int,
                            positionOffset: Float,
                            positionOffsetPixels: Int
                        ) {
                            super.onPageScrolled(position, positionOffset, positionOffsetPixels)

                            if (position > colors.size - 1) {
                                binding.topStockViewPager.setBackgroundColor(
                                    argbEvaluator.evaluate(
                                        positionOffset,
                                        colors[colors.size - 1],
                                        colors[colors.size - 1]
                                    ) as Int
                                )
                            } else if (position == colors.size - 1) {
                                binding.topStockViewPager.setBackgroundColor(
                                    argbEvaluator.evaluate(
                                        positionOffset,
                                        colors[position],
                                        colors[position]
                                    ) as Int
                                )
                            } else {
                                binding.topStockViewPager.setBackgroundColor(
                                    argbEvaluator.evaluate(
                                        positionOffset,
                                        colors[position],
                                        colors[position + 1]
                                    ) as Int
                                )
                            }
                        }
                    })
                }
                binding.stocksProgressBar.visibility = View.GONE
            }
        }
    }


    /****************************
    //
     * FEEDBACK
    //
     ****************************/

    private fun updateFeedback() {
        (activity as MainActivity).updateBadge()
    }


    /****************************
    //
     * Eco Check
    //
     ****************************/


    private fun eco_check() {

        if (preferences.hideEcoCheck) {
            binding.ecoCheckView.visibility = View.GONE
        } else {
            binding.ecoCheckView.visibility = View.VISIBLE
        }

        CoroutineScope(Dispatchers.Default).launch {
            val user = (activity as MainActivity).getUser()

            withContext(Dispatchers.Main) {
                if (user.send_eco_check) {
                    binding.mainEcoCheckView.visibility = View.GONE
                    binding.okEcoCheckView.visibility = View.VISIBLE
                } else {
                    binding.mainEcoCheckView.visibility = View.VISIBLE
                    binding.okEcoCheckView.visibility = View.GONE
                }

                binding.ecoCheckRefuseButton.setOnClickListener {
                    DialogSheet(activity)
                        .setTitle(resources.getString(R.string.sad_eco_check))
                        .setMessage(resources.getString(R.string.sad_eco_check_text) + "\n\n")
                        .setCancelable(false)
                        .setRoundedCorners(true)
                        .setButtonsColorRes(R.color.red)
                        .setPositiveButton(android.R.string.ok) {
                            MyPreferences().hideEcoCheck = true
                            binding.ecoCheckView.visibility = View.GONE
                        }
                        .show()
                }


                binding.ecoCheckButtonGet.setOnClickListener {
                    //No Email
                    if (user.email.isNullOrEmpty()) {
                        DialogSheet(activity)
                            .setTitle(resources.getString(R.string.good_eco_check))
                            .setMessage(resources.getString(R.string.no_email_eco_check_text) + "\n\n")
                            .setCancelable(false)
                            .setRoundedCorners(true)
                            .setButtonsColorRes(R.color.red)
                            .setPositiveButton(android.R.string.ok) {
                                Intent(context, ProfileActivity::class.java).apply {
                                    putExtra("enterEmail", true)
                                    context?.startActivity(this)
                                }
                            }
                            .show()
                    } else {
                        dialog.show()
                        CoroutineScope(Dispatchers.Default).launch {
                            user.send_eco_check = true

                            when ((activity as MainActivity).updateUser(user.toApiModel())) {
                                ResponseCode.OK -> {
                                    (activity as MainActivity).getUserAsync().await()
                                    (activity as MainActivity).getBonusCard(true)

                                    withContext(Dispatchers.Main) {
                                        dialog.dismiss()
                                        DialogSheet(activity)
                                            .setTitle(resources.getString(R.string.good_eco_check))
                                            .setMessage(resources.getString(R.string.email_eco_check_text) + "\n\n" + user.email + "\n\n")
                                            .setCancelable(false)
                                            .setRoundedCorners(true)
                                            .setButtonsColorRes(R.color.red)
                                            .setPositiveButton(android.R.string.ok) {
                                                binding.mainEcoCheckView.visibility = View.GONE
                                                binding.okEcoCheckView.visibility = View.VISIBLE
                                            }
                                            .show()
                                    }
                                }

                                else -> {
                                    withContext(Dispatchers.Main) {
                                        dialog.dismiss()
                                        alerts?.showAlerterNoInternet()
                                    }
                                }
                            }
                        }

                    }

                }



                binding.ecoCheckHideButton.setOnClickListener {
                    binding.ecoCheckView.visibility = View.GONE
                    MyPreferences().hideEcoCheck = true
                }


                binding.detailTextView.setOnClickListener {
                    val dialog = BottomSheetDialog(requireContext())

                    val view = layoutInflater.inflate(R.layout.sheet_save_forest, null)

                    val btnClose = view.findViewById<ImageButton>(R.id.imageButton)
                    val btnOk = view.findViewById<Button>(R.id.button2)

                    btnClose.setOnClickListener {
                        dialog.dismiss()
                    }

                    btnOk.setOnClickListener {
                        dialog.dismiss()
                    }

                    dialog.setCancelable(false)

                    dialog.setContentView(view)

                    dialog.behavior.peekHeight =
                        (Resources.getSystem().displayMetrics.heightPixels * 0.95).toInt()

                    dialog.window?.setBackgroundDrawableResource(android.R.color.transparent);
                    (requireView().parent as View).setBackgroundColor(Color.TRANSPARENT)
                    (view.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)


                    view.layoutParams.height =
                        (Resources.getSystem().displayMetrics.heightPixels * 0.95).toInt()

                    dialog.show()
                }
            }

        }
    }

    private fun setupForNewsView() {
        var ivArrayDotsPagerNews: Array<ImageView?> = arrayOf()

        //News
        binding.newsViewPager2.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                try {
                    page = position
                    if (ivArrayDotsPagerNews.isNotEmpty()) {
                        for (ivArrayDotsPagerNew in ivArrayDotsPagerNews) {
                            ivArrayDotsPagerNew?.setImageResource(R.drawable.page_indicator_unselected_red)
                        }
                    }
                    ivArrayDotsPagerNews[position]?.setImageResource(R.drawable.page_indicator_selected_red)
                } catch (_: java.lang.Exception) {

                }

            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        if (newsList.isNotEmpty() && context != null) {
            binding.newsPagerDots.removeAllViews()
            ivArrayDotsPagerNews = arrayOfNulls(newsList.size)
            for (i in newsList.indices) {
                ivArrayDotsPagerNews[i] = ImageView(context)
                val params = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(15, 0, 15, 0)
                ivArrayDotsPagerNews[i]?.layoutParams = params
                ivArrayDotsPagerNews[i]?.setImageResource(R.drawable.page_indicator_unselected_red)
                ivArrayDotsPagerNews[i]?.setOnClickListener { view -> view.alpha = 1f }
                binding.newsPagerDots.addView(ivArrayDotsPagerNews[i])
            }
            if (newsList.size == 1) {
                binding.newsPagerDots.visibility = LinearLayout.GONE
            }
            if (newsList.isEmpty()) {
                binding.noInternet.visibility = View.VISIBLE
            } else {
                ivArrayDotsPagerNews[0]?.setImageResource(R.drawable.page_indicator_selected_red)
            }
        }

    }

    private fun flipCard(context: Context, visibleView: View, inVisibleView: View) {

        try {
            visibleView.visibility = View.VISIBLE

            val scale = context.resources.displayMetrics.density
            val cameraDist = 8000 * scale
            visibleView.cameraDistance = cameraDist
            inVisibleView.cameraDistance = cameraDist
            val flipOutAnimatorSet =
                AnimatorInflater.loadAnimator(
                    context,
                    R.animator.flip_out_card
                ) as AnimatorSet
            flipOutAnimatorSet.setTarget(inVisibleView)

            val flipInAnimatorSet =
                AnimatorInflater.loadAnimator(
                    context,
                    R.animator.flip_in_card
                ) as AnimatorSet
            flipInAnimatorSet.setTarget(visibleView)

            flipOutAnimatorSet.start()
            flipInAnimatorSet.start()
            flipInAnimatorSet.doOnEnd {
                inVisibleView.visibility = View.GONE
            }
        } catch (e: Exception) {
//            logHandledException(e)
        }
    }

    //END FRAGMENT
}

