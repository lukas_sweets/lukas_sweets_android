package ua.lukas.sweets.sweets.data.db

import androidx.room.*
import ua.lukas.sweets.sweets.data.model.FeedbackModel

@Dao
interface FeedbackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addFeedbacks(feedback: ArrayList<FeedbackModel>)

    @Query("UPDATE feedbacks SET status = :status, subject = :subject")
    fun updateStatusSubject(status: Int, subject: Int)

    @Query("SELECT message_id, feedback_id, max(date_time) as date_time, subject, status, text, author, min(is_read) as is_read FROM feedbacks GROUP BY feedback_id ORDER BY date_time DESC")
    fun getFeedbacks(): List<FeedbackModel>

    @Query("SELECT * FROM feedbacks WHERE feedback_id = :feedback_id ORDER BY message_id")
    fun getFeedbackMessages(feedback_id: Int): List<FeedbackModel>

    @Query("SELECT message_id FROM feedbacks WHERE feedback_id = :feedback_id AND is_read = 0 AND author = 1")
    fun getNewMessageFromFeedback(feedback_id: Int): List<Int>

    @Query("SELECT COUNT(feedback_id) FROM feedbacks WHERE is_read = 0 AND author = 1 GROUP BY feedback_id")
    fun getNewFeedbacksCount() : Int

    @Query("SELECT CASE count(message_id) WHEN 0 THEN 0 ELSE 1 END AS as_new FROM feedbacks WHERE feedback_id = :feedback_id AND is_read = 0 AND author = 1")
    fun hasNewFeedback(feedback_id: Int) : Boolean

    @Query("DELETE FROM feedbacks")
    fun deleteAllFeedbacks()
}