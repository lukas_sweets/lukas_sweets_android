package ua.lukas.sweets.sweets.view.questions.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;

import ua.lukas.sweets.sweets.view.questions.Questions;


public class QuestionAdapter extends ArrayAdapter<Questions> {



    public QuestionAdapter(Context context, ArrayList<Questions> users) {
        super(context, 0, users);
    }

    @NotNull
    @Override
    public View getView(final int position, View convertView, @NotNull ViewGroup parent) {
        Questions questions = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView tvName = convertView.findViewById(android.R.id.text1);
        tvName.setText(questions != null ? questions.getQuestion() : "Questions");
        return convertView;
    }
}

