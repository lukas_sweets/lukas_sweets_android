package ua.lukas.sweets.sweets.data.model

import ua.lukas.sweets.sweets.BuildConfig

data class NewFeedbackModel(
    var phone: String? = null,
    var name: String? = null,
    var subject: Int? = null,
    var city: String? = null,
    var store: Int? = null,
    var text: String? = null,
    val platform: String = "Android",
    val version: String = BuildConfig.VERSION_NAME,
    val build: String = BuildConfig.VERSION_CODE.toString()
)
