package ua.lukas.sweets.sweets.view.catalog.adapter

import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ua.lukas.sweets.sweets.module.HideKeyboard
import ua.lukas.sweets.sweets.data.api.ProductAvailabilityApiModel
import ua.lukas.sweets.sweets.data.model.UserCityModel
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetProductBinding
import ua.lukas.sweets.sweets.view.catalog.ProductCatalog
import ua.lukas.sweets.sweets.viewmodel.ProductAvailabilityViewModel
import ua.lukas.sweets.sweets.viewmodel.UserCityViewModel

class ProductModalBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding: ModalBottomSheetProductBinding
    private val productAvailabilityViewModel: ProductAvailabilityViewModel by activityViewModels()
    private val userCityViewModel: UserCityViewModel by activityViewModels()

    private lateinit var adapter: ProductAvailabilityAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ModalBottomSheetProductBinding.inflate(layoutInflater)
        return binding.root
    }
    //? = inflater.inflate(R.layout.modal_bottom_sheet_product, container, false)

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior
        modalBottomSheetBehavior.peekHeight =
            (Resources.getSystem().displayMetrics.heightPixels * 0.9).toInt()
        modalBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        modalBottomSheetBehavior.isFitToContents = false
        view.minimumHeight = Resources.getSystem().displayMetrics.heightPixels

//        activity?.let { activity ->
//            alerts = Alerts(activity)
//            loadDialog = alerts.load()
//        }

//        loadDialog = KProgressHUD.create(activity)
//            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//            .setCancellable(false)
//            .setAnimationSpeed(1)
//            .setDimAmount(0.5f)





        adapter = ProductAvailabilityAdapter(requireContext(), arrayListOf())
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(requireContext())
        binding.recyclerView.layoutManager = layoutManager

        binding.recyclerView.adapter = adapter
        setupViewModel()
    }

    private fun setupViewModel() {
        productAvailabilityViewModel.availabilityProductModel.observe(viewLifecycleOwner) { data ->
            setupData(data)
        }

        userCityViewModel.listOfCities.observe(viewLifecycleOwner) { data ->
            settingStores(data)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun settingStores(data: UserCityModel) {
        val adapterKiosk = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_list_item_1,
            data.cities
        )


        binding.cityEdit.apply {
            setAdapter(adapterKiosk)
            if (data.userCity != null) {
                setText(data.userCity, false)
            }

            keyListener = null
            setOnTouchListener { v, event ->
                HideKeyboard.hideSoftKeyboard(requireActivity())
                (v as AutoCompleteTextView).showDropDown()
                false
            }
            setOnItemClickListener { parent, view, position, rowId ->
                (activity as ProductCatalog).changeCity(parent.getItemAtPosition(position) as String)
            }
            clearFocus()
        }
    }

    private fun setupData(in_data: List<ProductAvailabilityApiModel>?) {
        if (in_data != null) {
            adapter.update(in_data)
        }
    }
}