package ua.lukas.sweets.sweets.data.repository.history

import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.MyPreferences
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.HistoryDtApiModel
import ua.lukas.sweets.sweets.data.api.HistoryGeneralApiModel
import java.util.concurrent.TimeUnit

interface HistoryRemoteDataSourceInterface {
    suspend fun downloadHistories(limit: Int = 99999999): Pair<ResponseCode, HistoryGeneralApiModel?>
    suspend fun downloadHistoryDTs(history_id: Int): Pair<ResponseCode, List<HistoryDtApiModel>>
}

class HistoryRemoteDataSource(
    private val api: Api = Api(),
    private val preferences: MyPreferences = MyPreferences(),
) : HistoryRemoteDataSourceInterface {

    private val TAG = "HistoryRemoteDataSource"

    override suspend fun downloadHistories(limit: Int): Pair<ResponseCode, HistoryGeneralApiModel?> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.history)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("phone", preferences.phone)
            .addQueryParameter("limit", limit.toString())
            .build()

        val response = request.executeForObject(HistoryGeneralApiModel::class.java)

        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as HistoryGeneralApiModel)
        else
            Log.e(TAG, response.error.toString())


        return Pair(ResponseCode.ERROR, null)
    }

    override suspend fun downloadHistoryDTs(history_id: Int): Pair<ResponseCode, List<HistoryDtApiModel>> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.historyDt)
            .setOkHttpClient(okHttpClient)
            .addQueryParameter("id", history_id.toString())
            .build()

        val response = request.executeForObjectList(HistoryDtApiModel::class.java)

        if (response.isSuccess)
            return Pair(ResponseCode.OK, response.result as List<HistoryDtApiModel>)
        else
            Log.e(TAG, response.error.toString())


        return Pair(ResponseCode.ERROR, arrayListOf())
    }
}