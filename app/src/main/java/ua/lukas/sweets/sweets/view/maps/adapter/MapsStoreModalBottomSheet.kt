package ua.lukas.sweets.sweets.view.maps.adapter

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fondesa.kpermissions.allGranted
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ua.lukas.sweets.sweets.data.model.StoreModel
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetStoreBinding

class MapsStoreModalBottomSheet(
    private val store: StoreModel
) : BottomSheetDialogFragment() {

    private lateinit var binding: ModalBottomSheetStoreBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ModalBottomSheetStoreBinding.inflate(layoutInflater)
        return binding.root
    }
    //? = inflater.inflate(R.layout.modal_bottom_sheet_product, container, false)

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
//        bottomSheetBehavior.peekHeight =
//            (Resources.getSystem().displayMetrics.heightPixels * 0.9).toInt()
//
//        view.afterMeasured {
//            view.layoutParams = FrameLayout.LayoutParams(
//                view.width,
//                (Resources.getSystem().displayMetrics.heightPixels * 0.9).toInt()
//            )
//        }


        setupView()


    }

    private fun setupView() {

        binding.addressTextView.text = store.address
        binding.timeWorkTextView.text = store.time_work
        binding.callButton.setOnClickListener {
            permissionsBuilder(Manifest.permission.CALL_PHONE).build().addListener { permission ->
                if (permission.allGranted()) {
                    this@MapsStoreModalBottomSheet.startActivity(
                    Intent(
                        Intent.ACTION_CALL,
                        Uri.parse("tel:" + store.phone)
                    )
                )
                }

            }
//            if (ActivityCompat.checkSelfPermission(
//                    context,
//                    Manifest.permission.CALL_PHONE
//                ) == PackageManager.PERMISSION_GRANTED
//            ) {
//                this.startActivity(
//                    Intent(
//                        Intent.ACTION_CALL,
//                        Uri.parse("tel:" + store.phone)
//                    )
//                )
//            } else {
//                checkPermission(Manifest.permission.CALL_PHONE, 1)
//            }
        }

        binding.routeButton.setOnClickListener {
            val uri =
                "http://maps.google.com/maps?daddr=" + store.lat + "," + store.lon + " (" + "Lukas Sweets" + ")"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            try {
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                try {
                    val unrestrictedIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    startActivity(unrestrictedIntent)
                } catch (innerEx: ActivityNotFoundException) {
//                    Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG)
//                        .show()
                }
            }
        }

//        binding.closeButton.setOnClickListener {
//            dismiss()
//        }
    }


}