package ua.lukas.sweets.sweets.view.history.adapter;


public class HeaderRecycleView implements HistoryAdapter.ListItem {
    private String name, year;

    public HeaderRecycleView(String name, String year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public int getItemType() {
        return HistoryAdapter.ListItem.TYPE_HEADER;
    }
}
