package ua.lukas.sweets.sweets.view.history

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.Alerts
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.repository.history.HistoryRepository
import ua.lukas.sweets.sweets.databinding.ActivityHistoryBinding
import ua.lukas.sweets.sweets.view.history.adapter.HistoryAdapter

class HistoryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHistoryBinding

    private val historyRepository = HistoryRepository()

    private var alerts: Alerts? = null
    private var koefHistory = 1
    private val historyToOne = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        alerts = Alerts(this)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.recycleHistory.layoutManager = layoutManager

        binding.noCart.visibility = View.GONE
        binding.moreHistoryButton.visibility = View.INVISIBLE
        binding.progressBar.visibility = View.GONE


        binding.moreHistoryButton.setOnClickListener {
            koefHistory++
            getHistory(limit = historyToOne * koefHistory)
        }

        binding.swipeRefreshLayout.setColorSchemeColors(
            ContextCompat.getColor(
                this,
                R.color.colorPrimary
            )
        )
        binding.swipeRefreshLayout.setOnRefreshListener {
            getHistory(reload = true)
        }
    }

    override fun onResume() {
        super.onResume()
        getHistory()
    }

    private fun getHistory(reload: Boolean = false, limit: Int = historyToOne) {
        binding.progressBar.visibility = View.VISIBLE
        CoroutineScope(Dispatchers.Default).launch {
            val (response, listHistory) = historyRepository.getHistories(
                reload,
                limit
            )

            withContext(Dispatchers.Main) {
                if (response == ResponseCode.ERROR)
                    alerts?.showAlerterNoInternet()

                val dataHistory = HistoryAdapter.getNewListItem(this@HistoryActivity, listHistory)
                val adapter = HistoryAdapter(this@HistoryActivity, dataHistory)

                if (listHistory.isNotEmpty()) {
                    binding.noCart.visibility = View.GONE
                    if (listHistory.size < historyRepository.getCountHistories())
                        binding.moreHistoryButton.visibility = View.VISIBLE
                    else
                        binding.moreHistoryButton.visibility = View.INVISIBLE

                } else {
                    binding.noCart.visibility = View.VISIBLE
                    binding.moreHistoryButton.visibility = View.INVISIBLE
                }

                binding.recycleHistory.adapter = adapter

                if (binding.swipeRefreshLayout.isRefreshing)
                    binding.swipeRefreshLayout.isRefreshing = false
                binding.progressBar.visibility = View.GONE

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }


}