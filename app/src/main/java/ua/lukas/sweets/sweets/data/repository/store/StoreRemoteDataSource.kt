package ua.lukas.sweets.sweets.data.repository.store

import com.androidnetworking.AndroidNetworking
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import ua.lukas.sweets.sweets.module.ResponseCode
import ua.lukas.sweets.sweets.data.api.Api
import ua.lukas.sweets.sweets.data.api.StoreApiModel
import java.util.concurrent.TimeUnit


interface StoreRemoteDataStoreInterface {
    suspend fun downloadStores(): Pair<ResponseCode, ArrayList<StoreApiModel>>
}

class StoreRemoteDataSource(
    private val api: Api = Api(),
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) : StoreRemoteDataStoreInterface {

//    private val TAG = "StoreRemoteDataSource"


    override suspend fun downloadStores(): Pair<ResponseCode, ArrayList<StoreApiModel>> {

        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build()

        val request = AndroidNetworking.get(api.store)
            .setOkHttpClient(okHttpClient)
            .build()
        val response = request.executeForObjectList(StoreApiModel::class.java)
        if (response.isSuccess) {
            return Pair(
                ResponseCode.OK,
                response.result as ArrayList<StoreApiModel>
            )
        }

        return Pair(ResponseCode.ERROR, arrayListOf())
    }

}
