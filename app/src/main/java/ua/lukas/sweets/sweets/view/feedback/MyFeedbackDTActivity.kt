package ua.lukas.sweets.sweets.view.feedback

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ua.lukas.sweets.sweets.module.*
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.model.FeedbackModel
import ua.lukas.sweets.sweets.data.repository.feedback.FeedbackRepository
import ua.lukas.sweets.sweets.databinding.ActivityMyFeedbackDtBinding
import java.util.*


class MyFeedbackDTActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMyFeedbackDtBinding
    private val feedbackRepository = FeedbackRepository()
    private lateinit var alerts: Alerts
    private var loadDialog: KProgressHUD? = null


    private var messageList: ArrayList<FeedbackModel> = arrayListOf()
    private lateinit var messageAdapter: MyFeedbackDTAdapter
    private var feedback_id = 0
//    private var dialog: LoadingDialog? = null

    private val timer = Timer()
    val tt: TimerTask = object : TimerTask() {
        override fun run() {
            getMessages(true)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyFeedbackDtBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);


        alerts = Alerts(this)
        loadDialog = alerts.load()
        feedback_id = intent.getIntExtra("hd_id", 0)
        val subject = intent.getIntExtra("subject", -1)

        setSupportActionBar(binding.materialToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        messageAdapter = MyFeedbackDTAdapter(messageList)
        binding.recycleView.adapter = messageAdapter

        HideKeyboard.setupUI(findViewById(R.id.recycleView), this)

        binding.closeFeedbackTextView.visibility = View.GONE

        when (subject) {
            1 -> title =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_gratitude)
            2 -> title =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_question)
            3 -> title =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_complaint)
            4 -> title =
                LukasSweetsApp.getContext().resources.getString(R.string.feedback_subject_offer)
        }


        val llm = LinearLayoutManager(this)
        llm.reverseLayout = true;
        binding.recycleView.layoutManager = llm


        binding.sendImageButton.setOnClickListener {
            if (binding.textTextView.text.toString().trim().isNotEmpty()) {
                val feedback = FeedbackModel(
                    0,
                    feedback_id, DateTime.getCurrentDateFormat(),
                    -1,
                    -1,
                    binding.textTextView.text.toString(),
                    0,
                    0
                )

                messageList.add(feedback)
                messageList.sortByDescending { feedback -> feedback.date_time }
                messageAdapter = MyFeedbackDTAdapter(messageList)
                binding.recycleView.adapter = messageAdapter
                binding.recycleView.scrollToPosition(0)
                binding.textTextView.text = null

                CoroutineScope(Dispatchers.Default).launch {
                    val result = feedbackRepository.sendAnswerFeedback(feedback)
                    withContext(Dispatchers.Main) {
                        when (result) {
                            ResponseCode.OK -> {
                                binding.recycleView.scrollToPosition(0)
                            }
                            else -> {
                                alerts.showAlerterNoInternet()
                            }
                        }
                    }
                }
            }
        }


    }


    override fun onResume() {
        super.onResume()
        getMessages()
        timer.schedule(tt, 3000, 3000)
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }

    private fun getMessages(update: Boolean = false) {
        CoroutineScope(Dispatchers.Default).launch {
            var response = ResponseCode.OK
            if (update)
                response = feedbackRepository.downloadFeedbacksAsync(
                    feedbackRepository.getNewMessageFromFeedback(feedback_id)
                ).await()

            withContext(Dispatchers.Main) {

                if (response == ResponseCode.ERROR)
                    alerts?.showAlerterNoInternet()

                messageList =
                    ArrayList(feedbackRepository.getFeedbackMessages(feedback_id).reversed())
                messageAdapter.updateList(messageList)

                if (messageList[0].status == 3) {
                    binding.textTextView.visibility = View.GONE
                    binding.sendImageButton.visibility = View.GONE
                    binding.closeFeedbackTextView.visibility = View.VISIBLE
                } else {
                    binding.textTextView.visibility = View.VISIBLE
                    binding.sendImageButton.visibility = View.VISIBLE
                    binding.closeFeedbackTextView.visibility = View.GONE
                }
            }
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}

private class MyFeedbackDTAdapter(private var messageList: List<FeedbackModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val MESSAGE_TYPE_USER = 0;
    val MESSAGE_TYPE_SUPPORT = 1;

    private inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val messageBgView: CardView = itemView.findViewById(R.id.cardView)
        val textMessage: TextView = itemView.findViewById(R.id.textView)
        val timeTextView: TextView = itemView.findViewById(R.id.timeTextView)
        val newMessageTextView: TextView = itemView.findViewById(R.id.newMessageTextView)
        val lineBrake: LinearLayout = itemView.findViewById(R.id.lineBrake)

        fun bind(position: Int) {
            val messageFeedback = messageList[position]

            var nextMessageFeedback: FeedbackModel? = null
            var lastMessageFeedback: FeedbackModel? = null

            try {
                nextMessageFeedback = messageList[position + 1]
            } catch (_: Exception) {
            }
            try {
                lastMessageFeedback = messageList[position - 1]
            } catch (_: Exception) {
            }

            textMessage.text = messageFeedback.text
            timeTextView.text = DateTime.formatDateWithTimeFeedback(messageFeedback.date_time)

            if (lastMessageFeedback?.author == 0) {
                messageBgView.radius = 18.dp
            } else {
                messageBgView.radius = 0.dp
            }

            if (messageFeedback.is_read == 1 || nextMessageFeedback?.is_read == 0) {
                newMessageTextView.visibility = View.GONE
            } else {
                newMessageTextView.visibility = View.VISIBLE

                if (nextMessageFeedback == null) {
                    //Last message
                    newMessageTextView.visibility = View.GONE
                }
            }

            if (messageFeedback.author == 0) {
                newMessageTextView.visibility = View.GONE
            }

            if (lastMessageFeedback?.author != messageFeedback.author) {
                lineBrake.visibility = View.VISIBLE
            } else {
                lineBrake.visibility = View.GONE

            }
        }

    }

    private inner class SupportViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val messageBgView: CardView = itemView.findViewById(R.id.cardView)
        val racoonIcon: ImageView = itemView.findViewById(R.id.imageView)
        val textMessage: TextView = itemView.findViewById(R.id.textView)
        val timeTextView: TextView = itemView.findViewById(R.id.timeTextView)
        val newMessageTextView: TextView = itemView.findViewById(R.id.newMessageTextView)
        val lineBrake: LinearLayout = itemView.findViewById(R.id.lineBrake)

        fun bind(position: Int) {
            val messageFeedback = messageList[position]

            var nextMessageFeedback: FeedbackModel? = null
            var lastMessageFeedback: FeedbackModel? = null

            try {
                nextMessageFeedback = messageList[position + 1]
            } catch (e: Exception) {
                Log.e(null, e.toString())
            }
            try {
                lastMessageFeedback = messageList[position - 1]
            } catch (_: Exception) {
            }

            textMessage.text = messageFeedback.text
            timeTextView.text = DateTime.formatDateWithTimeFeedback(messageFeedback.date_time)

            if (lastMessageFeedback?.author == 1) {
                racoonIcon.visibility = View.INVISIBLE
                messageBgView.radius = 18.dp
            } else {
                racoonIcon.visibility = View.VISIBLE
                messageBgView.radius = 0.dp
            }

            if (messageFeedback.is_read == 1 || nextMessageFeedback?.is_read == 0) {
                newMessageTextView.visibility = View.GONE
            } else {
                newMessageTextView.visibility = View.VISIBLE
                if (nextMessageFeedback == null) {
                    //Last message
                    newMessageTextView.visibility = View.GONE
                }
            }


            if (lastMessageFeedback?.author != messageFeedback.author) {
                lineBrake.visibility = View.VISIBLE
            } else {
                lineBrake.visibility = View.GONE

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MESSAGE_TYPE_USER) {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_user_message_feedback, parent, false)
            return UserViewHolder(itemView)
        } else {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_support_message_feedback, parent, false)
            return SupportViewHolder(itemView)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (messageList[position].author == MESSAGE_TYPE_USER) {
            (holder as UserViewHolder).bind(position)
        } else {
            (holder as SupportViewHolder).bind(position)
        }

    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun getItemViewType(position: Int): Int {
        return messageList[position].author
    }


    val Int.dp: Float
        get() = (this * Resources.getSystem().displayMetrics.density + 0.5f)

    fun updateList(newMessageList: List<FeedbackModel>) {
        messageList = newMessageList
        notifyDataSetChanged()
    }

}