package ua.lukas.sweets.sweets.view.luckyWheel.adapter

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Picasso
import ua.lukas.sweets.sweets.module.afterMeasured
import ua.lukas.sweets.sweets.R
import ua.lukas.sweets.sweets.data.api.LuckyWheelWinPriseApiModel
import ua.lukas.sweets.sweets.databinding.ModalBottomSheetPriseBinding


class LuckyWheelPriseModalBottomSheet(
    private val prise: LuckyWheelWinPriseApiModel
) : BottomSheetDialogFragment() {

    private lateinit var binding: ModalBottomSheetPriseBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ModalBottomSheetPriseBinding.inflate(layoutInflater)
        return binding.root
    }

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheetBehavior.peekHeight =
            (Resources.getSystem().displayMetrics.heightPixels //* 0.8
                    ).toInt()

        view.afterMeasured {
            view.layoutParams = FrameLayout.LayoutParams(
                view.width,
                (Resources.getSystem().displayMetrics.heightPixels //* 0.8
                        ).toInt()
            )
        }

        setupView()
    }

    private fun setupView() {
        with(binding) {
            with(prise) {
                nameTextView.text = context?.resources?.getString(R.string.lucky_wheel_prise, name)

                Picasso.get().load(picture).into(priseImageView)
                if (is_bonus) infoMaterialCardView.visibility = View.GONE

                closeButton.setOnClickListener {
                    dismiss()
                }
            }
        }


    }


}